import React, { useCallback, useEffect, useMemo, useState } from "react";
import "react-big-calendar/lib/css/react-big-calendar.css"; // thư viện css ngoài
import { Calendar, momentLocalizer } from 'react-big-calendar'
import moment from 'moment';
import { useTranslation } from "react-i18next";

import AppointmentsModalCreate from "./AppointmentsModalCreate";
import CommonSpin from "../../../components/Common/Spin";

import appointmentServices from "../../../services/appointments.service";

const localizer = momentLocalizer(moment);

function AppointmentsCalendar(props:any) {
    const { doctorId } = props;
    const { t } = useTranslation();
    const [visibleModalCreate, setVisibleModalCreate] = useState<boolean>(false);
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [currentAppointment, setCurrentAppointment] = useState<any>(undefined)
    const [events, setEvents] = useState<any[]>([])
    const defaultMessages = useMemo(()=>{               // tạo ra 1 biến defaultMessages để dịch các từ khóa trong calendar
        return {
            date: t("appointmentPage.form.customCalendar.date"),
            time: t("appointmentPage.form.customCalendar.time"),
            event: t("appointmentPage.form.customCalendar.event"),
            allDay: t("appointmentPage.form.customCalendar.allDay"),
            week: t("appointmentPage.form.customCalendar.week"),
            work_week: t("appointmentPage.form.customCalendar.workWeek"),
            day: t("appointmentPage.form.customCalendar.day"),
            month: t("appointmentPage.form.customCalendar.month"),
            previous: t("appointmentPage.form.customCalendar.previous"),
            next: t("appointmentPage.form.customCalendar.next"),
            yesterday: t("appointmentPage.form.customCalendar.yesterday"),
            tomorrow: t("appointmentPage.form.customCalendar.tomorrow"),
            today: t("appointmentPage.form.customCalendar.today"),
            agenda: t("appointmentPage.form.customCalendar.agenda"),
            noEventsInRange: t("appointmentPage.form.customCalendar.noEventsInRange"),
            showMore: function showMore(total:number) {
                return "+" + total + " " + t("appointmentPage.form.customCalendar.more");
            }
        }
    },[t]);

    const getData = useCallback(async () => {
        setIsLoading(true);
        const paramsSearch = {
            page: 0,
            size: 1000,
            search: JSON.stringify({})
        }
        if(doctorId){
            paramsSearch.search = JSON.stringify({doctorId: doctorId})
        }
        const resp = await appointmentServices.getListAppointments(paramsSearch);
        const data = resp?.data;
        if (resp?.status === 200) {
            setEvents( 
                data?.data?.content?.filter((item:any)=> (item?.status === 0 || item?.status === 1 || item?.status === 2))
                                    ?.map((item:any)=> ({
                                                title: `${item?.admCustomer?.fullName} (${item?.appointmentCode})`,
                                                start: moment(item?.bookingDate).toDate(),
                                                end: moment(item?.bookingDate).toDate(),
                                                appointmentDetail: item
                                            }
                                        ))
            )
        } else {
            setEvents([])
        }
        setIsLoading(false);
    },[doctorId])

    useEffect(()=>{
        getData();
    },[getData])

    const handleSelectSlot = useCallback(({ start, end }:any) => {
            console.log("start", moment(start).format("YYYY-MM-DD HH:mm"))
            console.log("end", moment(end).format("YYYY-MM-DD HH:mm"))
    },[])

    const handleOk = ()=>{
        getData();
        setVisibleModalCreate(false);
        setCurrentAppointment(undefined)
    }
    
    return (
        <div>
            <CommonSpin isLoading={isLoading}>
                <Calendar
                    localizer={localizer}
                    events={events}
                    startAccessor="start"
                    endAccessor="end"
                    onSelectEvent={(event:any)=>{setCurrentAppointment(event?.appointmentDetail); setVisibleModalCreate(true)}}     // khi click vào event thì hiển thị modal chi tiết lịch hẹn đó
                    onSelectSlot={handleSelectSlot}
                    selectable
                    style={{ height: 620 }}
                    messages={defaultMessages}
                    eventPropGetter={(event:any) => {                          // xử lí màu sắc cho từng event
                        let backgroundColor:string ;                    // mặc định là màu xám
                        switch(event.appointmentDetail?.status){        // xử lí màu sắc cho từng trạng thái
                            case 0:                                     // 0: chờ xác nhận
                                backgroundColor = '#818181';
                                break;
                            case 1:                                     // 1: đã xác nhận
                                backgroundColor = '#F3A638';
                                break;
                            default:                                    // 2: đã hoàn thành
                                backgroundColor = '#3174ad';
                                break;
                        }
                        return { style: { backgroundColor} }
                    }}
                />
            </CommonSpin>
            {visibleModalCreate ?
                <AppointmentsModalCreate
                    dataDetail={currentAppointment}
                    modalStatus={visibleModalCreate}
                    handleOk={handleOk}
                    handleCancel={()=>{setVisibleModalCreate(false); setCurrentAppointment(undefined);}}
                />
                :<></>
            }
        </div>
    )
}

export default AppointmentsCalendar