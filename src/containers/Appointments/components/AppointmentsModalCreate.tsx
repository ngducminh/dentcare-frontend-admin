import React, { useCallback, useEffect, useState } from 'react'
import {Row, Col, Modal, Form, notification} from 'antd';
import { useTranslation } from 'react-i18next';
import { useWatch } from 'antd/es/form/Form';
import moment from 'moment';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { ReactComponent as UpdateIcon } from '../../../resources/images/update_icon.svg';
import { ReactComponent as AddIcon } from '../../../resources/images/plus.svg';
import { ReactComponent as DeleteIcon } from '../../../resources/images/delete.svg';

import CommonForm from '../../../components/Common/Form';
import CommonFormItem from '../../../components/Common/FormItem';
import CommonButton from '../../../components/Common/Button';
import { APPOINTMENT_PAGE_STATUS, DATE_TIME_FORMAT } from '../../../utils/constants';
import { isDoctorMedical } from '../../../utils/utilFunctions';

import appointmentServices from "../../../services/appointments.service";
import userServices from "../../../services/users.service";
import serviceServices from "../../../services/medicalServices.service";
import medicalServices from "../../../services/medicals.service";
import sendMailServices from "../../../services/sendMails.service";

interface AppointmentsModalCreateProps {
    dataDetail: any,
    modalStatus: boolean,
    handleOk: () => void,
    handleCancel: () => void,
}

function AppointmentsModalCreate(props:AppointmentsModalCreateProps) {
    const {modalStatus, handleOk, handleCancel, dataDetail} = props
    const [form] = Form.useForm();
    const { t } = useTranslation();
    const navigate = useNavigate();
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [isLoadingMedical, setIsLoadingMedical] = useState<boolean>(false);
    const [dataDoctor, setDataDoctor] = useState<any[]>([]);
    const [dataCustomer, setDataCustomer] = useState<any[]>([]);
    const [dataService, setDataService] = useState<any[]>([]);
    const [selectedDate, setSelectedDate] = useState();
    const [listAppointment, setListAppointment] = useState<string[]>([]);
    const customerId = useWatch("customer",form);
    const doctorId = useWatch("doctor",form);
    const {profile} = useSelector((state:any) => state?.profileReducer);

    //select bác sĩ
    const getDataDoctor = async () => {
        const paramsSearch = {
            page: 0,
            size: 1000,
            search: JSON.stringify({
                searchType: 0,
            }),
        }
        const resp = await userServices.getListAccounts(paramsSearch);
        const data = resp?.data;
        if (resp?.status === 200) {
            setDataDoctor(data?.data?.content)
        } else {
            setDataDoctor([])
        }
    }

    //select khách hàng
    const getDataCustomer = async () => {
        const paramsSearch = {
            page: 0,
            size: 1000,
            search: JSON.stringify({
                searchType: 2,
            }),
        }
        const resp = await userServices.getListAccounts(paramsSearch);
        const data = resp?.data;
        if (resp?.status === 200) {
            setDataCustomer(data?.data?.content)
        } else {
            setDataCustomer([])
        }
    }

    //select dịch vụ
    const getDataService = async () => {
        const paramsSearch = {
            page: 0,
            size: 1000,
            search: JSON.stringify({})
        }
        const resp = await serviceServices.getListServices(paramsSearch);
        const data = resp?.data;
        if (resp?.status === 200) {
            setDataService(data?.data?.content)
        } else {
            setDataService([])
        }
    }

    // gọi list appointment 
    const getListAppointment = useCallback(async () => { // lấy danh sách lịch hẹn
        if(doctorId){ // nếu có chọn bác sĩ
            const paramsSearch = {              // gọi api lấy danh sách lịch hẹn
                page: 0,
                size: 1000,
                search: JSON.stringify({doctorId: doctorId})                // lọc theo bác sĩ
            }
            const resp = await appointmentServices.getListAppointments(paramsSearch);           // gọi api lấy danh sách lịch hẹn
            const data = resp?.data;
            if (resp?.status === 200) {        // nếu thành công
                setListAppointment(data?.data?.content?.filter((item:any)=> (item?.status === 0 || item?.status === 1 || item?.status === 2)&&(item?.id !== dataDetail?.id)) // lọc ra các lịch hẹn có trạng thái 0,1,2 và khác lịch hẹn đang sửa
                                ?.map((item:any)=>  moment(item?.bookingDate).format(DATE_TIME_FORMAT))) // chuyển các lịch hẹn về dạng string
            } else {
                setListAppointment([]) // nếu thất bại thì set list về rỗng
            }
        }else{
            setListAppointment([])
        }
    },[dataDetail?.id, doctorId]) // nếu có thay đổi id bác sĩ thì gọi lại api

    useEffect(()=>{  // gọi api lấy danh sách lịch hẹn
        getListAppointment();  // gọi hàm getListAppointment
    },[getListAppointment]) // nếu có thay đổi id bác sĩ thì gọi lại api

    useEffect(() => {
        getDataDoctor();
        getDataCustomer();
        getDataService();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useEffect(()=>{
        const currentCustomer = dataCustomer?.find((item: any)=> item?.id===customerId) // tìm ra khách hàng hiện tại theo id
        form.setFieldsValue({                                   // select khách hàng thì auto fill email và phone number
            email: currentCustomer?.email,
            phoneNumber: currentCustomer?.phoneNumber,
        })
    },[customerId, dataCustomer, form])                         // nếu có thay đổi id khách hàng thì gọi lại useEffect

    const onChangeDate = (values:any) => {                      // xử lí khi thay đổi ngày giờ
        console.log(moment(values).format(DATE_TIME_FORMAT),listAppointment)                                

        const now = moment(values)                           // lấy ngày giờ mà user    nhập                    
        const there = moment(listAppointment[0], DATE_TIME_FORMAT)              // lấy ngày giờ đầu tiên trong listAppointment
        console.log('values ', moment(values).toDate());
        console.log('values 1 ',  moment(listAppointment[0], DATE_TIME_FORMAT).toDate());
        console.log('date diff ', moment.duration(now.diff(there)).asHours());

        for(var i =0; i<listAppointment.length; i++) {                  // duyệt qua listAppointment
            const first = moment(listAppointment[i], DATE_TIME_FORMAT)                  // lấy ngày giờ đầu tiên
            const second = moment(listAppointment[i+1], DATE_TIME_FORMAT)                   // lấy ngày giờ thứ 2

            console.log('diff 1 ', i + " " + moment.duration(now.diff(first)).asHours() )               // tính khoảng cách giữa ngày giờ hiện tại và ngày giờ đầu tiên
            console.log('diff 2 ', i + " " + Math.abs(moment.duration(now.diff(second)).asHours()))        // tính khoảng cách giữa ngày giờ hiện tại và ngày giờ thứ 2

            if(moment.duration(now.diff(first)).asHours() < 1 && Math.abs(moment.duration(now.diff(second)).asHours()) < 1){  
                // nếu khoảng cách giữa ngày giờ hiện tại và ngày giờ đầu tiên < 1 và khoảng cách giữa ngày giờ hiện tại và ngày giờ thứ 2 < 1      
                form.setFields([{            // set lỗi cho form
                    name: "bookingDate",
                    errors: [`${t("appointmentPage.form.warning")}!`],
                }]);
                return;
            }
        }
        
        // console.log('aaaa',  values) - moment(listAppointment[0]));
        // if(listAppointment.includes(moment(values).format(DATE_TIME_FORMAT))){
        //     form.setFields([{
        //         name: "bookingDate",
        //         errors: [`${t("appointmentPage.form.warning")}!`],
        //     }]);
        // }
    }

    const onFinish =  (values:any) => {                                                         // tạo lịch hẹn
        // if(listAppointment.includes(moment(values?.bookingDate).format(DATE_TIME_FORMAT))){
        //     form.setFields([{
        //         name: "bookingDate",
        //         errors: [`${t("appointmentPage.form.warning")}!`],
        //     }]);
        //     return;
        // }
        let check = true;                                                                       // biến check
        const now = moment(values?.bookingDate)                                                 // lấy thời gian mà ng dùng submit
        // const there = moment(listAppointment[0], DATE_TIME_FORMAT)                            
        // console.log('values ', moment(values).toDate());
        // console.log('values 1 ',  moment(listAppointment[0], DATE_TIME_FORMAT).toDate());
        // console.log('date diff ', moment.duration(now.diff(there)).asHours());

        for(var i =0; i<listAppointment.length; i++) {                                          // duyệt qua listAppointment
            const first = moment(listAppointment[i], DATE_TIME_FORMAT)                              // lấy ngày giờ đầu tiên
            const second = moment(listAppointment[i+1], DATE_TIME_FORMAT)                               // lấy ngày giờ thứ 2
            // console.log('update')

            // console.log('diff 1 ', i + " " + moment.duration(now.diff(first)).asHours() )
            // console.log('diff 2 ', i + " " + Math.abs(moment.duration(now.diff(second)).asHours()))

            if(Math.abs(moment.duration(now.diff(first)).asHours()) < 1 || Math.abs(moment.duration(now.diff(second)).asHours()) < 1){      
                // nếu khoảng cách giữa ngày giờ hiện tại và ngày giờ đầu tiên < 1 và khoảng cách giữa ngày giờ hiện tại và ngày giờ thứ 2 < 1
                form.setFields([{
                    name: "bookingDate",
                    errors: [`${t("appointmentPage.form.warning")}!`],
                }]);
                check = false;
                console.log("vao day");
                return;
            }
        }
        if(check) {
            console.log('add');
            onCreateAppointment(values);
        }
    }

    const onFinishFailed = (errorInfo:any) => {
        console.log('Failed:', errorInfo);
    };

    const onCreateAppointment = async (values: any) => {
        setIsLoading(true);
        const dataSubmit = {...values}                          // tạo ra 1 biến dataSubmit bằng values
        dataSubmit["admCustomer"] = { id: values?.customer};
        dataSubmit["admDoctor"] = { id: values?.doctor};
        dataSubmit["admStaff"] = { id: profile?.id};
        dataSubmit["services"] = values?.services?.map((item:any)=>({ id: item}))
        delete dataSubmit?.customer;
        delete dataSubmit?.doctor;
        delete dataSubmit?.email;
        delete dataSubmit?.phoneNumber;
        if(dataDetail?.id){                                                     // nếu có id thì là update
            if(!dataDetail?.admStaff?.id){                                      // nếu ko có id nhân viên thì set id nhân viên
                dataSubmit["admStaff"] = { id: profile?.id};            // nếu ko có id nhân viên thì set id nhân viên
            }
            const resp = await appointmentServices.updateAppointment({...dataDetail,...dataSubmit});    // gọi api update lịch hẹn
            const data = resp?.data;                    
            if (resp?.status === 200) {
                notification.success({
                    message: t('appointmentPage.message.editSuccess'),
                });
                handleOk();
            } else {
                notification.error({
                    message: data?.message || t('commonError.oopsSystem'),
                });
            }
        }else{
            dataSubmit["status"] = 0;                           // nếu ko có id thì là tạo mới
            console.log("values",dataSubmit)                                    
            const resp = await appointmentServices.createAppointment(dataSubmit);   // gọi api tạo mới lịch hẹn
            const data = resp?.data;
            if (resp?.status === 200) {
                notification.success({
                    message: t('appointmentPage.message.createSuccess'),
                });
                sendMailServices.createSendMail({
                    recipient: form.getFieldValue("email"),                         // gửi mail
                    subject: t('sendEmail.title'),
                    msgBody: `
                        ${t('sendEmail.createAppointment')} ${form.getFieldValue("appointmentCode")}
                        ${t('sendEmail.time')} ${moment(form.getFieldValue("bookingDate")).format(DATE_TIME_FORMAT)}
                        ${t('sendEmail.address')}
                        ${t('sendEmail.thankyou')}
                    `,
                });
                const currentDoctor = dataDoctor?.find((item:any)=> item?.id === form.getFieldValue("doctor"));
                const currentCustomer = dataCustomer?.find((item:any)=> item?.id === form.getFieldValue("customer"));
                if(currentDoctor && currentCustomer){
                    sendMailServices.createSendMail({
                        recipient: currentDoctor?.email,
                        subject: t('sendEmail.title'),
                        msgBody: `
                            ${t('sendEmail.doctor')} ${currentCustomer?.fullName} (${currentCustomer?.customerCode})
                            ${t('sendEmail.appointment')} ${form.getFieldValue("appointmentCode")}
                            ${t('sendEmail.time')} ${moment(form.getFieldValue("bookingDate")).format(DATE_TIME_FORMAT)}
                            ${t('sendEmail.address')}
                        `,
                    });
                }
                handleOk();
            } else {
                notification.error({
                    message: data?.message || t('commonError.oopsSystem'),
                });
            }
        }
        setIsLoading(false);
    }

    const onCancelAppointment = async (newStatus:number)=>{
        setIsLoading(true);
        let check:boolean = false;
        if(newStatus===1){
            if(!form.getFieldValue("doctor")){
                form.setFields([{
                    name: "doctor",
                    errors: [`${t("validate.select")} ${t("appointmentPage.form.label.doctor")}!`],
                }]);
                check = true;
            }
            if(!form.getFieldValue("services")?.length){
                form.setFields([{
                    name: "services",
                    errors: [`${t("validate.select")} ${t("appointmentPage.form.label.service")}!`],
                }]);
                check = true;
            }
        }
        if(!check){
            Modal.confirm({
                title: t('common.confirmAction'),
                content: t('common.editDataAction'),
                centered: true,
                okText: t('common.button.accept'),
                onOk: async () => { 
                    const dataSubmit = {
                        ...dataDetail,
                        status: newStatus,
                        admDoctor: { id: form.getFieldValue("doctor")},
                        services: form.getFieldValue("services")?.map((item:any)=>({ id: item}))
                    }
                    const resp = await appointmentServices.updateAppointment(dataSubmit);
                    const data = resp?.data;
                    if (resp?.status === 200) {
                        if(newStatus===4){
                            notification.success({
                                message: t('appointmentPage.message.cancelSuccess'),
                            });
                            sendMailServices.createSendMail({
                                recipient: dataDetail?.admCustomer?.email,
                                subject: t('sendEmail.title'),
                                msgBody: `
                                    ${t('sendEmail.cancelAppointment')}
                                    ${t('sendEmail.appointment')} ${dataDetail?.appointmentCode}
                                    ${t('sendEmail.time')} ${moment(dataDetail?.bookingDate).format(DATE_TIME_FORMAT)}
                                    ${t('sendEmail.address')}
                                    ${t('sendEmail.thankyou')}
                                `,
                            });
                            sendMailServices.createSendMail({
                                recipient: dataDetail?.admDoctor?.email,
                                subject: t('sendEmail.title'),
                                msgBody: `
                                    ${t('sendEmail.doctorCancel')} ${dataDetail?.admCustomer?.fullName} (${dataDetail?.admCustomer?.customerCode}) ${t('sendEmail.cancel')}
                                    ${t('sendEmail.appointment')} ${dataDetail?.appointmentCode}
                                    ${t('sendEmail.time')} ${moment(dataDetail?.bookingDate).format(DATE_TIME_FORMAT)}
                                    ${t('sendEmail.address')}
                                `,
                            });
                        }else if(newStatus===1){
                            notification.success({
                                message: t('appointmentPage.message.approvalSuccess'),
                            });
                        }
                        handleOk();
                    } else {
                        notification.error({
                            message: data?.message || t('commonError.oopsSystem'),
                        });
                    }
                },
                okButtonProps:{loading: isLoading},
                cancelText: t('common.button.cancel')
            });
        }
        
        setIsLoading(false);
    }
    
    const onCreateMedical = ()=>{
        setIsLoading(true);
        Modal.confirm({
            title: t('common.confirmAction'),
            content: t('common.editDataAction'),
            centered: true,
            okText: t('common.button.accept'),
            onOk: async () => {                                         // tạo mới bệnh án
                setIsLoadingMedical(true);                                  // loading
                const dataCreate = {
                    admAppointment: {                                           // lấy id lịch hẹn
                        id: dataDetail?.id                                               
                    },
                    medicalCode: `DentCare_MDC_${moment().unix()}`,         // tạo mã bệnh án
                    medicalDate: moment().toISOString(),
                    diagnostic: "",
                    note: "",
                    prescription: ""
                }
                console.log(dataCreate)
                const resp = await medicalServices.createMedical(dataCreate);
                const data = resp?.data;
                if (resp?.status === 200) {
                    const respAppointment = await appointmentServices.updateAppointment({...dataDetail, status: 2});        // cập nhật trạng thái lịch hẹn
                    if (respAppointment?.status === 200) {                      // nếu thành công
                        notification.success({
                            message: t('medicalPage.message.createSuccess'),
                        });
                        navigate(`/medicals/detail/${data?.data?.id}`, { replace: true })
                    } else {
                        notification.error({
                            message: data?.message || t('commonError.oopsSystem'),
                        });
                    }
                } else {
                    notification.error({
                        message: data?.message || t('commonError.oopsSystem'),
                    });
                }
                setIsLoadingMedical(false);
            },
            okButtonProps:{loading: isLoadingMedical},              // loading
            cancelText: t('common.button.cancel')                   // nút hủy
        });
        setIsLoading(false);
    }

    return <Modal
        className="appointment-modal-create"
        title={dataDetail?.id ? t("appointmentPage.form.editTitle") : t("appointmentPage.form.createTitle")}
        open={modalStatus}
        onCancel={handleCancel}
        maskClosable={false}
        footer={[
            <CommonButton
                key="close"
                onClick={handleCancel}
                size="small"
            >
                {t("common.button.close")}
            </CommonButton>,
            ((dataDetail?.status === 0 || dataDetail?.status === 1 )&& profile?.admUser?.permission !== 1) ? <CommonButton
                key="cancel"
                onClick={()=>onCancelAppointment(4)}
                btnType="danger"
                size="small"
                className="btn-icon-left"
            >
                <><DeleteIcon/> {t("common.button.cancel")}</>
            </CommonButton> : null,
            (dataDetail?.status === 1 && (isDoctorMedical(dataDetail?.admDoctor?.id, profile)||profile?.admUser?.permission !==1)) ? <CommonButton
                key="createMedical"
                onClick={onCreateMedical}
                btnType="info"
                size="small"
                className="btn-icon-left"
            >
                <><AddIcon/> {t("common.button.createMedical")}</>
            </CommonButton> : null,
            (dataDetail?.status === 0 && profile?.admUser?.permission !== 1) ? <CommonButton
                key="confirmMedical"
                onClick={()=>onCancelAppointment(1)}
                btnType="success"
                size="small"
                className="btn-icon-left"
            >
                <><AddIcon/> {t("common.button.approval")}</>
            </CommonButton> : null,
            (dataDetail?.status === 0 && profile?.admUser?.permission !== 1) ? <CommonButton
                form="myForm"
                key="update"
                htmlType="submit"
                btnType="primary"
                size="small"
                className="btn-icon-left"
                loading={isLoading}
            >
                <><UpdateIcon/> {t("common.button.update")}</>
            </CommonButton> : null,
            !dataDetail?.id ? <CommonButton
                form="myForm"
                key="submit"
                htmlType="submit"
                btnType="primary"
                size="small"
                className="btn-icon-left"
                loading={isLoading}
            >
                <><AddIcon/> {t("common.button.addNew")}</>
            </CommonButton> : null,
        ]}
    >
        <CommonForm
            form={form}
            id="myForm"
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            layout="vertical"
            initialValues={{
                appointmentCode: dataDetail?.appointmentCode ? dataDetail?.appointmentCode : `DentCare_APP_${moment().unix()}`,
                customer: dataDetail?.admCustomer?.id,
                doctor: dataDetail?.admDoctor?.id,
                services: dataDetail?.services?.map((item:any)=>item?.id),
                bookingDate: dataDetail?.bookingDate ? moment(dataDetail?.bookingDate) : undefined,
                note: dataDetail?.note,
                status: dataDetail?.status
            }}
            disabled={((dataDetail?.status === 0 || !dataDetail?.id) && profile?.admUser?.permission !== 1) ? false : true}
        >
            <Row gutter={20}>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("appointmentPage.form.label.appointmentCode") as string} 
                        name="appointmentCode"
                        placeholder={t("appointmentPage.form.placeholder.appointmentCode") as string}
                        disabled
                    />
                </Col>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("appointmentPage.form.label.customer") as string} 
                        name="customer"
                        placeholder={t("appointmentPage.form.placeholder.customer") as string}
                        rules={[
                            { required: true, message: `${t("validate.select")} ${t("appointmentPage.form.label.customer")}!` }
                        ]}
                        type="select"
                        options={dataCustomer?.map((item:any)=>({value: item?.id, label: item?.fullName}))}
                        showRequiredIcon
                    />
                </Col>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("appointmentPage.form.label.email") as string} 
                        name="email"
                        placeholder={t("appointmentPage.form.placeholder.email") as string}
                        disabled={true}
                    />
                </Col>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("appointmentPage.form.label.phoneNumber") as string}
                        name="phoneNumber"
                        placeholder={t("appointmentPage.form.placeholder.phoneNumber") as string}
                        disabled={true}
                        maxLength={10}
                    />
                </Col>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("appointmentPage.form.label.doctor") as string}
                        name="doctor" 
                        placeholder={t("appointmentPage.form.placeholder.doctor") as string}
                        rules={[
                            { required: true, message: `${t("validate.select")} ${t("appointmentPage.form.label.doctor")}!`},
                        ]}
                        showRequiredIcon
                        type="select"
                        options={dataDoctor?.map((item:any)=>({value: item?.id, label: item?.fullName}))}
                    />
                </Col>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("appointmentPage.form.label.service") as string}
                        name="services"
                        placeholder={t("appointmentPage.form.placeholder.service") as string}  
                        showRequiredIcon
                        type="select"
                        mode="multiple"
                        options={dataService?.map((item:any)=>({value: item?.id, label: item?.name}))}
                        rules={[
                            { required: true, message: `${t("validate.select")} ${t("appointmentPage.form.label.service")}!`},
                        ]}
                    />
                </Col>
                <Col span={12}>
                    <Row>
                        <Col span={24}>
                            <CommonFormItem
                                format={DATE_TIME_FORMAT}
                                showTime={{
                                    disabledHours: () => {
                                        let listDisableHours:number[] = [0,1,2,3,4,5,6,22,23];
                                        if(moment(selectedDate).unix() <= moment().unix()){
                                            listDisableHours=[...listDisableHours, ...Array.from(Array(moment().hours()).keys())]
                                        }
                                        return listDisableHours;
                                    },
                                    disabledMinutes: () => moment(selectedDate).unix() <= moment().unix() ? Array.from(Array(moment().minutes()).keys()) : [],
                                }}
                                showRequiredIcon={true}
                                label={t('appointmentPage.form.label.bookingDate')}
                                name="bookingDate"
                                type="datePicker"
                                disabledDate={(current:any) => current < moment().startOf("day").valueOf()}
                                onSelect={setSelectedDate}
                                placeholder={t('appointmentPage.form.placeholder.bookingDate') as string}
                                rules={[
                                    { required: true, message: `${t("validate.select")} ${t("appointmentPage.form.label.bookingDate")}!`},
                                ]}
                                onChange={onChangeDate}
                            />
                        </Col>
                        { dataDetail?.id ?
                            <Col span={24}>
                                <CommonFormItem 
                                    label={t("appointmentPage.form.label.status") as string}
                                    name="status"
                                    type='select'
                                    options={APPOINTMENT_PAGE_STATUS?.map((item:any)=>({value: item.value, label: t(item.label)}))}
                                    disabled={true}
                                />
                            </Col> : <></>
                        }
                    </Row>
                </Col>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("appointmentPage.form.label.note") as string}
                        name="note" 
                        type="textArea"
                        rows={5}
                        placeholder={t("appointmentPage.form.placeholder.note") as string}
                    />
                </Col>
                
            </Row>

        </CommonForm>
    </Modal>
}

export default AppointmentsModalCreate;

