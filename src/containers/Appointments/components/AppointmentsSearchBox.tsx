import React, { useEffect, useState } from 'react'
import {useNavigate} from "react-router-dom";
import {Row, Col, Form, Space} from "antd";
import { useTranslation } from 'react-i18next';
import {CaretDownOutlined, CaretUpOutlined} from "@ant-design/icons";
import moment from 'moment';

import { ReactComponent as Search } from '../../../resources/images/search-1.svg';

import {buildQueryString} from '../../../utils/utilFunctions'
import {useQuery} from '../../../utils/customHooks'
import CommonForm from "../../../components/Common/Form";
import CommonFormItem from '../../../components/Common/FormItem';
import CommonButton from './../../../components/Common/Button';
import { APPOINTMENT_PAGE_STATUS, DATE_FORMAT, DEFAULT_PAGE_SIZE } from '../../../utils/constants';
import CommonInput from '../../../components/Common/Input';
import CommonRangePicker from '../../../components/Common/RangePicker';

import userServices from "../../../services/users.service";
import medicalServices from "../../../services/medicalServices.service";

function AppointmentsSearchBox(props:any) {
    const componentPath = props?.componentPath;
    const navigate = useNavigate();
    const queryObj:any = useQuery();
    const {params = {}, search} = queryObj;
    const {
        advance: advanceQuery,
        pageSize: pageSizeQuery,
        sortBy: sortByQuery,
        sortType: sortTypeQuery,
        search: searchQuery,
    } = params;
    const { t } = useTranslation();
    const [form] = Form.useForm();
    const [advance, setAdvance] = useState(advanceQuery as boolean || false);
    const searchQueryData = searchQuery ? JSON.parse(searchQuery) : {}
    const pageSize = pageSizeQuery ? parseFloat(pageSizeQuery) : DEFAULT_PAGE_SIZE;
    const [dataSelectDoctor, setDataSelectDoctor] = useState<{value:any, label: string}[]>([]);
    const [dataSelectCustomer, setDataSelectCustomer] = useState<{value:any, label: string}[]>([]);
    const [dataSelectService, setDataSelectService] = useState<{value:any, label: string}[]>([]);

    const getDataDoctor = async () => {
        const paramsSearch = {
            page: 0,
            size: 1000,
            search: JSON.stringify({
                searchType: 0,
            }),
        }
        const resp = await userServices.getListAccounts(paramsSearch);
        const data = resp?.data;
        if (resp?.status === 200) {
            setDataSelectDoctor(data?.data?.content?.map((item:any)=>({value: item?.id, label: item?.fullName})))
        } else {
            setDataSelectDoctor([])
        }
    }

    const getDataCustomer = async () => {
        const paramsSearch = {
            page: 0,
            size: 1000,
            search: JSON.stringify({
                searchType: 2,
            }),
        }
        const resp = await userServices.getListAccounts(paramsSearch);
        const data = resp?.data;
        if (resp?.status === 200) {
            setDataSelectCustomer(data?.data?.content?.map((item:any)=>({value: item?.id, label: item?.fullName})))
        } else {
            setDataSelectCustomer([])
        }
    }

    const getDataService = async () => {
        const paramsSearch = {
            page: 0,
            size: 1000,
            search: JSON.stringify({})
        }
        const resp = await medicalServices.getListServices(paramsSearch);
        const data = resp?.data;
        if (resp?.status === 200) {
            setDataSelectService(data?.data?.content?.map((item:any)=>({value: item?.id, label: item?.name})))
        } else {
            setDataSelectService([])
        }
    }

    useEffect(() => {
        getDataDoctor();
        getDataCustomer();
        getDataService();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])


    useEffect(() => {
        setAdvance(advanceQuery === 'true')
    }, [advanceQuery])

    const onFinish = (values: any) => {
        onSearch(values)
    }

    const onClear = (clearParams = {}) => {
        onSearch({...clearParams},true)
        setTimeout(() => {
            form.resetFields();
        }, 100)
    }

    const onSearch = (data:any = {},isClear?:boolean) => {
        const dataSearch = {...data};
        if(data?.modifiedDate?.length){
            dataSearch["modifiedDateFromLong"] = moment(data?.modifiedDate?.[0]).startOf("day").valueOf();
            dataSearch["modifiedDateToLong"] = moment(data?.modifiedDate?.[1]).endOf("day").valueOf();
        }
        if(data?.bookingDate?.length){
            dataSearch["bookingDateFromLong"] = moment(data?.bookingDate?.[0]).startOf("day").valueOf();
            dataSearch["bookingDateToLong"] = moment(data?.bookingDate?.[1]).endOf("day").valueOf();
        }
        delete dataSearch.advance;
        delete dataSearch?.modifiedDate;
        delete dataSearch?.bookingDate;
        let queryString ={ 
            advance, 
            page: 1,
            pageSize: pageSize,
            sortBy: sortByQuery,
            sortType: sortTypeQuery,
            search: JSON.stringify(dataSearch||{}),
        }
        if(isClear){
            queryString.advance = data.advance;
        }
        if (queryString !== search) {
            navigate(`${componentPath}${buildQueryString(queryString) || ''}`)
        } else {
            if (props?.getData) props?.getData()
        }
    }

    return <div className="avic-search-box">
            {
                !advance ?
                <div className="normal-search-box">
                    <CommonForm
                        form={form}
                        onFinish={onFinish}
                        layout="horizontal"
                        initialValues={{
                            keyword: searchQueryData?.keyword,
                        }}
                    >
                        <CommonFormItem name="keyword">
                            <CommonInput
                                placeholder={t("appointmentPage.search.placeholder.advancedSearch") as string}
                                prefix={
                                    <div
                                        onClick={() => onClear({advance: true})}
                                        className="open-advance-search-btn"
                                    >
                                        {t("appointmentPage.search.label.advancedSearch")} <CaretDownOutlined />
                                    </div>
                                }
                                addonAfter={
                                    <div onClick={() => form.submit()} className="btn-normal-search">
                                        <Search />
                                    </div>
                                }
                            />
                        </CommonFormItem>
                    </CommonForm>
                </div>
                :
                <div className="advance-search-box">
                    <div className="close-advance-search-btn" >
                        <div onClick={() => onClear({advance: false})} >
                            {t("appointmentPage.search.title")}
                        </div>
                        <Space>
                            <CaretUpOutlined className="cursor-pointer"  onClick={() =>  onClear({advance: false}) } />
                        </Space>
                    </div>
                    <CommonForm
                        form={form}
                        onFinish={onFinish}
                        layout="vertical"
                        initialValues={{
                            appointmentCode: searchQueryData?.appointmentCode,
                            customerId: searchQueryData?.customerId,
                            doctorId: searchQueryData?.doctorId,
                            serviceId: searchQueryData?.serviceId,
                            bookingDate: (searchQueryData?.bookingDateFromLong && searchQueryData?.bookingDateToLong) ? [moment(searchQueryData?.bookingDateFromLong),moment(searchQueryData?.bookingDateToLong)] : [],
                            modifiedDate: (searchQueryData?.modifiedDateFromLong && searchQueryData?.modifiedDateToLong) ? [moment(searchQueryData?.modifiedDateFromLong),moment(searchQueryData?.modifiedDateToLong)] : [],
                            modifiedBy: searchQueryData?.modifiedBy,
                            status: searchQueryData?.status,
                        }}
                    >
                        <Row gutter={30}>
                            <Col span={12}>
                                <CommonFormItem 
                                    label={t("appointmentPage.search.label.appointmentCode")}
                                    placeholder={t("appointmentPage.search.placeholder.appointmentCode") as string}
                                    name="appointmentCode"
                                />
                            </Col>
                            <Col span={12}>
                                <CommonFormItem 
                                    label={t("appointmentPage.search.label.customerName")}
                                    placeholder={t("appointmentPage.search.placeholder.customerName") as string}
                                    name="customerId"
                                    type='select'
                                    options={dataSelectCustomer}
                                />
                            </Col>
                        </Row>

                        <Row gutter={30}>
                            <Col span={12}>
                                <CommonFormItem 
                                    label={t("appointmentPage.search.label.doctor")}
                                    name="doctorId"
                                    placeholder={t("appointmentPage.search.placeholder.doctor") as string}
                                    type='select'
                                    options={dataSelectDoctor}
                                />
                            </Col>
                            <Col span={12}>
                                <CommonFormItem 
                                    label={t("appointmentPage.search.label.service")}
                                    type= "select"
                                    name="serviceId"
                                    placeholder={t("appointmentPage.search.placeholder.service") as string}
                                    options={dataSelectService}
                                />
                            </Col>
                        </Row>

                        <Row gutter={30}>
                            <Col span={12}>
                                <CommonFormItem  
                                    name="bookingDate"
                                    label={t("appointmentPage.search.label.bookingDate")}
                                    format={DATE_FORMAT}
                                    placeholder={[t("common.fromDate"),t("common.toDate")]}
                                    type="rangePicker"
                                />
                            </Col>
                            <Col span={12}>
                                <CommonFormItem  
                                    name="modifiedDate"
                                    label={t("appointmentPage.search.label.modifiedDate")}
                                >
                                    <CommonRangePicker />
                                </CommonFormItem>
                            </Col>
                        </Row>

                        <Row gutter={30}>
                            <Col span={12}>
                                <CommonFormItem 
                                    label={t("appointmentPage.search.label.modifiedBy")}
                                    name="modifiedBy"
                                    placeholder={t("appointmentPage.search.placeholder.modifiedBy") as string}
                                />
                            </Col>
                            <Col span={12}>
                                <CommonFormItem 
                                    label={t("appointmentPage.search.label.status")}
                                    type= "select"
                                    name="status"
                                    placeholder={t("appointmentPage.search.placeholder.status") as string}
                                    options={APPOINTMENT_PAGE_STATUS?.map((item:any)=>({value: item.value, label: t(item.label)}))}
                                />
                            </Col>
                        </Row>

                        <Space className="form-btn-container">
                            <CommonButton btnType="default" size={'small'} onClick={()=> onClear({advance: true})}>
                                {t("common.button.deleteCondition")}
                            </CommonButton>
                            <CommonButton btnType="primary" size={'small'} htmlType="submit">
                                {t("layout.search")}
                            </CommonButton>
                        </Space>
                    </CommonForm>
                </div>
            }
            </div>
}

export default AppointmentsSearchBox;

