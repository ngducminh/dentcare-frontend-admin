import React, { useCallback, useState, useMemo, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { ExportOutlined } from '@ant-design/icons';
import moment from "moment";
import { Modal, Space, notification } from "antd";
import { saveAs } from 'file-saver';
import { useSelector } from "react-redux";

import { ReactComponent as IconAdd } from '../../resources/images/plus.svg';
import { ReactComponent as IconList } from '../../resources/images/list_icon.svg';
import { ReactComponent as IconGrid } from '../../resources/images/gird_icon.svg';
import { ReactComponent as IconAction } from '../../resources/images/action_icon.svg';

import AppointmentsCalendar from "./components/AppointmentsCalendar";
import AppointmentsSearchBox from "./components/AppointmentsSearchBox";
import { useQuery } from "../../utils/customHooks";
import { DATE_TIME_FORMAT_SECOND, DEFAULT_PAGE_SIZE, APPOINTMENT_PAGE_STATUS, DATE_TIME_FORMAT } from "../../utils/constants";
import CommonButton from "../../components/Common/Button";
import CommonDropdown from "../../components/Common/Dropdown";
import CommonTag from "../../components/Common/Tag";
import { buildQueryString } from "../../utils/utilFunctions";
import CommonTable from "../../components/Common/Table";
import AppointmentsModalCreate from "./components/AppointmentsModalCreate";
import CommonSelect from './../../components/Common/Select';

import appointmentServices from "../../services/appointments.service";
import userServices from "../../services/users.service";

function Appointments(){
    const componentPath = '/appointments'
    const navigate = useNavigate();
    const { t } = useTranslation();
    const queryObj:any = useQuery();
    const {params = {}, search} = queryObj;
    const {
        page: pageQuery,
        pageSize: pageSizeQuery,
        sortBy: sortByQuery,
        sortType: sortTypeQuery,
        search: searchQuery,
    } = params
    const page = pageQuery ? parseFloat(pageQuery) : 1;
    const pageSize = pageSizeQuery ? parseFloat(pageSizeQuery) : DEFAULT_PAGE_SIZE;
    
    const {profile} = useSelector((state:any) => state?.profileReducer);
    const [data, setData] = useState<any>();
    const [visibleModalCreate, setVisibleModalCreate] = useState<boolean>(false);
    const [showListGird, setShowListGird] = useState<boolean>(false);
    const [isLoading, setIsLoading] = useState(false);
    const [dataSelected, setDataSelected] = useState<number[]>([]);
    const [isLoadingExport, setIsLoadingExport] = useState(false);
    const [currentAppointment, setCurrentAppointment] = useState<any>(undefined)
    const [expandedRowKeys, setExpandedRowKeys] = useState<number[]>([]);
    const [dataSelectDoctor, setDataSelectDoctor] = useState<{value:any, label: string}[]>([]);
    const [doctorId, setDoctorId] = useState<number|undefined>(undefined);

    const getData = useCallback(async (reload?: boolean) => {
        setIsLoading(true);
        const newSearch = searchQuery ? JSON.parse(searchQuery) : {};
        const paramsSearch = {
            page: reload? 0 : page-1,
            size: pageSize,
            sortBy: sortByQuery,
            sortType: sortTypeQuery,
            search: JSON.stringify({...newSearch})
        }
        const resp = await appointmentServices.getListAppointments(paramsSearch);
        const data = resp?.data;
        if (resp?.status === 200) {
            setData(data?.data)
        } else {
            notification.error({
                message: data?.message || t('commonError.oopsSystem'),
            });
        }

        setIsLoading(false);
    },[page, pageSize, searchQuery, sortByQuery, sortTypeQuery, t])

    useEffect(() => {
        getData()
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [search])

    
    const getDataDoctor = async () => {
        const paramsSearch = {
            page: 0,
            size: 1000,
            search: JSON.stringify({
                searchType: 0,
            }),
        }
        const resp = await userServices.getListAccounts(paramsSearch);
        const data = resp?.data;
        if (resp?.status === 200) {
            setDataSelectDoctor(data?.data?.content?.map((item:any)=>({value: item?.id, label: item?.fullName})))
        } else {
            setDataSelectDoctor([])
        }
    }

    useEffect(() => {
        getDataDoctor();
    }, [])

    const onPageChange = (pagination:any, filters:any, sorter:any) => {
        let queryString = buildQueryString({
            ...params,
            page: pageSize === pagination?.pageSize ? pagination?.current : 1,
            pageSize: pagination?.pageSize,
            sortBy: sorter?.order ? sorter?.field : '',
            sortType: sorter?.order ? sorter?.order === 'ascend' ? 'asc' : 'desc' : ''
        })
        navigate(`${componentPath}${queryString || ''}`)
    }

    // rowSelection objects indicates the need for row selection
    const rowSelection = {
        onChange: (selectedRowKeys:any, selectedRows:any) => {
            setDataSelected(selectedRowKeys);
            console.log('onChange rowSelection', selectedRowKeys, selectedRows);
        },
        onSelect: (record:any, selected:any, selectedRows:any) => {
            console.log('onChange onSelect', record, selected, selectedRows);
        },
        onSelectAll: (selected:any, selectedRows:any, changeRows:any) => {
            console.log('onChange onSelectAll', selected, selectedRows, changeRows);
        },
        getCheckboxProps: (record:any) => {
            if(record?.status !== 0 && record?.status !== 1){
                return {
                    disabled: true 
                }
            }
            return {
                disabled: false 
            }
        }
    };

    const handleOk = ()=>{
        getData(true);
        setVisibleModalCreate(false);
        setCurrentAppointment(undefined)
    }

    // Thao tác xóa, đổi trạng thái
    const updateDepartments = useCallback((type:any, value?:any) => {
        if (dataSelected?.length === 0) {
            return
        }
        Modal.confirm({
            title: t('common.confirmAction'),
            okText: t('common.button.accept'),
            onOk: async () => {
                let resp;
                if(type === 'approval'){
                    resp = await appointmentServices.approvalAppointments(dataSelected)
                }else{
                    resp = await appointmentServices.cancelAppointments(dataSelected)
                }
                const data = resp?.data;
                if (resp?.status === 200) {
                    if(type === 'approval'){
                        notification.success({
                            message: t('appointmentPage.message.approvalSuccess'),
                        });
                    }else{
                        notification.success({
                            message: t('appointmentPage.message.cancelSuccess'),
                        });
                    }
                    setDataSelected([]);
                    setExpandedRowKeys([]);
                    getData();
                } else {
                    notification.error({
                        message: data?.message || t('commonError.oopsSystem'),
                    });
                }
            },
            onCancel: () => {
                console.log('Cancel');
            },
            cancelText: t('common.button.cancel')
        })
    },[dataSelected, getData, t])

    // xuất excel
    const exportData = async () => {
        setIsLoadingExport(true);
        const newSearch = searchQuery ? JSON.parse(searchQuery) : {};
        const paramsSearch = {
            page: 0,
            size: 10000,
            sortBy: sortByQuery,
            sortType: sortTypeQuery,
            search: JSON.stringify({...newSearch})
        }
        const resp = await appointmentServices.exportAppointment(paramsSearch);
        const data = resp?.data;
        if (resp?.status === 200) {
            const fileName = `Appointments_Data_Export_${moment().format('YYYYMMDD')}_${moment().unix()}.xlsx`
            saveAs(data, fileName);             // saveAs là hàm của thư viện file-saver
        } else {
            notification.error({
                message: data?.message || t('commonError.oopsSystem'),
            });
        }
        
        setIsLoadingExport(false);
    }

    const items = useMemo(() => {
        return [
            {
                key: '1',
                label: <div onClick={()=>{updateDepartments("approval")}}>{`${t('common.button.approval')}`}</div>,
            },
            {
                key: '2',
                label:  <div onClick={()=>{updateDepartments("cancel")}}>{`${t('common.button.cancel')}`}</div>,
            },
        ];
    }, [t, updateDepartments])

    const columns = [
        {
            title: t("appointmentPage.list.columns.STT"),
            key: 'index',
            align: "center",
            render: (cell:any, record:any, index:number) => (page - 1) * pageSize + index + 1,
        },
        {
            title: t("appointmentPage.list.columns.appointmentCode"),
            dataIndex: 'appointmentCode',
            key: 'appointmentCode',
            render: (value:any, row:any) => {
                return <div className="link" onClick={()=>{setCurrentAppointment(row); setVisibleModalCreate(true)}}>{value || '--'}</div>;
            }
        },
        {
            title: t("appointmentPage.list.columns.customer"),
            dataIndex: 'admCustomer',
            key: 'admCustomer',
            render: (value:any, row:any) => value?.fullName
        },
        {
            title: t("appointmentPage.list.columns.doctor"),
            dataIndex: 'admDoctor',
            key: 'admDoctor',
            render: (value:any, row:any) => value?.fullName
        },
        {
            title: t("appointmentPage.list.columns.service"),
            dataIndex: 'services',
            key: 'services',
            render: (value:any, row:any) => value?.map((item:any)=> item?.name).join(", ") 
        },
        {
            title: t("appointmentPage.list.columns.bookingDate"),
            dataIndex: 'bookingDate',
            key: 'bookingDate',
            render:  (cell: any, record: any, index: number)=> cell ? moment(cell).format(DATE_TIME_FORMAT) : "",
        },
        {
            title: t("appointmentPage.list.columns.note"),
            dataIndex: 'note',
            key: 'note',
        },
        {
            title: t("appointmentPage.list.columns.modifiedDate"),
            dataIndex: 'modifiedDate',
            key: 'modifiedDate',
            render:  (cell: any, record: any, index: number)=> cell ? moment(cell).format(DATE_TIME_FORMAT_SECOND) : "",
        },
        {
            title: t("appointmentPage.list.columns.modifiedBy"),
            dataIndex: 'modifiedBy',
            key: 'modifiedBy',
        },
        {
            title: t("appointmentPage.list.columns.status"),
            dataIndex: 'status',
            key: 'status',
            render: (value:any, cell:any) => {
                const curStatus:any = APPOINTMENT_PAGE_STATUS.find((x:any) => x.value === value) || {}
                return <CommonTag tagType={curStatus?.type}>{t(curStatus?.label)}</CommonTag>
            },
        },
    ];
    return <>
        {!showListGird ? <AppointmentsSearchBox getData={()=>{}} componentPath={componentPath}/> : <></>}
        
        <div className="avic-table-top">
            <div className="avic-table-top-title">
                {t("appointmentPage.list.title")}
            </div>
            { showListGird ?
                <CommonSelect
                    className="width-350"
                    options={dataSelectDoctor}
                    onChange={(value) => setDoctorId(value)}
                    placeholder={t("appointmentPage.form.placeholder.doctor")}
                    allowClear
                    showSearch
                /> : <></>
            }

            {/* Big calendar thư viện ngoài*/}
            <Space className="avic-table-top-right"> 
                <IconList className={`icon_list ${showListGird ? "" : "icon_list-active"}`} onClick={()=>{setShowListGird(false)}}/>
                <IconGrid className={`icon_list ${showListGird ? "icon_list-active" : ""}`} onClick={()=>{setShowListGird(true)}}/>
                {   profile?.admUser?.permission !== 1 ?
                    <>
                        <CommonButton icon={<ExportOutlined />} btnType="default" size={'small'} onClick={()=>{exportData()}} loading={isLoadingExport}>
                            {t("common.button.exportExcel")}
                        </CommonButton>
                        {!showListGird ? <CommonDropdown menu={{ items }}>
                                            <CommonButton btnType="primary" size={'small'} className="btn-icon-right">
                                                {t("common.button.action")}<IconAction />
                                            </CommonButton>
                                        </CommonDropdown> : <></>
                        }
                        <CommonButton btnType="primary" size={'small'} className="btn-icon-left" onClick={()=>{setVisibleModalCreate(true)}}>
                            <IconAdd />{t("common.button.addNew")}
                        </CommonButton>
                    </> : <></>
                }
            </Space>
        </div>

        {!showListGird ? <CommonTable
                rowKey={'id'}
                loading={isLoading}
                dataSource={data?.content || []}
                columns={columns}
                data={data}
                onChange={onPageChange}
                rowSelection={{...rowSelection, selectedRowKeys: dataSelected,checkStrictly: false,}}
                defaultSorter={{
                    order: sortTypeQuery,
                    field: sortByQuery,
                }}
                expandable={{
                    expandedRowKeys: expandedRowKeys,
                    onExpandedRowsChange: (expandedRows: any) => {
                        setExpandedRowKeys(expandedRows)
                    }
                }}
            /> : <AppointmentsCalendar doctorId={doctorId}/>
        }

        {visibleModalCreate ?
            <AppointmentsModalCreate
                dataDetail={currentAppointment}
                modalStatus={visibleModalCreate}
                handleOk={handleOk}
                handleCancel={()=>{setVisibleModalCreate(false); setCurrentAppointment(undefined);}}
            />
            :<></>
        }
    </>
}

export default Appointments;