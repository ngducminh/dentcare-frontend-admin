import React, { useCallback, useEffect, useState } from 'react'
import { Modal, Form, notification, Tabs, TabsProps} from 'antd';
import { useTranslation } from 'react-i18next';

import { ReactComponent as UpdateIcon } from '../../../resources/images/update_icon.svg';
import { ReactComponent as AddIcon } from '../../../resources/images/plus.svg';
import { ReactComponent as DeleteIcon } from '../../../resources/images/delete.svg';

import CommonButton from '../../../components/Common/Button';
import DoctorInfoTab from './DoctorInfoTab';
import AccountInfoTab from './AccountInfoTab';

import userServices from "../../../services/users.service";

interface DoctorsCreateModalProps {
    dataDetail: any,
    modalStatus: boolean,
    handleOk: () => void,
    handleCancel: () => void
}

function DoctorsCreateModal(props:DoctorsCreateModalProps) {
    const [activeKey, setActiveKey] = useState<string>("1");
    const [items, setItems] = useState<TabsProps["items"]>([]);
    const [formAccountInfo] = Form.useForm();
    const [formDoctorInfo] = Form.useForm();
    const {modalStatus, handleOk, handleCancel, dataDetail} = props;
    const { t } = useTranslation();
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [isSubmitAccountSuccess, setIsSubmitAccountSuccess] = useState<boolean>(false);
    const [isSubmitDoctorSuccess, setIsSubmitDoctorSuccess] = useState<boolean>(false);

    const onSubmit = ()=>{ 
        setIsLoading(true);
        formAccountInfo.submit();
        formDoctorInfo.submit();
        setIsLoading(false)
    }

    const onCreateDoctor = useCallback(async () => {
        setIsLoading(true);
        if(dataDetail?.id){ //Chỉnh sửa bác sĩ
            const resp = await userServices.updateUser({...dataDetail?.admUser, ...formAccountInfo.getFieldsValue()});
            const data = resp?.data;
            if (resp?.status === 200) {
                const dataSubmit={
                    user: {...dataDetail, ...formDoctorInfo.getFieldsValue()},
                    ex: formDoctorInfo.getFieldValue("specialize")?.map((item:any)=>item.experience),
                    specializes: formDoctorInfo.getFieldValue("specialize")?.map((item:any)=>({id: item.specializeId})),
                }
                delete dataSubmit?.user?.admDoctorSpecializes;
                delete dataSubmit?.user?.specialize;
                console.log("dataSubmit", dataSubmit)
                const respDoctor = await userServices.updateDoctor(dataSubmit);
                const dataDoctor = respDoctor?.data;
                if (respDoctor?.status === 200) {
                    notification.success({
                        message: t('doctorPage.message.editSuccess'),
                    });
                    handleOk();
                } else {
                    notification.error({
                        message: dataDoctor?.message || t('commonError.oopsSystem'),
                    });
                    setIsSubmitAccountSuccess(false);
                    setIsSubmitDoctorSuccess(false);
                    setIsLoading(false);
                }
            } else {
                notification.error({
                    message: data?.message || t('commonError.oopsSystem'),
                });
                setIsSubmitAccountSuccess(false);
                setIsSubmitDoctorSuccess(false);
                setIsLoading(false);
            }
        }else{ // Thêm mới bác sĩ
            const resp = await userServices.createUser({...formAccountInfo.getFieldsValue()});
            const data = resp?.data;
            if (resp?.status === 200) {
                const dataSubmit={
                    user: {...formDoctorInfo.getFieldsValue() , admUser: {id: data?.data?.id}},
                    ex: formDoctorInfo.getFieldValue("specialize")?.map((item:any)=>item.experience),
                    specializes: formDoctorInfo.getFieldValue("specialize")?.map((item:any)=>({id: item.specializeId})),
                }
                delete dataSubmit?.user?.specialize;
                const respDoctor = await userServices.createDoctor(dataSubmit);
                const dataDoctor = respDoctor?.data;
                if (respDoctor?.status === 200) {
                    notification.success({
                        message: t('doctorPage.message.createSuccess'),
                    });
                    handleOk();
                } else {
                    notification.error({
                        message: dataDoctor?.message || t('commonError.oopsSystem'),
                    });
                    setIsSubmitAccountSuccess(false);
                    setIsSubmitDoctorSuccess(false);
                    setIsLoading(false);
                }
            } else {
                notification.error({
                    message: data?.message || t('commonError.oopsSystem'),
                });
                setIsSubmitAccountSuccess(false);
                setIsSubmitDoctorSuccess(false);
                setIsLoading(false);
            }
        }
        setIsSubmitAccountSuccess(false);
        setIsSubmitDoctorSuccess(false);
        setIsLoading(false);
    },[dataDetail, formAccountInfo, formDoctorInfo, handleOk, t])

    useEffect(()=>{
        console.log("isSubmitAccountSuccess", isSubmitAccountSuccess);
        console.log("isSubmitDoctorSuccess", isSubmitDoctorSuccess);
        if(isSubmitAccountSuccess && isSubmitDoctorSuccess){
            onCreateDoctor();
        }
        if(!isSubmitDoctorSuccess){
            setActiveKey("1")
        }else if(!isSubmitAccountSuccess){
            setActiveKey("2")
        }
    },[isSubmitAccountSuccess, isSubmitDoctorSuccess, onCreateDoctor])

    // Xóa bác sĩ
    const onDelete = async ()=>{
        setIsLoading(true);
        Modal.confirm({
            title: t('common.confirmAction'),
            content: t('common.editDataAction'),
            centered: true,
            okText: t('common.button.accept'),
            onOk: async () => { 
                const respDoctor = await userServices.deleteDoctors([dataDetail?.id]);
                const dataDoctor = respDoctor?.data;
                if (respDoctor?.status === 200) {
                    const resp = await userServices.deleteUsers([dataDetail?.admUser?.id]);
                    const data = resp?.data;
                    if (resp?.status === 200) {
                        notification.success({
                            message: t('doctorPage.message.createSuccess'),
                        });
                        handleOk();
                    } else {
                        notification.error({
                            message: data?.message || t('commonError.oopsSystem'),
                        });
                    }
                } else {
                    notification.error({
                        message: dataDoctor?.message || t('commonError.oopsSystem'),
                    });
                }
            },
            okButtonProps:{loading: isLoading},
            cancelText: t('common.button.cancel')
        });
        setIsLoading(false);
    }

    // console.log("dataDetail",dataDetail)

    const onChangeTab = (key:string) => {
        setActiveKey(key);
    }

        // render tabs item
        useEffect(()=>{
            const listItem = [ {
                key: "1",
                label: t("doctorPage.form.tabs.doctorInfo") as string,
                closable: false,
                children: <DoctorInfoTab
                                dataDetail={dataDetail}
                                formDoctorInfo={formDoctorInfo}
                                onCreateDoctor={setIsSubmitDoctorSuccess}
                            />
            },
            {
                key: "2",
                label: t("doctorPage.form.tabs.accountInfo") as string,
                closable: false,
                children: <AccountInfoTab
                                dataDetail={dataDetail?.admUser}
                                formAccountInfo={formAccountInfo}
                                onCreateAccount={setIsSubmitAccountSuccess}
                            />
            }];
            setItems(listItem);
        },[dataDetail, formAccountInfo, formDoctorInfo, t]);

    return <Modal
        className="doctor-modal-create"
        title={dataDetail?.id ? t("doctorPage.form.editTitle") : t("doctorPage.form.createTitle")}
        open={modalStatus}
        maskClosable={false}
        onCancel={handleCancel}
        footer={[
            <CommonButton
                key="close"
                onClick={handleCancel}
                size="small"
            >
                {t("common.button.close")}
            </CommonButton>,
            dataDetail?.id ? <CommonButton
                key="remove"
                onClick={onDelete}
                btnType="danger"
                size="small"
                className="btn-icon-left"
            >
                <><DeleteIcon/> {t("common.button.remove")}</>
            </CommonButton> : null,
            <CommonButton
                key="submit"
                htmlType="submit"
                btnType="primary"
                size="small"
                onClick={onSubmit}
                className="btn-icon-left"
                loading={isLoading}
            >
                {dataDetail?.id ? <><UpdateIcon/> {t("common.button.update")}</> : <><AddIcon/> {t("common.button.addNew")}</>}
            </CommonButton>
        ]}
    >
        <Tabs 
            items={items} 
            activeKey={activeKey}
            onChange={onChangeTab}
        />
    </Modal>
}

export default DoctorsCreateModal;

