import React, { useEffect, useState } from 'react'
import {useNavigate} from "react-router-dom";
import {Row, Col, Form, Space} from "antd";
import { useTranslation } from 'react-i18next';
import {CaretDownOutlined, CaretUpOutlined} from "@ant-design/icons";
import moment from "moment";

import { ReactComponent as Search } from '../../../resources/images/search-1.svg';

import {buildQueryString} from '../../../utils/utilFunctions'
import {useQuery} from '../../../utils/customHooks'
import CommonForm from "../../../components/Common/Form";
import CommonFormItem from '../../../components/Common/FormItem';
import CommonButton from '../../../components/Common/Button';
import { DOCTOR_PAGE_STATUS, DEFAULT_PAGE_SIZE } from '../../../utils/constants';
import CommonInput from '../../../components/Common/Input';
import CommonRangePicker from '../../../components/Common/RangePicker';

import specializeServices from "../../../services/specializes.service";

function DoctorsSearchBox(props:any) {
    const componentPath = props?.componentPath
    const navigate = useNavigate();
    const queryObj:any = useQuery();
    const {params = {}, search} = queryObj;
    const {
        advance: advanceQuery,
        pageSize: pageSizeQuery,
        sortBy: sortByQuery,
        sortType: sortTypeQuery,
        search: searchQuery,
    } = params;
    const { t } = useTranslation();
    const [form] = Form.useForm();
    const [advance, setAdvance] = useState(advanceQuery as boolean || false);
    const searchQueryData = searchQuery ? JSON.parse(searchQuery) : {}
    const pageSize = pageSizeQuery ? parseFloat(pageSizeQuery) : DEFAULT_PAGE_SIZE;
    const [dataSelectSpecialize, setDataSelectSpecialize] = useState<{value:any, label: string}[]>([]);

    const getData = async () => {
        const resp = await specializeServices.findAllSpecialize();
        const data = resp?.data;
        if (resp?.status === 200) {
            setDataSelectSpecialize(data?.data?.filter((item:any)=> item?.isDelete === 0)?.map((item:any)=> ({value: item?.id, label: item?.name})))
        } else {
            setDataSelectSpecialize([])
        }
    }

    useEffect(() => {
        getData()
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useEffect(() => {
        setAdvance(advanceQuery === 'true')
    }, [advanceQuery])

    const onFinish = (values: any) => {
        onSearch(values)
    }

    const onClear = (clearParams = {}) => {
        onSearch({...clearParams},true)
        setTimeout(() => {
            form.resetFields();
        }, 100)
    }

    const onSearch = (data:any = {},isClear?:boolean) => {
        const dataSearch = {...data};
        if(data?.modifiedDate?.length){
            dataSearch["modifiedDateFromLong"] = moment(data?.modifiedDate?.[0]).startOf("day").valueOf();
            dataSearch["modifiedDateToLong"] = moment(data?.modifiedDate?.[1]).endOf("day").valueOf();
        }
        delete dataSearch.advance;
        delete dataSearch?.modifiedDate;
        let queryString ={ 
            advance, 
            page: 1,
            pageSize: pageSize,
            sortBy: sortByQuery,
            sortType: sortTypeQuery,
            search: JSON.stringify(dataSearch||{}),
        }
        if(isClear){
            queryString.advance = data.advance;
        }
        if (queryString !== search) {
            navigate(`${componentPath}${buildQueryString(queryString) || ''}`)
        } else {
            if (props?.getData) props?.getData()
        }
    }

    return <div className="avic-search-box">
            {
                !advance ?
                <div className="normal-search-box">
                    <CommonForm
                        form={form}
                        onFinish={onFinish}
                        layout="horizontal"
                        initialValues={{
                            keyword: searchQueryData?.keyword,
                        }}
                    >
                        <CommonFormItem name="keyword">
                            <CommonInput
                                placeholder={t("doctorPage.search.placeholder.advancedSearch") as string}
                                prefix={
                                    <div
                                        onClick={() => onClear({advance: true})}
                                        className="open-advance-search-btn"
                                    >
                                        {t("doctorPage.search.label.advancedSearch")} <CaretDownOutlined />
                                    </div>
                                }
                                addonAfter={
                                    <div onClick={() => form.submit()} className="btn-normal-search">
                                        <Search />
                                    </div>
                                }
                            />
                        </CommonFormItem>
                    </CommonForm>
                </div>
                :
                <div className="advance-search-box">
                    <div className="close-advance-search-btn" >
                        <div onClick={() => onClear({advance: false})} >
                            {t("doctorPage.search.title")}
                        </div>
                        <Space>
                            <CaretUpOutlined className="cursor-pointer"  onClick={() =>  onClear({advance: false}) } />
                        </Space>
                    </div>
                    <CommonForm
                        form={form}
                        onFinish={onFinish}
                        layout="vertical"
                        initialValues={{
                            doctorCode: searchQueryData?.doctorCode,
                            fullName: searchQueryData?.fullName,
                            email: searchQueryData?.email,
                            phoneNumber: searchQueryData?.phoneNumber,
                            username: searchQueryData?.username,
                            specializeId: searchQueryData?.specializeId,
                            modifiedBy: searchQueryData?.modifiedBy,
                            modifiedDate: (searchQueryData?.modifiedDateFromLong && searchQueryData?.modifiedDateToLong) ? [moment(searchQueryData?.modifiedDateFromLong),moment(searchQueryData?.modifiedDateToLong)] : [],
                            status: searchQueryData?.status,
                        }}
                    >
                        <Row gutter={30}>
                            <Col span={12}>
                                <CommonFormItem 
                                    label={t("doctorPage.search.label.doctorCode")}
                                    placeholder={t("doctorPage.search.placeholder.doctorCode") as string}
                                    name="doctorCode"
                                />
                            </Col>
                            <Col span={12}>
                                <CommonFormItem 
                                    label={t("doctorPage.search.label.fullName")}
                                    placeholder={t("doctorPage.search.placeholder.fullName") as string}
                                    name="fullName"
                                />
                            </Col>
                        </Row>
                        
                        <Row gutter={30}>
                            <Col span={12}>
                                <CommonFormItem 
                                    label={t("doctorPage.search.label.email")}
                                    placeholder={t("doctorPage.search.placeholder.email") as string}
                                    name="email"
                                />
                            </Col>
                            <Col span={12}>
                                <CommonFormItem 
                                    label={t("doctorPage.search.label.phoneNumber")}
                                    placeholder={t("doctorPage.search.placeholder.phoneNumber") as string}
                                    name="phoneNumber"
                                />
                            </Col>
                        </Row>

                        <Row gutter={30}>
                            <Col span={12}>
                                <CommonFormItem 
                                    label={t("doctorPage.search.label.userName")}
                                    placeholder={t("doctorPage.search.placeholder.userName") as string}
                                    name="username"
                                />
                            </Col>
                            <Col span={12}>
                                <CommonFormItem 
                                    label={t("doctorPage.search.label.specialize")}
                                    type= "select"
                                    name="specializeId"
                                    placeholder={t("doctorPage.search.placeholder.specialize") as string}
                                    options={dataSelectSpecialize}
                                />
                            </Col>
                        </Row>
                        <Row gutter={30}> 
                            <Col span={12}>
                                <CommonFormItem  
                                    name="modifiedDate"
                                    label={t("doctorPage.search.label.modifiedDate")}
                                >
                                    <CommonRangePicker />
                                </CommonFormItem>
                            </Col>
                            <Col span={12}>
                                <CommonFormItem 
                                    label={t("doctorPage.search.label.modifiedBy")}
                                    name="modifiedBy"
                                    placeholder={t("doctorPage.search.placeholder.modifiedBy") as string}
                                />
                            </Col>
                        </Row>
                        <Row gutter={30}>
                            <Col span={12}>
                                <CommonFormItem 
                                    label={t("doctorPage.search.label.status")}
                                    type= "select"
                                    name="status"
                                    placeholder={t("doctorPage.search.placeholder.status") as string}
                                    options={DOCTOR_PAGE_STATUS?.map((item:any)=>({value: item.value, label: t(item.label)}))}
                                />
                            </Col>
                        </Row>

                        <Space className="form-btn-container">
                            <CommonButton btnType="default" size={'small'} onClick={()=> onClear({advance: true})}>
                                {t("common.button.deleteCondition")}
                            </CommonButton>
                            <CommonButton btnType="primary" size={'small'} htmlType="submit">
                                {t("layout.search")}
                            </CommonButton>
                        </Space>
                    </CommonForm>
                </div>
            }
            </div>
}

export default DoctorsSearchBox;

