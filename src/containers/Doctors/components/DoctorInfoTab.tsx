import React, { useEffect, useState } from 'react'
import {Row, Col, Form, Upload, UploadProps, UploadFile, Radio} from 'antd';
import { useTranslation } from 'react-i18next';
import ImgCrop from 'antd-img-crop';
import { RcFile } from 'antd/es/upload';
import { PlusOutlined } from '@ant-design/icons';
import moment from 'moment';

import { ReactComponent as DeleteIcon } from '../../../resources/images/delete.svg';

import CommonForm from '../../../components/Common/Form';
import CommonFormItem from '../../../components/Common/FormItem';
import CommonButton from '../../../components/Common/Button';
import { DATE_FORMAT, DOCTOR_PAGE_STATUS, REGEX_EMAIL, REGEX_PHONE_NUMBER } from '../../../utils/constants';

import specializeServices from "../../../services/specializes.service";

export interface DoctorInfoTabProps{
    dataDetail: any,
    formDoctorInfo: any,
    onCreateDoctor: any
}

function DoctorInfoTab(props: DoctorInfoTabProps) {
    const { t } = useTranslation();
    const { dataDetail, formDoctorInfo, onCreateDoctor } = props;
    const [dataSelectSpecialize, setDataSelectSpecialize] = useState<{value:any, label: string}[]>([]);
    const [fileList, setFileList] = useState<UploadFile[]>(dataDetail?.id ?[
        {
            uid: '-1',
            name: 'image.png',
            status: 'done',
            url: dataDetail?.image,
        },
    ]:[]);

    const getData = async () => {
        const resp = await specializeServices.findAllSpecialize();
        const data = resp?.data;
        if (resp?.status === 200) {
            setDataSelectSpecialize(data?.data?.filter((item:any)=> item?.isDelete === 0)?.map((item:any)=> ({value: item?.id, label: item?.name})))
        } else {
            setDataSelectSpecialize([])
        }
    }

    useEffect(() => {
        getData()
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    
    const onChange: UploadProps['onChange'] = ({ fileList: newFileList }) => {
        if(newFileList?.length===0){
            setFileList([])
        }else{
            formDoctorInfo.setFields([{
                name: "image",
                errors: [""],
            }]);
        }
        console.log("newFileList",newFileList)
    };
    
    const onPreview = async (file: UploadFile) => {
        let src = file.url as string;
        if (!src) {
        src = await new Promise((resolve) => {
            const reader = new FileReader();
            reader.readAsDataURL(file.originFileObj as RcFile);
            reader.onload = () => resolve(reader.result as string);
        });
        }
        const image = new Image();
        image.src = src;
        const imgWindow = window.open(src);
        imgWindow?.document.write(image.outerHTML);
    };

    const onUploadFile = async (options:any) => {
        if (!options?.file) {
            return
        }
        console.log("options?.file",options)
        const reader = new FileReader()
        reader.readAsDataURL(options?.file)
        reader.onload = () => {
            console.log('called: ', reader)
            setFileList([{
                uid: options?.file?.uid,
                name: options?.file?.name,
                status: 'done',
                url: reader.result as string,
            }])
        }
    }

    const onFinishDoctorInfo =  (values:any) => {
        if(fileList.length===0){
            formDoctorInfo.setFields([{
                name: "image",
                errors: [`${t("validate.select")} ${t("doctorPage.form.label.image")}!`],
            }]);
            onCreateDoctor(false);
            return
        }else{
            formDoctorInfo.setFieldValue("image",fileList[0]?.url)
            onCreateDoctor(true);
        }
    }

    const onFinishDoctorInfoFailed = (errorInfo:any) => {
        if(fileList.length===0){
            formDoctorInfo.setFields([{
                name: "image",
                errors: [`${t("validate.select")} ${t("doctorPage.form.label.image")}!`],
            }]);
            return
        }
        onCreateDoctor(false)
        console.log('Failed:', errorInfo, fileList);
    };

    console.log(dataDetail)

    return (
        <CommonForm
            form={formDoctorInfo}
            onFinish={onFinishDoctorInfo}
            onFinishFailed={onFinishDoctorInfoFailed}
            layout="vertical"
            initialValues={{
                doctorCode: dataDetail?.doctorCode ? dataDetail?.doctorCode : `BS_${moment().unix()}`,
                fullName: dataDetail?.fullName,
                dob: dataDetail?.dob ? moment(dataDetail?.dob) : undefined,
                gender: dataDetail ? dataDetail?.gender : 0,
                email: dataDetail?.email,
                phoneNumber: dataDetail?.phoneNumber,
                address: dataDetail?.address,
                status: dataDetail?.status,
                specialize: dataDetail?.admDoctorSpecializes ? dataDetail?.admDoctorSpecializes?.map((item:any)=>({specializeId: item?.admSpecialize?.id, experience: item?.experience})) : [],
                note: dataDetail?.note,
            }}
        >
            <Row gutter={20}>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("doctorPage.form.label.doctorCode") as string} 
                        name="doctorCode"
                        placeholder={t("doctorPage.form.placeholder.doctorCode") as string}
                        disabled
                    />
                </Col>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("doctorPage.form.label.fullName") as string} 
                        name="fullName"
                        placeholder={t("doctorPage.form.placeholder.fullName") as string}
                        showRequiredIcon
                        rules={[
                            { required: true, message: `${t("validate.input")} ${t("doctorPage.form.label.fullName")}!` }
                        ]}
                    />
                </Col>
            </Row>
            <Row gutter={20}>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("doctorPage.form.label.dob") as string} 
                        name="dob"
                        placeholder={t("doctorPage.form.placeholder.dob") as string}
                        type='datePicker'
                        showRequiredIcon
                        disabledDate={(current:any)=> current >= moment().endOf("day").toDate()}
                        rules={[
                            { required: true, message: `${t("validate.select")} ${t("doctorPage.form.label.dob")}!` }
                        ]}
                        format={DATE_FORMAT}
                    />
                </Col>
                <Col span={12}>
                    <CommonFormItem
                        name="gender"
                        label={t('doctorPage.form.label.gender')}
                        showRequiredIcon
                    >
                        <Radio.Group >
                            <Radio value={0}>{t("doctorPage.options.gender.male")}</Radio>
                            <Radio value={1}>{t("doctorPage.options.gender.female")}</Radio>
                        </Radio.Group>
                    </CommonFormItem>
                </Col>
            </Row>
            <Row gutter={20}>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("doctorPage.form.label.email") as string}
                        name="email"
                        placeholder={t("doctorPage.form.placeholder.email") as string}  
                        showRequiredIcon
                        rules={[
                            { required: true, message: `${t("validate.input")} ${t("doctorPage.form.label.email")}!`},
                            { pattern: REGEX_EMAIL, message: `${t("validate.emailRegex")}`},
                        ]}
                    />
                </Col>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("doctorPage.form.label.phoneNumber") as string}
                        name="phoneNumber"
                        placeholder={t("doctorPage.form.placeholder.phoneNumber") as string}  
                        showRequiredIcon
                        rules={[
                            { required: true, message: `${t("validate.input")} ${t("doctorPage.form.label.phoneNumber")}!`},
                            { pattern: new RegExp(REGEX_PHONE_NUMBER), message: t('validate.phoneNumberFormat') as string },
                        ]}
                        maxLength={10}
                    />
                </Col>
            </Row>
            <Row gutter={20}>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("doctorPage.form.label.address") as string}
                        name="address"
                        placeholder={t("doctorPage.form.placeholder.address") as string}  
                        showRequiredIcon
                        rules={[
                            { required: true, message: `${t("validate.input")} ${t("doctorPage.form.label.address")}!`},
                        ]}
                    />
                </Col>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("doctorPage.form.label.status") as string}
                        name="status" 
                        placeholder={t("doctorPage.form.placeholder.status") as string}
                        rules={[
                            { required: true, message: `${t("validate.select")} ${t("doctorPage.form.label.status")}!`},
                        ]}
                        showRequiredIcon
                        type="select"                                    
                        options={DOCTOR_PAGE_STATUS?.map((item:any)=>({value: item.value, label: t(item.label)}))}
                    />
                </Col>
            </Row>
            <div className="title-doctor">{t("doctorPage.form.label.specializeUse")}</div>
            <Row gutter={20}>
                <Col span={24}>
                    <Form.List name="specialize">
                    {(fields, { add, remove }) => {
                        return (
                        <div>
                            {fields.map((field, index) => (
                            <Row gutter={20} key={field.key}>
                                <Col span={12}>
                                    <CommonFormItem
                                        label={`${t("doctorPage.form.label.specialize")} ${index+1}`}
                                        name={[index, "specializeId"]}
                                        rules={[
                                            { required: true, message: `${t("validate.select")} ${t("doctorPage.form.label.specialize")} ${index+1}!`},
                                        ]}
                                        showRequiredIcon
                                        placeholder={`${t("doctorPage.form.placeholder.specialize")} ${index+1}` as string}
                                        type='select'
                                        options={dataSelectSpecialize}
                                    />
                                </Col>
                                <Col span={9}>
                                    <CommonFormItem
                                        name={[index, "experience"]}
                                        label={t("doctorPage.form.label.experience")}
                                        type='inputNumber'
                                        min={1}
                                        showRequiredIcon
                                        rules={[
                                            { required: true, message: `${t("validate.input")} ${t("doctorPage.form.label.experience")}!`},
                                        ]}
                                        placeholder={t("doctorPage.form.placeholder.experience") as string}
                                    />
                                </Col>
                                <Col span={3}>
                                    <CommonFormItem
                                        name="button-remove-specialize"
                                        label=" "
                                    >
                                        <CommonButton
                                            key="remove"
                                            onClick={() => remove(field.name)}
                                            btnType="danger"
                                            size="small"
                                            className="btn-icon-left dynamic-delete-button"
                                        >
                                            <DeleteIcon/>
                                        </CommonButton>
                                    </CommonFormItem>
                                </Col>
                            </Row>
                            ))}
                            <Row style={{justifyContent: 'center'}}>
                                <CommonButton
                                    type="dashed"
                                    onClick={() => add()}
                                    style={{ width: "60%" }}
                                    className="btn-icon-left btn-add-specialize"
                                >
                                    <PlusOutlined /> {t('doctorPage.form.label.specializeAdd')}
                                </CommonButton>
                            </Row>
                        </div>
                        );
                    }}
                    </Form.List>
                </Col>
            </Row>
            <Row gutter={20}>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("doctorPage.form.label.image") as string} 
                        name="image"
                        placeholder={t("doctorPage.form.placeholder.image") as string}
                        showRequiredIcon
                    >
                        <ImgCrop showGrid rotationSlider aspectSlider showReset>
                            <Upload
                                listType= "picture-card"
                                accept="image/png, image/jpeg"
                                fileList={fileList}
                                onChange={onChange}
                                onPreview={onPreview}
                                customRequest={onUploadFile}
                                maxCount={1}
                            >
                                {fileList.length < 1 && '+ Upload'}
                            </Upload>
                        </ImgCrop>
                    </CommonFormItem>
                </Col>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("doctorPage.form.label.note") as string}
                        name="note" 
                        placeholder={t("doctorPage.form.placeholder.note") as string}
                        type='textArea'
                        rows={5}
                    />
                </Col>
            </Row>
        </CommonForm>
    )
}

export default DoctorInfoTab