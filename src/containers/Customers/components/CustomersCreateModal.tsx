import React, { useCallback, useEffect, useState } from 'react'
import { Modal, Form, notification, Tabs, TabsProps} from 'antd';
import { useTranslation } from 'react-i18next';

import { ReactComponent as UpdateIcon } from '../../../resources/images/update_icon.svg';
import { ReactComponent as AddIcon } from '../../../resources/images/plus.svg';
import { ReactComponent as DeleteIcon } from '../../../resources/images/delete.svg';

import CommonButton from '../../../components/Common/Button';
import CustomerInfoTab from './CustomerInfoTab';
import AccountInfoTab from './AccountInfoTab';

import userServices from "../../../services/users.service";

interface CustomersCreateModalProps {
    dataDetail: any,
    modalStatus: boolean,
    handleOk: () => void,
    handleCancel: () => void
}

function CustomersCreateModal(props:CustomersCreateModalProps) {
    const [activeKey, setActiveKey] = useState<string>("1");
    const [items, setItems] = useState<TabsProps["items"]>([]);
    const [formAccountInfo] = Form.useForm();
    const [formCustomerInfo] = Form.useForm();
    const {modalStatus, handleOk, handleCancel, dataDetail} = props;
    const { t } = useTranslation();
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [isSubmitAccountSuccess, setIsSubmitAccountSuccess] = useState<boolean>(false);
    const [isSubmitCustomerSuccess, setIsSubmitCustomerSuccess] = useState<boolean>(false);

    const onSubmit = ()=>{ 
        setIsLoading(true);
        formAccountInfo.submit();
        formCustomerInfo.submit();
        setIsLoading(false)
    }

    const onCreateCustomer = useCallback(async () => {
        setIsLoading(true);
        if(dataDetail?.id){                                                                     //Chỉnh sửa khách hàng
            const resp = await userServices.updateUser({...dataDetail?.admUser, ...formAccountInfo.getFieldsValue()}); // cập nhật tài khoản
            const data = resp?.data; // data là dữ liệu tài khoản
            if (resp?.status === 200) { // nếu cập nhật tài khoản thành công
                const respCustomer = await userServices.updateCustomer({...dataDetail,...formCustomerInfo.getFieldsValue()}); // cập nhật khách hàng
                const dataCustomer = respCustomer?.data;    // dataCustomer là dữ liệu khách hàng
                if (respCustomer?.status === 200) {     // nếu cập nhật khách hàng thành công
                    notification.success({
                        message: t('customerPage.message.editSuccess'),
                    });
                    handleOk();
                } else {                               // nếu cập nhật khách hàng thất bại
                    notification.error({
                        message: dataCustomer?.message || t('commonError.oopsSystem'),
                    });
                    setIsSubmitAccountSuccess(false); // set lại trạng thái tạo tài khoản thành công
                    setIsSubmitCustomerSuccess(false); // set lại trạng thái tạo khách hàng thành công
                    setIsLoading(false);           // set lại trạng thái loading
                }
            } else {
                notification.error({
                    message: data?.message || t('commonError.oopsSystem'),
                });
                setIsSubmitAccountSuccess(false);
                setIsSubmitCustomerSuccess(false);
                setIsLoading(false);
            }
        }else{                                                                          // Thêm mới khách hàng
            const resp = await userServices.createUser({...formAccountInfo.getFieldsValue()});  // tạo tài khoản
            const data = resp?.data;  // data là dữ liệu tài khoản
            if (resp?.status === 200) { // nếu tạo tài khoản thành công
                const respCustomer = await userServices.createCustomer({...formCustomerInfo.getFieldsValue(), admUser: {id: data?.data?.id}}); // tạo khách hàng
                const dataCustomer = respCustomer?.data;
                if (respCustomer?.status === 200) { // nếu tạo khách hàng thành công
                    notification.success({
                        message: t('customerPage.message.createSuccess'),
                    });
                    handleOk(); // đóng modal
                } else {
                    notification.error({
                        message: dataCustomer?.message || t('commonError.oopsSystem'),
                    });
                    setIsSubmitAccountSuccess(false);
                    setIsSubmitCustomerSuccess(false);
                    setIsLoading(false);
                }
            } else {
                notification.error({
                    message: data?.message || t('commonError.oopsSystem'),
                });
                setIsSubmitAccountSuccess(false);
                setIsSubmitCustomerSuccess(false);
                setIsLoading(false);
            }
        }
        setIsSubmitAccountSuccess(false);
        setIsSubmitCustomerSuccess(false);
        setIsLoading(false); 
    },[dataDetail, formAccountInfo, formCustomerInfo, handleOk, t]) // nếu có 1 trong các biến này thay đổi thì sẽ chạy lại hàm

    useEffect(()=>{ // khi tạo tài khoản thành công thì mới tạo khách hàng
        console.log("isSubmitAccountSuccess", isSubmitAccountSuccess); 
        console.log("isSubmitCustomerSuccess", isSubmitCustomerSuccess);
        if(isSubmitAccountSuccess && isSubmitCustomerSuccess){ // nếu tạo tài khoản và khách hàng thành công thì mới tạo khách hàng
            onCreateCustomer();                               // tạo khách hàng
        }
        if(!isSubmitCustomerSuccess){                     // nếu tạo khách hàng thất bại thì chuyển về tab khách hàng
            setActiveKey("1")                                   // chuyển về tab khách hàng
        }else if(!isSubmitAccountSuccess){               // nếu tạo tài khoản thất bại thì chuyển về tab tài khoản
            setActiveKey("2")                                // chuyển về tab tài khoản
        }
    },[isSubmitAccountSuccess, isSubmitCustomerSuccess, onCreateCustomer]) // nếu có 1 trong các biến này thay đổi thì sẽ chạy lại hàm

    // Xóa Khách hàng
    const onDelete = async ()=>{
        setIsLoading(true);
        Modal.confirm({
            title: t('common.confirmAction'),
            content: t('common.editDataAction'),
            centered: true,
            okText: t('common.button.accept'),
            onOk: async () => {                                                         // nếu chọn đồng ý
                const respCustomer = await userServices.deleteCustomers([dataDetail?.id]); 
                const dataCustomer = respCustomer?.data;
                if (respCustomer?.status === 200) { // nếu xóa khách hàng thành công
                    const resp = await userServices.deleteUsers([dataDetail?.admUser?.id]); // xóa tài khoản
                    const data = resp?.data;
                    if (resp?.status === 200) { // nếu xóa tài khoản thành công
                        notification.success({
                            message: t('customerPage.message.createSuccess'),
                        });
                        handleOk(); // đóng modal
                    } else {
                        notification.error({
                            message: data?.message || t('commonError.oopsSystem'),
                        });
                    }
                } else {
                    notification.error({
                        message: dataCustomer?.message || t('commonError.oopsSystem'),
                    });
                }
            },
            okButtonProps:{loading: isLoading}, // nếu loading thì ko cho click
            cancelText: t('common.button.cancel') // nếu chọn hủy
        });
        setIsLoading(false); // set lại trạng thái loading
    }

    console.log("dataDetail",dataDetail)

    const onChangeTab = (key:string) => {
        setActiveKey(key);
    }

    // render tabs item
    useEffect(()=>{                                         // khi dataDetail thay đổi thì mới render lại tabs
        const listItem = [ {                                // render lại tabs
            key: "1",
            label: t("customerPage.form.tabs.customerInfo") as string,
            closable: false,
            children: <CustomerInfoTab                                  // tab thông tin khách hàng
                            dataDetail={dataDetail}                     // dữ liệu khách hàng
                            formCustomerInfo={formCustomerInfo}             // form thông tin khách hàng
                            onCreateCustomer={setIsSubmitCustomerSuccess}           // hàm tạo khách hàng
                        />
        },
        {
            key: "2",
            label: t("customerPage.form.tabs.accountInfo") as string,
            closable: false,
            children: <AccountInfoTab                                   // tab tạo tài khoản
                            dataDetail={dataDetail?.admUser}
                            formAccountInfo={formAccountInfo}
                            onCreateAccount={setIsSubmitAccountSuccess}
                        />
        }];
        setItems(listItem);                            // set lại tabs
    },[dataDetail, formAccountInfo, formCustomerInfo, t]);

    return <Modal
        className="customer-modal-create"
        title={dataDetail?.id ? t("customerPage.form.editTitle") : t("customerPage.form.createTitle")}
        open={modalStatus}
        maskClosable={false}
        onCancel={handleCancel}
        footer={[
            <CommonButton
                key="close"
                onClick={handleCancel}
                size="small"
            >
                {t("common.button.close")}
            </CommonButton>,
            dataDetail?.id ? <CommonButton
                key="remove"
                onClick={onDelete}
                btnType="danger"
                size="small"
                className="btn-icon-left"
            >
                <><DeleteIcon/> {t("common.button.remove")}</>
            </CommonButton> : null,
            <CommonButton
                key="submit"
                htmlType="submit"
                btnType="primary"
                size="small"
                onClick={onSubmit}
                className="btn-icon-left"
                loading={isLoading}
            >
                {dataDetail?.id ? <><UpdateIcon/> {t("common.button.update")}</> : <><AddIcon/> {t("common.button.addNew")}</>}
            </CommonButton>
        ]}
    >
        <Tabs 
            items={items} 
            activeKey={activeKey}
            onChange={onChangeTab}
        />
    </Modal>
}

export default CustomersCreateModal;

