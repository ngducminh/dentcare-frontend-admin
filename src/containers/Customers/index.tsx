import React, { useCallback, useState, useMemo, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { ExportOutlined } from '@ant-design/icons';
import moment from "moment";
import { saveAs } from 'file-saver';
import { Modal, Space, notification } from "antd";

import { ReactComponent as IconAdd } from '../../resources/images/plus.svg';
import { ReactComponent as IconAction } from '../../resources/images/action_icon.svg';

import CustomersSearchBox from "./components/CustomersSearchBox";
import CustomersCreateModal from "./components/CustomersCreateModal";
import { useQuery } from "../../utils/customHooks";
import { DATE_TIME_FORMAT_SECOND, DEFAULT_PAGE_SIZE, EQUIPMENT_PAGE_STATUS } from "../../utils/constants";
import CommonButton from "../../components/Common/Button";
import CommonDropdown from "../../components/Common/Dropdown";
import CommonTag from "../../components/Common/Tag";
import { buildQueryString } from "../../utils/utilFunctions";
import CommonTable from "../../components/Common/Table";

import userServices from "../../services/users.service";

function Customers(){
    const componentPath = '/customers'
    const navigate = useNavigate();
    const { t } = useTranslation();
    const queryObj:any = useQuery();
    const {params = {}, search} = queryObj
    const {
        page: pageQuery,
        pageSize: pageSizeQuery,
        sortBy: sortByQuery,
        sortType: sortTypeQuery,
        search: searchQuery,
    } = params
    const page = pageQuery ? parseFloat(pageQuery) : 1;
    const pageSize = pageSizeQuery ? parseFloat(pageSizeQuery) : DEFAULT_PAGE_SIZE;
    
    const [data, setData] = useState<any>();
    const [visibleModalCreate, setVisibleModalCreate] = useState<boolean>(false);
    const [isLoading, setIsLoading] = useState(false);
    const [dataSelected, setDataSelected] = useState<number[]>([]);
    const [isLoadingExport, setIsLoadingExport] = useState(false);
    const [currentCustomer, setCurrentCustomer] = useState<any>(undefined)
    const [expandedRowKeys, setExpandedRowKeys] = useState<number[]>([]);

    const getData = useCallback(async (reload?: boolean) => {
        setIsLoading(true);
        const newSearch = searchQuery ? JSON.parse(searchQuery) : {};
        const paramsSearch = {
            page: reload? 0 : page-1,
            size: pageSize,
            sortBy: sortByQuery,
            sortType: sortTypeQuery,
            search: JSON.stringify({
                ...newSearch,
                searchType: 2,
            }),
        }
        const resp = await userServices.getListAccounts(paramsSearch);
        const data = resp?.data;
        if (resp?.status === 200) {
            setData(data?.data)
        } else {
            notification.error({
                message: data?.message || t('commonError.oopsSystem'),
            });
        }
        setIsLoading(false);
    },[page, pageSize, searchQuery, sortByQuery, sortTypeQuery, t])

    useEffect(() => {
        getData()
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [search])

    const onPageChange = (pagination:any, filters:any, sorter:any) => {
        let queryString = buildQueryString({
            ...params,
            page: pageSize === pagination?.pageSize ? pagination?.current : 1,
            pageSize: pagination?.pageSize,
            sortBy: sorter?.order ? sorter?.field : '',
            sortType: sorter?.order ? sorter?.order === 'ascend' ? 'asc' : 'desc' : ''
        })
        navigate(`${componentPath}${queryString || ''}`)
    }

    // rowSelection objects indicates the need for row selection
    const rowSelection = {
        onChange: (selectedRowKeys:any, selectedRows:any) => {
            setDataSelected(selectedRowKeys);
            console.log('onChange rowSelection', selectedRowKeys, selectedRows);
        },
        onSelect: (record:any, selected:any, selectedRows:any) => {
            console.log('onChange onSelect', record, selected, selectedRows);
        },
        onSelectAll: (selected:any, selectedRows:any, changeRows:any) => {
            console.log('onChange onSelectAll', selected, selectedRows, changeRows);
        },
    };

    const handleOk = ()=>{
        getData(true);
        setVisibleModalCreate(false);
        setCurrentCustomer(undefined);
    }

    // Thao tác xóa, đổi trạng thái
    const updateCustomers = useCallback((type:any, value?:any) => {
        if (dataSelected?.length === 0) {
            return
        }
        Modal.confirm({
            title: t('common.confirmAction'),
            okText: t('common.button.accept'),
            onOk: async () => {
                let resp;
                if(type === 'delete'){
                    resp = await userServices.deleteCustomers(dataSelected)
                }else{
                    if(value===0){
                        resp = await userServices.unlockCustomers(dataSelected)
                    }else{
                        resp = await userServices.lockCustomers(dataSelected)
                    }
                }
                const data = resp?.data;
                if (resp?.status === 200) {
                    if(type === 'delete'){
                        notification.success({
                            message: t('doctorPage.message.deleteSuccess'),
                        });
                    }else{
                        notification.success({
                            message: t('doctorPage.message.changeStatusSuccess'),
                        });
                    }
                    getData();
                } else {
                    notification.error({
                        message: data?.message || t('commonError.oopsSystem'),
                    });
                }
            },
            onCancel: () => {
                console.log('Cancel');
            },
            cancelText: t('common.button.cancel')
        })
    },[dataSelected, getData, t])

    // xuất excel
    const exportData = async () => {
        setIsLoadingExport(true);
        const newSearch = searchQuery ? JSON.parse(searchQuery) : {};
        const paramsSearch = {
            page: 0,
            size: 10000,
            sortBy: sortByQuery,
            sortType: sortTypeQuery,
            search: JSON.stringify({
                ...newSearch,
                searchType: 2,
            }),
        }
        const resp = await userServices.exportUser(paramsSearch);
        const data = resp?.data;
        if (resp?.status === 200) {
            const fileName = `Customers_Data_Export_${moment().format('YYYYMMDD')}_${moment().unix()}.xlsx`
            saveAs(data, fileName);
        } else {
            notification.error({
                message: data?.message || t('commonError.oopsSystem'),
            });
        }
        
        setIsLoadingExport(false);
    }

    const items = useMemo(() => {
        const children = EQUIPMENT_PAGE_STATUS.map((item:any, index:number) => {
            return {
                key: `1-${index}`,
                label: <div onClick={()=>{updateCustomers("changeStatus",item.value)}}>{`${t(item.label)}`}</div>
            }
        })
        return [
            {
                key: '1',
                label: `${t('customerPage.changeStatus')}`,
                children: children
            },
            {
                key: '2',
                label:  <div onClick={()=>{updateCustomers("delete")}}>{`${t('common.button.remove')}`}</div>,
            },
        ];
    }, [t, updateCustomers])

    const columns = [
        {
            title: t("customerPage.list.columns.STT"),
            key: 'index',
            fixed: "left",
            width: "3%",
            align: "center",
            render: (cell:any, record:any, index:number) => (page - 1) * pageSize + index + 1,
        },
        {
            title: t("customerPage.list.columns.customerCode"),
            dataIndex: 'customerCode',
            key: 'customerCode',
            fixed: "left",
            render: (value:any, row:any) => {
                return <div className="link" onClick={()=>{setCurrentCustomer(row); setVisibleModalCreate(true)}}>{value || '--'}</div>;
            }
        },
        {
            title: t("customerPage.list.columns.fullName"),
            dataIndex: 'fullName',
            key: 'fullName',
        },
        {
            title: t("customerPage.list.columns.userName"),
            dataIndex: 'admUser',
            key: 'admUser',
            render:  (cell: any, record: any, index: number)=>  cell?.username,
        },
        {
            title: t("customerPage.list.columns.email"),
            dataIndex: 'email',
            key: 'email',
        },
        {
            title: t("customerPage.list.columns.phoneNumber"),
            dataIndex: 'phoneNumber',
            key: 'phoneNumber',
        },
        {
            title: t("customerPage.list.columns.note"),
            dataIndex: 'note',
            key: 'note',
        },
        {
            title: t("customerPage.list.columns.modifiedDate"),
            dataIndex: 'modifiedDate',
            key: 'modifiedDate',
            render:  (cell: any, record: any, index: number)=>  cell ? moment(cell).format(DATE_TIME_FORMAT_SECOND) : "",
        },
        {
            title: t("customerPage.list.columns.modifiedBy"),
            dataIndex: 'modifiedBy',
            key: 'modifiedBy',
        },
        {
            title: t("customerPage.list.columns.status"),
            dataIndex: 'status',
            key: 'status',
            render: (value:any, cell:any) => {
                const curStatus:any = EQUIPMENT_PAGE_STATUS.find((x:any) => x.value === value) || {}
                return <CommonTag tagType={curStatus?.type}>{t(curStatus?.label)}</CommonTag>
            },
        },
    ];
    return <>
        <CustomersSearchBox getData={()=>{}} componentPath={componentPath}/>
        
        <div className="avic-table-top">
            <div className="avic-table-top-title">
                {t("customerPage.list.title")}
            </div>
            <Space className="avic-table-top-right">
                <CommonButton icon={<ExportOutlined />} btnType="default" size={'small'} onClick={()=>{exportData()}} loading={isLoadingExport}>
                    {t("common.button.exportExcel")}
                </CommonButton>
                <CommonDropdown menu={{ items }}>
                    <CommonButton btnType="primary" size={'small'} className="btn-icon-right">
                        {t("common.button.action")}<IconAction />
                    </CommonButton>
                </CommonDropdown>
                <CommonButton btnType="primary" size={'small'} className="btn-icon-left" onClick={()=>{setVisibleModalCreate(true)}}>
                    <IconAdd />{t("common.button.addNew")}
                </CommonButton>
            </Space>
        </div>

        <CommonTable
                rowKey={'id'}
                loading={isLoading}
                dataSource={data?.content || []}
                columns={columns}
                data={data}
                onChange={onPageChange}
                rowSelection={{...rowSelection, checkStrictly: false,}}
                defaultSorter={{
                    order: sortTypeQuery,
                    field: sortByQuery,
                }}
                scroll={{x: 1800}}
                expandable={{
                    expandedRowKeys: expandedRowKeys,
                    onExpandedRowsChange: (expandedRows: any) => {
                        setExpandedRowKeys(expandedRows)
                    }
                }}
            /> 

        {visibleModalCreate ?
            <CustomersCreateModal
                dataDetail={currentCustomer}
                modalStatus={visibleModalCreate}
                handleOk={handleOk}
                handleCancel={()=>{setVisibleModalCreate(false); setCurrentCustomer(undefined);}}
            />
            :<></>
        }
    </>
}

export default Customers;