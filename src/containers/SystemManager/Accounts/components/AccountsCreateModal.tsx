import React, { useState } from 'react'
import {Row, Col, Modal, Form, notification, Input, Avatar, Upload} from 'antd';
import { useTranslation } from 'react-i18next';
import ImgCrop from 'antd-img-crop';
import { useSelector } from 'react-redux';

import {ReactComponent as CameraIcon} from "../../../../resources/images/camera-icon.svg";
import { ReactComponent as UpdateIcon } from '../../../../resources/images/update_icon.svg';
import { ReactComponent as AddIcon } from '../../../../resources/images/plus.svg';

import CommonForm from '../../../../components/Common/Form';
import CommonFormItem from '../../../../components/Common/FormItem';
import CommonButton from '../../../../components/Common/Button';
import { ACCOUNT_PAGE_PERMISSION, ACCOUNT_PAGE_STATUS, REGEX_PASSWORD } from '../../../../utils/constants';
import { UserOutlined } from '@ant-design/icons';

import userServices from "../../../../services/users.service";

interface AccountsCreateModalProps {
    dataDetail: any,
    modalStatus: boolean,
    handleOk: () => void,
    handleCancel: () => void
}

function AccountsCreateModal(props:AccountsCreateModalProps) {
    const [form] = Form.useForm();
    const {modalStatus, handleOk, handleCancel, dataDetail} = props
    const { t } = useTranslation();
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [fileAvatar, setFileAvatar] = useState<any>(dataDetail?.avatar ? dataDetail?.avatar : null);
    const { profile } = useSelector((state:any) => state?.profileReducer);

    const onFinish =  (values:any) => {
        onUpdateAccount(values);
    }

    const onFinishFailed = (errorInfo:any) => {
        console.log('Failed:', errorInfo);
    };

    const onUploadFile = async (options: any) => {
        if (!options?.file) {
            setFileAvatar(undefined);
            return
        }
        
        console.log("options?.file", options?.file)
        const reader = new FileReader()
        reader.readAsDataURL(options?.file)
        reader.onload = () => {
            console.log('called: ', reader)
            setFileAvatar(reader.result);
        }
        
    }


    const onUpdateAccount = async (values: any) => {
        setIsLoading(true);
        const resp = await userServices.updateUser({...dataDetail, ...form.getFieldsValue(), avatar: fileAvatar});
        const data = resp?.data;
        if (resp?.status === 200) {
            notification.success({
                message: t('accountPage.message.editSuccess'),
            });
            if(profile?.admUser?.id === dataDetail?.id){
                window.location.reload()
            }else{
                handleOk();
            }
        } else {
            notification.error({
                message: data?.message || t('commonError.oopsSystem'),
            });
        }
        setIsLoading(false);
    }

    console.log(dataDetail)


    return <Modal
        title={dataDetail?.id ? t("accountPage.form.editTitle") : t("accountPage.form.createTitle")}
        open={modalStatus}
        maskClosable={false}
        onCancel={handleCancel}
        footer={[
            <CommonButton
                key="close"
                onClick={handleCancel}
                size="small"
            >
                {t("common.button.close")}
            </CommonButton>,
            <CommonButton
                form="myForm"
                key="submit"
                htmlType="submit"
                btnType="primary"
                size="small"
                className="btn-icon-left"
                loading={isLoading}
            >
                {dataDetail?.id ? <><UpdateIcon/> {t("common.button.update")}</> : <><AddIcon/> {t("common.button.addNew")}</>}
            </CommonButton>
        ]}
    >
        <CommonForm
            form={form}
            id="myForm"
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            layout="vertical"
            initialValues={{
                username: dataDetail?.username,
                password: dataDetail?.password,
                permission: dataDetail?.permission,
                status: dataDetail?.status,
            }}
        >
            <Row gutter={20}>
                <Col span={24}>
                    <div className="avatar-box-container" style={{paddingTop: 2}}>
                        <div className="avatar-box">
                            {fileAvatar
                                ? <Avatar className="avatar" src={fileAvatar} icon={<UserOutlined />} />
                                :
                                <Avatar className="avatar" icon={<UserOutlined />} />
                            }
                            <div className="avatar-change-icon">
                                <ImgCrop showGrid rotationSlider aspectSlider showReset>
                                    <Upload
                                        fileList={[]}
                                        customRequest={onUploadFile}
                                    >
                                        <CameraIcon />
                                    </Upload>
                                </ImgCrop>
                            </div>
                        </div>
                    </div>
                </Col>
            </Row>
            <Row gutter={20}>
                <Col span={24}>
                    <CommonFormItem 
                        label={t("accountPage.form.label.username") as string} 
                        name="username"
                        placeholder={t("accountPage.form.placeholder.username") as string}
                        rules={[
                            { required: true, message: `${t("validate.input")} ${t("accountPage.form.label.username")}!` }
                        ]}
                        showRequiredIcon={!dataDetail?.id}
                        disabled={dataDetail?.id}
                    />
                </Col>
                <Col span={24}>
                    <CommonFormItem
                        validateTrigger={["onChange", "onBlur"]}
                        name="password"
                        label={t("accountPage.form.label.password") as string} 
                        rules={[
                            { required: true, message: `${t("validate.input")} ${t("accountPage.form.label.password")}!` },
                            { pattern: REGEX_PASSWORD, message: `${t("validate.passwordRegex")}` }
                        ]}
                        showRequiredIcon={!dataDetail?.id}
                        disabled={dataDetail?.id}
                    >
                        <Input.Password 
                            disabled={dataDetail?.id} 
                            placeholder={t("accountPage.form.placeholder.password") as string} 
                            allowClear 
                        />
                    </CommonFormItem>
                </Col> 
                <Col span={24}>
                    <CommonFormItem 
                        label={t("accountPage.form.label.permission") as string} 
                        name="permission"
                        type='select'
                        rules={[
                            { required: true, message: `${t("validate.input")} ${t("accountPage.form.label.permission")}!` },
                        ]}
                        showRequiredIcon
                        placeholder={t("accountPage.form.placeholder.permission") as string}
                        options={ACCOUNT_PAGE_PERMISSION?.map((item:any)=>({value: item.value, label: t(item.label)}))}
                    />
                </Col>
                <Col span={24}>
                    <CommonFormItem 
                        label={t("accountPage.form.label.status") as string}
                        name="status" 
                        placeholder={t("accountPage.form.placeholder.status") as string}
                        rules={[
                            { required: true, message: `${t("validate.select")} ${t("accountPage.form.label.status")}!`},
                        ]}
                        showRequiredIcon
                        type="select"                                    
                        options={ACCOUNT_PAGE_STATUS?.map((item:any)=>({value: item.value, label: t(item.label)}))}
                    />
                </Col>
            </Row>
        </CommonForm>
    </Modal>
}

export default AccountsCreateModal;

