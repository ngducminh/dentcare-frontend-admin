import { EditOutlined } from '@ant-design/icons';
import { Form, Space, Upload, UploadFile, UploadProps, notification } from 'antd';
import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next';
import ImgCrop from 'antd-img-crop';
import { RcFile } from 'antd/es/upload';

import CommonForm from '../../../components/Common/Form';
import CommonFormItem from '../../../components/Common/FormItem';
import CommonButton from '../../../components/Common/Button';
import { REGEX_PHONE_NUMBER } from '../../../utils/constants';

import homePageService from "../../../services/homePages.service"

function HomePageInfo() {
    const { t } = useTranslation();
    const [form] = Form.useForm();
    const [isEdit, setIsEdit] = useState(false);
    const [homeData, setHomeData] = useState<any>();
    const [isLoading, setIsLoading] = useState(false);
    const [fileList, setFileList] = useState<UploadFile[]>([]);
    
    useEffect(()=>{
        if(isEdit){
            form.setFieldValue("websiteAddress", homeData?.websiteAddress);
        }else{
            form.setFieldValue("websiteAddress", <a href="http://localhost:3001" target='_blank' rel="noreferrer">{homeData?.websiteAddress}</a>);
        }
    },[form, isEdit, homeData])
    
    useEffect(()=>{
        form.setFieldsValue({
            email: homeData?.email,
            phoneNumber: homeData?.phoneNumber,
            address: homeData?.address,
        })
        setFileList([{
            uid: "id-1",
            name:"banner.jpg",
            status: 'done',
            url: homeData?.banner,
        }]);
    },[form, homeData])

    const getData = async ()=>{
        setIsLoading(true)
        const resp = await homePageService.getHomePage(1);
        const data = resp?.data;
        if(resp?.status === 200){
            setHomeData(data?.data)
        }else{
            notification.error({
                message: data?.message || t('commonError.oopsSystem'),
            });   
        }
        setIsLoading(false)
    }
    useEffect(()=>{
        getData()
    // eslint-disable-next-line react-hooks/exhaustive-deps
    },[])
    
    const onChange: UploadProps['onChange'] = ({ fileList: newFileList }) => {
        if(newFileList?.length===0){
            setFileList([])
        }else{
            form.setFields([{
                name: "banner",
                errors: [""],
            }]);
        }
        console.log("newFileList",newFileList)
    };
    
    const onPreview = async (file: UploadFile) => {
        let src = file.url as string;
        if (!src) {
        src = await new Promise((resolve) => {
            const reader = new FileReader();
            reader.readAsDataURL(file.originFileObj as RcFile);
            reader.onload = () => resolve(reader.result as string);
        });
        }
        const image = new Image();
        image.src = src;
        const imgWindow = window.open(src);
        imgWindow?.document.write(image.outerHTML);
    };

    const onUploadFile = async (options:any) => {
        if (!options?.file) {
            return
        }
        console.log("options?.file",options)
        const reader = new FileReader()
        reader.readAsDataURL(options?.file)
        reader.onload = () => {
            console.log('called: ', reader)
            setFileList([{
                uid: options?.file?.uid,
                name: options?.file?.name,
                status: 'done',
                url: reader.result as string,
            }])
        }
    }

    const onFinish = (values: any) => {
        if(fileList.length === 0){
            form.setFields([{
                name: "banner",
                errors: [`${t("validate.select")} ${t("homePage.label.banner")}!`],
            }]);
            return
        }
        setHomeData({...homeData,...values, banner: fileList[0]?.url})
        onSubmit(values)
    }
    const onFinishFailed = (errorInfo:any) => {
        if(fileList.length === 0){
            form.setFields([{
                name: "banner",
                errors: [`${t("validate.select")} ${t("homePage.label.banner")}!`],
            }]);
            return
        }
        console.log('Failed:', errorInfo);
    };

    const onSubmit = async (values:any) => {
        setIsLoading(true)
        const body = {
            ...values,
            id: 1,
            banner: fileList[0]?.url
        }
        const resp = await homePageService.updateHomePage(body);
        const data = resp?.data;
        if (resp?.status === 200) {
            notification.success({
                message: t('homePage.submitSuccess'),
            });
            setIsEdit(false)
        } else {
            notification.error({
                message: data?.message || t('commonError.oopsSystem'),
            });
        }
        setIsLoading(false)
    }

    const onClear = () => {
        getData();
        setIsEdit(false)
    }

    return (
        <div className="avic-search-box home-page-info">
            <div className="advance-search-box">
                <div className="close-advance-search-btn" >
                    <div>
                        {t('homePage.title')}
                    </div>
                    <Space>
                        {
                            !isEdit &&
                            <EditOutlined className='cursor-pointer' onClick={() => setIsEdit(true)}/>
                        }
                    </Space>
                </div>
    
                <CommonForm
                    form={form}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    layout="horizontal"
                    isLoading={isLoading}
                >
                    <CommonFormItem
                        isView={!isEdit}
                        name="email"
                        label={t('homePage.label.email')}
                        placeholder={t('homePage.placeholder.email') as string}
                        rules={[
                            { required: true, whitespace: true,  message: `${t("validate.input")} ${t("homePage.placeholder.email")}!` }
                        ]}
                        showRequiredIcon
                    />
                    <CommonFormItem
                        isView={!isEdit}
                        name="phoneNumber"
                        label={t('homePage.label.phoneNumber')}
                        placeholder={t('homePage.placeholder.phoneNumber') as string}
                        rules={[
                            { pattern: new RegExp(REGEX_PHONE_NUMBER), message: t('validate.phoneNumberFormat') as string },
                            { required: true, whitespace: true,  message: `${t("validate.input")} ${t("homePage.placeholder.phoneNumber")}!` }
                        ]}
                        showRequiredIcon
                        maxLength={10}
                    />
                    <CommonFormItem
                        isView={!isEdit}
                        name="address"
                        label={t('homePage.label.address')}
                        placeholder={t('homePage.placeholder.address') as string}
                        rules={[
                            { required: true, whitespace: true,  message: `${t("validate.input")} ${t("homePage.placeholder.address")}!` }
                        ]}
                        showRequiredIcon
                    />
                    <CommonFormItem
                        isView={!isEdit}
                        name="websiteAddress"
                        label={t('homePage.label.websiteAddress')}
                        placeholder={t('homePage.placeholder.websiteAddress') as string}
                        rules={[
                            { required: true, whitespace: true,  message: `${t("validate.input")} ${t("homePage.placeholder.websiteAddress")}!` }
                        ]}
                        showRequiredIcon
                    />
                    <CommonFormItem 
                        isView={!isEdit}
                        label={t("homePage.label.banner") as string} 
                        name="banner"
                        placeholder={t("homePage.label.banner") as string}
                        showRequiredIcon
                    >
                        <ImgCrop showGrid rotationSlider aspectSlider showReset>
                            <Upload
                                listType= "picture-card"
                                accept="image/png, image/jpeg"
                                fileList={fileList}
                                onChange={onChange}
                                onPreview={onPreview}
                                customRequest={onUploadFile}
                                maxCount={1}
                                disabled={!isEdit}
                            >
                                {fileList.length < 1 && '+ Upload'}
                            </Upload>
                        </ImgCrop>
                    </CommonFormItem>
                    {
                        isEdit && <Space className="form-btn-container">
                            <CommonButton size={'small'} onClick={onClear}>
                                {t('common.button.cancel')}
                            </CommonButton>
                            <CommonButton btnType="primary" size={'small'} htmlType="submit">
                                {t('common.button.save')}
                            </CommonButton>
                        </Space>
                    }
                </CommonForm>
    
            </div>
        </div>
    )
}

export default HomePageInfo
