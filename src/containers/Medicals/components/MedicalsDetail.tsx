import React, { useCallback, useEffect, useState } from 'react'
import {Row, Col, Table, Form, notification, Space, Modal} from 'antd';
import { useTranslation } from 'react-i18next';
import { useNavigate, useParams } from 'react-router-dom';
import {EditOutlined} from '@ant-design/icons';
import { useSelector } from 'react-redux';

import CommonForm from '../../../components/Common/Form';
import CommonFormItem from '../../../components/Common/FormItem';
import CommonButton from '../../../components/Common/Button';
import CommonTable from '../../../components/Common/Table';
import CommonFormEditor from '../../../components/Common/FormEditor';
import { APPOINTMENT_PAGE_STATUS } from '../../../utils/constants';

import medicalServices from "../../../services/medicals.service";
import appointmentServices from "../../../services/appointments.service";
import { isDoctorMedical } from '../../../utils/utilFunctions';

function MedicalsDetail() {
    const [form] = Form.useForm();
    const { t } = useTranslation();
    const navigate= useNavigate();
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const { medicalId } = useParams();
    const [isEdit, setIsEdit] = useState<boolean>(false);
    const [isSuccess, setIsSuccess] = useState<boolean>(false);
    const [dataSource, setDataSource] = useState<any[]>([]);
    const [currentMedical, setCurrentMedical] = useState<any>();
    const {profile} = useSelector((state:any) => state?.profileReducer);

    const getData = useCallback(async () => {
        setIsLoading(true);
        const resp = await medicalServices.getMedical(parseInt(medicalId as string));
        const data = resp?.data;
        if (resp?.status === 200) {
            form.setFieldsValue({
                fullName: data?.data?.admAppointment?.admCustomer?.fullName,
                dob: data?.data?.admAppointment?.admCustomer?.dobStr,
                address: data?.data?.admAppointment?.admCustomer?.address,
                gender: data?.data?.admAppointment?.admCustomer?.gender === 0 ? t("doctorPage.options.gender.male") : t("doctorPage.options.gender.female"),
                email: data?.data?.admAppointment?.admCustomer?.email,
                phoneNumber: data?.data?.admAppointment?.admCustomer?.phoneNumber,
                appointmentCode: data?.data?.admAppointment?.appointmentCode,
                bookingDate: data?.data?.admAppointment?.bookingDateStr,
                appointmentNote: data?.data?.admAppointment?.note,
                medicalCode: data?.data?.medicalCode,
                doctor: data?.data?.admAppointment?.admDoctor?.fullName,
                medicalDate: data?.data?.medicalDateStr,
                status:  t(APPOINTMENT_PAGE_STATUS[data?.data?.admAppointment?.status]?.label),
                diagnostic: data?.data?.diagnostic,
                prescription: data?.data?.prescription,
                medicalNote: data?.data?.note,
            })
            setDataSource(data?.data?.admAppointment?.services?.map((item:any)=>({
                id: item?.id,
                serviceName: item?.name,
                price: item?.price,
                quantity: 1,
                total: item?.price,
            })))
            if(data?.data?.admAppointment?.status===3) setIsSuccess(true)
            setCurrentMedical(data?.data);
        } else {
            notification.error({
                message: data?.message || t('commonError.oopsSystem'),
            });
        }
        setIsLoading(false);
    },[medicalId, t, form])

    useEffect(()=>{
        getData()
    },[getData])

    const onFinish =  (values:any) => {
        onSubmit(values);
    }

    const onSubmit = async (values:any) => {
        setIsLoading(true)
        const body = {
            ...currentMedical,
            diagnostic: values.diagnostic,
            prescription: values.prescription,
            note: values.medicalNote,
        }
        const resp = await medicalServices.updateMedical(body);
        const data = resp?.data;
        if (resp?.status === 200) {
            notification.success({
                message: t('medicalPage.message.editSuccess'),
            });
            setIsEdit(false)
        } else {
            notification.error({
                message: data?.message || t('commonError.oopsSystem'),
            });
        }
        setIsLoading(false)
    }

    const onSuccess = async ()=>{
        setIsLoading(true);
        Modal.confirm({
            title: t('medicalPage.message.confirmTitle'),
            content: t('medicalPage.message.confirmContent'),
            centered: true,
            okText: t('common.button.accept'),
            onOk: async () => { 
                const resp = await appointmentServices.updateAppointment({...currentMedical?.admAppointment,status: 3});
                const data = resp?.data;
                if (resp?.status === 200) {
                    notification.success({
                        message: t('medicalPage.message.success'),
                    });
                    setIsSuccess(true);
                    form.setFieldValue("status",t(APPOINTMENT_PAGE_STATUS[3]?.label))
                } else {
                    notification.error({
                        message: data?.message || t('commonError.oopsSystem'),
                    });
                }
            },
            okButtonProps:{loading: isLoading},
            cancelText: t('common.button.cancel')
        });
        setIsLoading(false);
    }

    const onFinishFailed = (errorInfo:any) => {
        console.log('Failed:', errorInfo);
    };

    const columns = [
        {
            title: t("medicalPage.form.columns.STT"),
            key: 'index',
            width: '5%',
            render: (cell:any, record:any, index:number) => index + 1,
        },
        {
            title: t("medicalPage.form.columns.serviceName"),
            dataIndex: 'serviceName',
            key: 'serviceName',
        },
        {
            title: t("medicalPage.form.columns.price"),
            dataIndex: 'price',
            key: 'price',
            render: (cell: any, record: any, index: number) => {return cell.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}
        },
        {
            title: t("medicalPage.form.columns.quantity"),
            dataIndex: 'quantity',
            key: 'quantity',
        },
        {
            title: t("medicalPage.form.columns.total"),
            dataIndex: 'total',
            key: 'total',
            render: (cell: any, record: any, index: number) => {return cell.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}
        },
    ];

    return (
        <CommonForm
            form={form}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            layout="horizontal"
            isLoading={isLoading}
        >
            <div className="detail-page-box box-shadow radius-15">
                <div>
                    <div className="box-title">
                        {t('medicalPage.form.customerInfo')}
                    </div>
                    <Row gutter={30}>
                        <Col span={12}>
                            <CommonFormItem
                                isView={true}
                                disabled={true}
                                name="fullName"
                                label={t('medicalPage.form.label.fullName')}
                            />
                        </Col>
                        <Col span={12}>
                            <CommonFormItem
                                isView={true}
                                disabled={true}
                                name="dob"
                                label={t('medicalPage.form.label.dob')}
                            />
                        </Col>
                    </Row>
                    <Row gutter={30}>
                        <Col span={12}>
                            <CommonFormItem
                                isView={true}
                                disabled={true}
                                name="address"
                                label={t('medicalPage.form.label.address')}
                            />
                        </Col>
                        <Col span={12}>
                            <CommonFormItem
                                isView={true}
                                disabled={true}
                                name="gender"
                                label={t('medicalPage.form.label.gender')}
                            />
                        </Col>
                    </Row>
                    <Row gutter={30}>
                        <Col span={12}>
                            <CommonFormItem
                                isView={true}
                                disabled={true}
                                name="email"
                                label={t('medicalPage.form.label.email')}
                            />
                        </Col>
                        <Col span={12}>
                            <CommonFormItem
                                isView={true}
                                disabled={true}
                                name="phoneNumber"
                                label={t('medicalPage.form.label.phoneNumber')}
                            />
                        </Col>
                    </Row>
                </div>

                <div>
                    <div className="box-title">
                        {t('medicalPage.form.appointmentInfo')}
                    </div>
                    <Row gutter={30}>
                        <Col span={12}>
                            <CommonFormItem
                                isView={true}
                                disabled={true}
                                name="appointmentCode"
                                label={t('medicalPage.form.label.appointmentCode')}
                            />
                        </Col>
                        <Col span={12}>
                            <CommonFormItem
                                isView={true}
                                disabled={true}
                                name="bookingDate"
                                label={t('medicalPage.form.label.bookingDate')}
                            />
                        </Col>
                    </Row>
                    <Row gutter={30}>
                        <Col span={12}>
                            <CommonFormItem
                                isView={true}
                                disabled={true}
                                name="appointmentNote"
                                label={t('medicalPage.form.label.note')}
                            />
                        </Col>
                    </Row>
                </div>

                <div>
                    <div className="box-title">
                        {t('medicalPage.form.medicalInfo')}
                    </div>
                    <Row gutter={30}>
                        <Col span={12}>
                            <CommonFormItem
                                isView={true}
                                disabled={true}
                                name="medicalCode"
                                label={t('medicalPage.form.label.medicalCode')}
                            />
                        </Col>
                        <Col span={12}>
                            <CommonFormItem
                                isView={true}
                                disabled={true}
                                name="doctor"
                                label={t('medicalPage.form.label.doctor')}
                            />
                        </Col>
                    </Row>
                    
                    <Row gutter={30}>
                        <Col span={12}>
                            <CommonFormItem
                                isView={true}
                                disabled={true}
                                name="medicalDate"
                                label={t('medicalPage.form.label.medicalDate')}
                            />
                        </Col>
                        <Col span={12}>
                            <CommonFormItem
                                isView={true}
                                disabled={true}
                                name="status"
                                label={t('medicalPage.form.label.status')}
                            />
                        </Col>
                    </Row>

                    <CommonTable
                        style={{marginBottom: 10}}
                        scroll={{ y: 250 }}
                        pagination={false}
                        rowKey={'id'}
                        columns={columns}
                        dataSource={dataSource}
                        summary={() => (
                            <Table.Summary fixed>
                                <Table.Summary.Row>
                                    <Table.Summary.Cell index={0}></Table.Summary.Cell>
                                    <Table.Summary.Cell index={1}></Table.Summary.Cell>
                                    <Table.Summary.Cell index={2}></Table.Summary.Cell>
                                    <Table.Summary.Cell index={3}>{t("medicalPage.form.columns.totalMoney")}</Table.Summary.Cell>
                                    <Table.Summary.Cell index={4}>
                                        {dataSource.reduce((accumulator:number, object:any) => { return accumulator + object.total;  }, 0)
                                            .toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}
                                    </Table.Summary.Cell>
                                </Table.Summary.Row>
                            </Table.Summary>
                        )}
                    />
                </div>

                <div>
                    <div className="box-title" >
                        {t('medicalPage.form.diagnosticPrescription')}
                        {
                            (!isEdit && !isSuccess && (isDoctorMedical(currentMedical?.admAppointment?.admDoctor?.id, profile)||profile?.admUser?.permission ===0)) &&
                            <EditOutlined style={{marginLeft: 24}} className='cursor-pointer' onClick={() => setIsEdit(true)}/>
                        }
                    </div>
                    <Row gutter={30}>
                        <Col span={24}>
                            <CommonFormItem
                                isView={!isEdit}
                                name="diagnostic"
                                label={t('medicalPage.form.label.diagnostic')}
                                placeholder={t('medicalPage.form.placeholder.diagnostic') as string}
                                type="textArea"
                                rows={4}
                                maxLength={5000}
                            />
                        </Col>
                    </Row>
                    <Row gutter={30}>
                        <Col span={24}>
                            <CommonFormItem
                                isView={!isEdit}
                                name="prescription"
                                label={t('medicalPage.form.label.prescription')}
                            >
                                <CommonFormEditor disabled={!isEdit} placeholder={t('Đơn thuốc') as string}/>
                            </CommonFormItem>
                        </Col>
                    </Row>
                    <Row gutter={30}>
                        <Col span={24}>
                            <CommonFormItem
                                isView={!isEdit}
                                name="medicalNote"
                                label={t('medicalPage.form.label.note')}
                                placeholder={t('medicalPage.form.placeholder.note') as string}
                                type="textArea"
                                rows={4}
                                maxLength={5000}
                            />
                        </Col>
                    </Row>
                </div>

                <Space style={{width: "100%", justifyContent:"flex-end"}} className="form-btn-container">
                    { isEdit ? <>
                            <CommonButton size={'small'} onClick={()=>{form.resetFields(); setIsEdit(false);}}>
                                {t('common.button.cancel')}
                            </CommonButton>
                            <CommonButton btnType="primary" size={'small'} htmlType="submit">
                                {t('common.button.save')}
                            </CommonButton>
                        </> : <>
                            <CommonButton size={'small'} onClick={()=>{navigate(-1)}}>
                                {t('common.button.close')}
                            </CommonButton>
                            { !isSuccess ? (isDoctorMedical(currentMedical?.admAppointment?.admDoctor?.id, profile)||profile?.admUser?.permission ===0) ? <CommonButton btnType='success' size={'small'} onClick={onSuccess}>
                                        {t('common.button.success')}
                                    </CommonButton> : <></>
                                    : 
                                    <CommonButton btnType='info' size={'small'} onClick={()=>{navigate(-1)}}>
                                        {t('common.button.exportMedical')}
                                    </CommonButton>
                            }
                        </>
                    }
                </Space>
            </div>
        </CommonForm>
    )
}

export default MedicalsDetail;

