import React, { useCallback, useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { ExportOutlined } from '@ant-design/icons';
import moment from "moment";
import { Space, notification } from "antd";
import { saveAs } from 'file-saver';

import MedicalsSearchBox from "./components/MedicalsSearchBox";
import { useQuery } from "../../utils/customHooks";
import { DATE_TIME_FORMAT_SECOND, DEFAULT_PAGE_SIZE, APPOINTMENT_PAGE_STATUS, DATE_TIME_FORMAT } from "../../utils/constants";
import CommonButton from "../../components/Common/Button";
import CommonTag from "../../components/Common/Tag";
import { buildQueryString } from "../../utils/utilFunctions";
import CommonTable from "../../components/Common/Table";

import medicalServices from "../../services/medicals.service";

function Medicals(){
    const componentPath = '/medicals'
    const navigate = useNavigate();
    const { t } = useTranslation();
    const queryObj:any = useQuery();
    const {params = {}, search} = queryObj;
    const {
        page: pageQuery,
        pageSize: pageSizeQuery,
        sortBy: sortByQuery,
        sortType: sortTypeQuery,
        search: searchQuery,
    } = params
    const page = pageQuery ? parseFloat(pageQuery) : 1;
    const pageSize = pageSizeQuery ? parseFloat(pageSizeQuery) : DEFAULT_PAGE_SIZE;

    
    const [data, setData] = useState<any>();
    const [isLoading, setIsLoading] = useState(false);
    const [isLoadingExport, setIsLoadingExport] = useState(false);

    const getData = useCallback(async (reload?: boolean) => {
        setIsLoading(true);
        const newSearch = searchQuery ? JSON.parse(searchQuery) : {};
        const paramsSearch = {
            page: reload? 0 : page-1,
            size: pageSize,
            sortBy: sortByQuery,
            sortType: sortTypeQuery,
            search: JSON.stringify({
                ...newSearch,
            }),
        }
        const resp = await medicalServices.getListMedicals(paramsSearch);
        const data = resp?.data;
        if (resp?.status === 200) {
            setData(data?.data)
        } else {
            notification.error({
                message: data?.message || t('commonError.oopsSystem'),
            });
        }
        setIsLoading(false);
    },[page, pageSize, searchQuery, sortByQuery, sortTypeQuery, t])

    useEffect(() => {
        getData()
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [search])

    const onPageChange = (pagination:any, filters:any, sorter:any) => {
        let queryString = buildQueryString({
            ...params,
            page: pageSize === pagination?.pageSize ? pagination?.current : 1,
            pageSize: pagination?.pageSize,
            sortBy: sorter?.order ? sorter?.field : '',
            sortType: sorter?.order ? sorter?.order === 'ascend' ? 'asc' : 'desc' : ''
        })
        navigate(`${componentPath}${queryString || ''}`)
    }

    // xuất excel
    const exportData = async () => {
        setIsLoadingExport(true);
        const newSearch = searchQuery ? JSON.parse(searchQuery) : {};
        const paramsSearch = {
            page: 0,
            size: 10000,
            sortBy: sortByQuery,
            sortType: sortTypeQuery,
            search: JSON.stringify({
                ...newSearch,
            }),
        }
        const resp = await medicalServices.exportMedical(paramsSearch);
        const data = resp?.data;
        if (resp?.status === 200) {
            const fileName = `Medicals_Data_Export_${moment().format('YYYYMMDD')}_${moment().unix()}.xlsx`
            saveAs(data, fileName);
        } else {
            notification.error({
                message: data?.message || t('commonError.oopsSystem'),
            });
        }
        setIsLoadingExport(false);
    }

    const columns = [
        {
            title: t("medicalPage.list.columns.STT"),
            key: 'index',
            fixed: "left",
            width: "4%",
            align: "center",
            render: (cell:any, record:any, index:number) => (page - 1) * pageSize + index + 1,
        },
        {
            title: t("medicalPage.list.columns.medicalCode"),
            dataIndex: 'medicalCode',
            key: 'medicalCode',
            fixed: "left",
            render: (value:any, row:any) => {
                return <Link className="link" to={`${componentPath}/detail/${row.id}`}>{value || '--'}</Link>;
            }
        },
        {
            title: t("medicalPage.list.columns.appointmentCode"),
            dataIndex: 'admAppointment',
            key: 'admAppointment',
            render: (value:any, row:any, index:number) =>  value?.appointmentCode || '--'
        },
        {
            title: t("medicalPage.list.columns.customer"),
            dataIndex: 'admAppointment',
            key: 'customer',
            render: (value:any, row:any, index:number) =>  value?.admCustomer?.fullName || '--'
        },
        {
            title: t("medicalPage.list.columns.doctor"),
            dataIndex: 'admAppointment',
            key: 'doctor',
            render: (value:any, row:any, index:number) =>  value?.admDoctor?.fullName || '--'
        },
        {
            title: t("medicalPage.list.columns.service"),
            dataIndex: 'admAppointment',
            key: 'service',
            render: (value:any, row:any, index:number) =>  value?.services?.map((item:any)=>item?.name).join(" ,") || '--'
        },
        {
            title: t("medicalPage.list.columns.medicalDate"),
            dataIndex: 'medicalDate',
            key: 'medicalDate',
            render:  (cell: any, record: any, index: number)=>  cell ? moment(cell).format(DATE_TIME_FORMAT) : "",
        },
        {
            title: t("medicalPage.list.columns.diagnostic"),
            dataIndex: 'diagnostic',
            key: 'diagnostic',
        },
        {
            title: t("medicalPage.list.columns.note"),
            dataIndex: 'note',
            key: 'note',
        },
        {
            title: t("medicalPage.list.columns.modifiedDate"),
            dataIndex: 'modifiedDate',
            key: 'modifiedDate',
            render:  (cell: any, record: any, index: number)=>  cell ? moment(cell).format(DATE_TIME_FORMAT_SECOND) : "",
        },
        {
            title: t("medicalPage.list.columns.modifiedBy"),
            dataIndex: 'modifiedBy',
            key: 'modifiedBy',
        },
        {
            title: t("medicalPage.list.columns.status"),
            dataIndex: 'admAppointment',
            key: 'status',
            render: (value:any, cell:any) => {
                const curStatus:any = APPOINTMENT_PAGE_STATUS.find((x:any) => x.value === value?.status) || {}
                return <CommonTag tagType={curStatus?.type}>{t(curStatus?.label)}</CommonTag>
            },
        },
    ];
    return <>
        <MedicalsSearchBox getData={()=>{}} componentPath={componentPath}/>
        
        <div className="avic-table-top">
            <div className="avic-table-top-title">
                {t("medicalPage.list.title")}
            </div>
            <Space className="avic-table-top-right">
                <CommonButton icon={<ExportOutlined />} btnType="default" size={'small'} onClick={()=>{exportData()}} loading={isLoadingExport}>
                    {t("common.button.exportExcel")}
                </CommonButton>
            </Space>
        </div>

        <CommonTable
            rowKey={'id'}
            loading={isLoading}
            dataSource={data?.content || []}
            columns={columns}
            data={data}
            onChange={onPageChange}
            defaultSorter={{
                order: sortTypeQuery,
                field: sortByQuery,
            }}
            scroll={{x: 2000}}
        />
    </>
}

export default Medicals;