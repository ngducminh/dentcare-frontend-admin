import React, { useCallback, useEffect, useMemo, useState } from 'react'
import moment from "moment"
import { Col, Form, Row, Space, Table } from 'antd'
import { CloudDownloadOutlined } from '@ant-design/icons'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import _ from 'lodash'

import CommonButton from '../../components/Common/Button'
import CommonTable from '../../components/Common/Table'
import CommonForm from '../../components/Common/Form'
import CommonFormItem from '../../components/Common/FormItem'
import { useQuery } from '../../utils/customHooks'
import { buildQueryString } from '../../utils/utilFunctions'

import userServices from "../../services/users.service";
import homeServices from "../../services/homePages.service";

const convertData = (data: any, currentYear:any) => {
    let newData:any = [];
    Object.keys(data).map((item:any)=>{
        let newItemData:any = {};
        newItemData["id"] = data[item][0]?.loai_dich_vu;
        newItemData["service"] = item;
        Array.from({length: 12}, (_, i) => {
            const a = data[item]?.find((itemFind:any)=> itemFind.thoi_gian === (moment(currentYear).format("YYYY")+"-"+(i+1)))
            newItemData["thang_"+(i+1)] = a?.doanh_thu || 0;
            return i+1;
        })
        newData.push(newItemData)
        return item;
    })
    return newData;
}

const calculateTotal = (data:any[])=>{
    let newData:number[] = [];
    Array.from({length: 12}, (_, i) => {
        const newTotalItem = data.reduce((accumulator:number, object:any) => { return accumulator + (object[`thang_${i+1}`]);  }, 0);
        newData.push(newTotalItem)
        return i+1;
    })
    return newData;
}

function RevenueReport() {
    const {t}=useTranslation();
    const [form] = Form.useForm();
    const [dataCustomer, setDataCustomer] = useState<any[]>([]);
    const [dataSource, setDataSource] = useState<any[]>([]);
    const [total, setTotal] = useState<any[]>([]);
    const navigate = useNavigate();
    const queryObj:any = useQuery();
    const {params = {}, search} = queryObj;
    const {
        search: searchQuery,
    } = params;
    const searchQueryData = useMemo(()=>{
        return searchQuery ? JSON.parse(searchQuery) : {};
    },[searchQuery])
    const getData = useCallback(async ()=>{
        const resp = await homeServices.tinhDoanhThuTheoDichVu({
            from: moment(form.getFieldValue("date")).startOf("year").valueOf(),
            to: moment(form.getFieldValue("date")).endOf("year").valueOf(),
            ...searchQueryData
        });
        const data = resp?.data;
        const dataTable = convertData(_.groupBy(data?.data, function (item:any){
            return item.ten_dich_vu
        }), searchQueryData?.from)
        setDataSource(dataTable);
        setTotal(calculateTotal(dataTable));
    },[searchQueryData, form])

    useEffect(()=>{
        getData();
    },[getData])

    const getDataCustomer = useCallback(async (reload?: boolean) => {
        const resp = await userServices.findAllCustomer();
        const data = resp?.data;
        if (resp?.status === 200) {
            setDataCustomer(data?.data?.map((item:any)=>({value: item.id, label: item.fullName})))
        } else {
            setDataCustomer([])
        }   
    },[])

    useEffect(() => {
        getDataCustomer()
    }, [getDataCustomer])

    const columns = [
        {
            title: t("STT"),
            dataIndex: 'index',
            key: 'index',
            width: '4%',
            align: "center",
            fixed: "left",
            render: (cell: any, record: any, index: number) => index + 1,
        },
        {
            title: t("revenueReportPage.columns.service"),
            dataIndex: 'service',
            key: 'service',
            fixed: "left",
            align: "center",
            width: '10%',
        },
        {
            title: t("revenueReportPage.columns.january"),
            dataIndex: 'thang_1',
            align: "center",
            key: 'thang_1',
            render: (cell: any, record: any, index: number) => cell.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.'),
        },
        {
            title: t("revenueReportPage.columns.february"),
            dataIndex: 'thang_2',
            align: "center",
            key: 'thang_2',
            render: (cell: any, record: any, index: number) => cell.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.'),
        },
        {
            title: t("revenueReportPage.columns.march"),
            dataIndex: 'thang_3',
            
            align: "center",
            key: 'thang_3',
            render: (cell: any, record: any, index: number) => cell.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.'),
        },
        {
            title: t("revenueReportPage.columns.april"),
            dataIndex: 'thang_4',
            align: "center",
            key: 'thang_4',
            render: (cell: any, record: any, index: number) => cell.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.'),
        },
        {
            title: t("revenueReportPage.columns.may"),
            dataIndex: 'thang_5',
            align: "center",
            key: 'thang_5',
            render: (cell: any, record: any, index: number) => cell.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.'),
        },
        {
            title: t("revenueReportPage.columns.june"),
            dataIndex: 'thang_6',
            align: "center",
            key: 'thang_6',
            render: (cell: any, record: any, index: number) => cell.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.'),
        },
        {
            title: t("revenueReportPage.columns.july"),
            dataIndex: 'thang_7',
            align: "center",
            key: 'thang_7',
            render: (cell: any, record: any, index: number) => cell.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.'),
        },
        {
            title: t("revenueReportPage.columns.august"),
            dataIndex: 'thang_8',
            align: "center",
            key: 'thang_8',
            render: (cell: any, record: any, index: number) => cell.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.'),
        },
        {
            title: t("revenueReportPage.columns.september"),
            dataIndex: 'thang_9',
            align: "center",
            key: 'thang_9',
            render: (cell: any, record: any, index: number) => cell.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.'),
        },
        {
            title: t("revenueReportPage.columns.october"),
            dataIndex: 'thang_10',
            align: "center",
            key: 'thang_10',
            render: (cell: any, record: any, index: number) => cell.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.'),
        },
        {
            title: t("revenueReportPage.columns.november"),
            dataIndex: 'thang_11',
            align: "center",
            key: 'thang_11',
            render: (cell: any, record: any, index: number) => cell.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.'),
        },
        {
            title: t("revenueReportPage.columns.december"),
            dataIndex: 'thang_12',
            align: "center",
            key: 'thang_12',
            render: (cell: any, record: any, index: number) => cell.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.'),
        },
    ];

    const onFinish = (values: any) => {
        const dataSearch = {...values};
        if(values?.date){
            dataSearch["from"] = moment(values?.date).startOf("year").valueOf();
            dataSearch["to"] = moment(values?.date).endOf("year").valueOf();
        }
        delete dataSearch.date;
        let queryString ={ 
            search: JSON.stringify(dataSearch||{}),
        }
        if (queryString !== search) {
            navigate(`${buildQueryString(queryString) || ''}`)
        } else {
            getData();
        }
    }

    const onClear = () => {
        const dataSearch = {
            from: moment().startOf("year").valueOf(),
            to: moment().endOf("year").valueOf(),
        };
        let queryString ={ 
            search: JSON.stringify(dataSearch||{}),
        }
        setTimeout(() => {
            form.resetFields();
            form.setFieldValue("userId", undefined)
            navigate(`${buildQueryString(queryString) || ''}`)
        }, 100)
    }

    return (
        <div>
            <div className="avic-search-box">
                <div className="advance-search-box">
                    <CommonForm
                        form={form}
                        layout="vertical"
                        onFinish={onFinish}
                        initialValues={{
                            date: moment(searchQueryData?.from),
                            userId: searchQueryData?.userId
                        }}
                    >
                        <Row gutter={30}>
                        <Col span={12}>
                                <CommonFormItem
                                    name="userId"
                                    label={t('revenueReportPage.label.customer')}
                                    placeholder={t('revenueReportPage.placeholder.customer') as string}
                                    options={dataCustomer}
                                    type='select'
                                />
                            </Col>
                            <Col span={12}>
                                <CommonFormItem
                                    name="date"
                                    type="datePicker"
                                    label={t('revenueReportPage.label.time')}
                                    placeholder={t('revenueReportPage.placeholder.time') as string}
                                    picker="year"
                                />
                            </Col>
                        </Row>
                        <Space className="form-btn-container">
                            <CommonButton btnType="default" size={'small'} onClick={onClear}>
                                {t('common.button.deleteCondition')}
                            </CommonButton>
                            <CommonButton btnType="primary" size={'small'} htmlType="submit">
                                {t('common.button.search')}
                            </CommonButton>
                        </Space>

                    </CommonForm>
                </div>
            </div>

            <div className="avic-table-top">
                <div className="avic-table-top-title">
                    {t("revenueReportPage.title")}
                </div>
                <Space className="avic-table-top-right">
                    <CommonButton type="default" size={'small'}>
                        <CloudDownloadOutlined /> {t("common.button.exportExcel")}
                    </CommonButton>
                </Space>
            </div>
    
            <CommonTable
                rowKey={"id"}
                dataSource={dataSource}
                columns={columns}
                pagination={false}
                scroll={{x: 1500}}
                summary={() => (
                    total?.find((item:any) => item>0) ? <Table.Summary fixed>
                        <Table.Summary.Row>
                            <Table.Summary.Cell index={0}></Table.Summary.Cell>
                            <Table.Summary.Cell index={1} align="center">{t("revenueReportPage.total")}</Table.Summary.Cell>
                            {
                                total?.map((item:any, index: number)=>(
                                    <Table.Summary.Cell index={index+2} align="center">{item.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}</Table.Summary.Cell>
                                ))
                            }
                        </Table.Summary.Row>
                    </Table.Summary> : <></>
                )}
            />
        </div>
    )
}
export default RevenueReport