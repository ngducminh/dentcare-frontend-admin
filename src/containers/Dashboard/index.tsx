import { useEffect, useState } from "react";
import { Col, Row } from "antd";
import { useTranslation } from "react-i18next";
import _ from "lodash";
import { useNavigate } from "react-router-dom";

import { ReactComponent as MoneyIcon } from "../../resources/images/money.svg";
import { ReactComponent as ArriveIcon } from "../../resources/images/arrive.svg";

import BarChartDashboard from "./components/BarChartDashboard";
import PieChartDashboard from "./components/PieChartDashboard";
import CommonSpin from "../../components/Common/Spin";

import homePageService from "../../services/homePages.service"
import appointmentServices from "../../services/appointments.service";

function Dashboard(){
    const { t } = useTranslation();
    const navigate = useNavigate();
    const [data, setData] = useState<any[]>([]);
    const [dataAppointment, setDataAppointment] = useState<any[]>([]);
    const [isLoading, setIsLoading] = useState<boolean>(false)

    const getData = async ()=>{
        const resp = await homePageService.tinhDoanhThu({
            from: 946659600, 
            to: 4133980799
        });
        const data = resp?.data;
        if(resp?.status===200){
            setData(data?.data)
        }else{
            setData([])
        }
    }

    const getDataAppointment = async (reload?: boolean) => {
        setIsLoading(true);
        const resp = await appointmentServices.findAllAppointment();
        const data = resp?.data;
        if (resp?.status === 200) {
            setDataAppointment(data?.data)
        } else {
            setDataAppointment([])
        }
        setIsLoading(false);
    }

    useEffect(()=>{
        getData();
        getDataAppointment();
    },[])

    return (
        <CommonSpin isLoading={isLoading}>
            <Row gutter={12}>
                <Col span={14} xs={24} sm={24} lg={24} xl={14} xxl={14} style={{marginBottom: 12}}>
                    <Row gutter={12} style={{marginBottom: 12}}>
                        <Col span={12} onClick={()=>{navigate("/revenue-report", { replace: true })}} style={{cursor:"pointer"}}>
                            <Row className='dashboard-container' style={{minHeight:0}}>
                                <div className="dashboard-tag-wrapper">
                                    <MoneyIcon/>
                                    <div className="title">{t("dashboardPage.totalMoney")} <br /> {`(${t("dashboardPage.VNĐ")})`}</div>
                                    <div className="total">
                                        {data.reduce((accumulator:number, object:any) => { return accumulator + object.doanh_thu;  }, 0)
                                                .toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}
                                    </div>
                                </div>
                            </Row>
                        </Col>
                        <Col span={12} onClick={()=>{navigate("/appointments", { replace: true })}} style={{cursor:"pointer"}}>
                            <Row className='dashboard-container' style={{minHeight:0}}> <div className="dashboard-tag-wrapper">
                                    <ArriveIcon/>
                                    <div className="title">{t("dashboardPage.totalMedicalCustomer")} <br /> {`(${t("dashboardPage.countMedical")})`}</div>
                                    <div className="total">{dataAppointment?.filter((item:any)=>item.status === 2 || item.status === 3)?.length}</div>
                                </div>
                            </Row>
                        </Col>
                    </Row>
                    <BarChartDashboard/>
                </Col>
                <Col span={10} xs={24} sm={24} lg={24} xl={10} xxl={10}>
                    <PieChartDashboard dataAppointment={_.groupBy(dataAppointment, function(item:any){return item.status})}/>
                </Col>
            </Row>
        </CommonSpin>
    )
}

export default Dashboard;
