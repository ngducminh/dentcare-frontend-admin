import { SetStateAction, useCallback, useEffect, useState } from "react";
import { Payload } from "recharts/types/component/DefaultLegendContent";
import { Cell, Legend, Pie, PieChart, ResponsiveContainer, Sector } from "recharts";
import { Col, Row } from "antd";
import { useTranslation } from "react-i18next";

import CommonEmpty from "../../../components/Common/Empty";

export interface DataChartTypeProps {
    name?: string;
    value?: number;
}

function PieChartDashboard({dataAppointment}:{dataAppointment:any}) {
    const { t } = useTranslation();
    const [isTotalPieChart, setIsTotalPieChart] = useState(10);
    const [activeIndex, setActiveIndex] = useState<number | undefined>(0);
    const [dataChartPie, setDataChartPie] = useState<DataChartTypeProps[]>([]);

    const COLOR_CHART: Payload[] = [
        {
            value: t("dashboardPage.pieChart.options.status.pending") as string,
            type: "square",
            color: "#818181",
        },
        {
            value: t("dashboardPage.pieChart.options.status.approved") as string,
            type: "square",
            color: "#F3A638",
        },
        {
            value:  t("dashboardPage.pieChart.options.status.examining") as string,
            type: "square",
            color: "#54B7D3",
        },
        {
            value:  t("dashboardPage.pieChart.options.status.examined") as string,
            type: "square",
            color: "#4CB64C",
        },
        {
            value:  t("dashboardPage.pieChart.options.status.cancelAppointment") as string,
            type: "square",
            color: "#DC3537",
        },
    ];

    const formatDataNotification = useCallback((obj: any, key: string) => {
        const dataFormated = [];
        for (const property in obj) {
            let a = {
                [key]: `${property}`,
                "value": parseInt(`${obj[property]}`)
            }
            if (a[key] === 'total') {
                a[key] = t("dashboardPage.pieChart.options.status.total") as string
            }
            if (a[key] === 'pending') {
                a[key] = t("dashboardPage.pieChart.options.status.pending") as string
            }
            if (a[key] === 'approved') {
                a[key] = t("dashboardPage.pieChart.options.status.approved") as string
            }
            if (a[key] === 'examining') {
                a[key] = t("dashboardPage.pieChart.options.status.examining") as string
            }
            if (a[key] === 'examined') {
                a[key] = t("dashboardPage.pieChart.options.status.examined") as string
            }
            if (a[key] === 'cancelAppointment') {
                a[key] = t("dashboardPage.pieChart.options.status.cancelAppointment") as string
            }
            
            dataFormated.push(a);
        }
        return dataFormated;
    },[t])

    useEffect(() => {
        const newData = {
            pending: dataAppointment?.["0"]?.length || 0,
            approved: dataAppointment?.["1"]?.length || 0,
            examining: dataAppointment?.["2"]?.length || 0,
            examined: dataAppointment?.["3"]?.length || 0,
            cancelAppointment: dataAppointment?.["4"]?.length || 0,
            total: (dataAppointment?.["0"]?.length || 0) + (dataAppointment?.["1"]?.length || 0) + (dataAppointment?.["2"]?.length || 0) + (dataAppointment?.["3"]?.length || 0) + (dataAppointment?.["4"]?.length || 0),
        }
        const dataFormat = formatDataNotification(newData, 'name');
        const new_arr = dataFormat.filter((item:any) => item.name !== t("dashboardPage.pieChart.options.status.total") as string);
        let findTotal = dataFormat.filter((item:any) => item.name === t("dashboardPage.pieChart.options.status.total") as string);
        setIsTotalPieChart(findTotal[0].value)
        setDataChartPie(new_arr)
    }, [dataAppointment, formatDataNotification, t]);
    
    
    const renderActiveShape = (props: any) => {
        const RADIAN = Math.PI / 180;
        const {
            cx,
            cy,
            innerRadius,
            outerRadius,
            startAngle,
            endAngle,
            midAngle,
            name,
            fill,
            payload,
            percent,
            value
        } = props;

        const sin = Math.sin(-RADIAN * midAngle);
        const cos = Math.cos(-RADIAN * midAngle);
        const sx = cx + (outerRadius + 10) * cos;
        const sy = cy + (outerRadius + 10) * sin;
        const mx = cx + (outerRadius + 28) * cos;
        const my = cy + (outerRadius + 28) * sin;
        const ex = mx + (cos >= 0 ? 1 : -1) * 22;
        const ey = my;
        const textAnchor = "middle";

        return (
            <g>
                <text x={cx} y={cy - 15} fontSize={17} textAnchor="middle" fill={fill}>
                    {`${payload.name}: ${value}`}
                </text>
                <text
                    x={cx}
                    fontSize={15}
                    y={cy + 20}
                    textAnchor="middle"
                    fill="#999"
                >
                    {`${(percent * 100).toFixed(2)}%`}
                </text>
                <Sector
                    cx={cx}
                    cy={cy}
                    innerRadius={innerRadius}
                    outerRadius={outerRadius}
                    startAngle={startAngle}
                    endAngle={endAngle}
                    fill={fill}
                />
                <Sector
                    cx={cx}
                    cy={cy}
                    startAngle={startAngle}
                    endAngle={endAngle}
                    innerRadius={outerRadius + 6}
                    outerRadius={outerRadius + 10}
                    fill={fill}
                />
                <path
                    d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`}
                    stroke={fill}
                    fill="none"
                />

                <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none" className="text" />
                <text
                    x={ex + (cos >= 0 ? 1 : -1) * 50}
                    y={ey + (sin >= 0 ? 1 : -1) * 10}
                    fontSize={10}
                    textAnchor={textAnchor}
                    fill={fill}
                >{`${name} : ${value}`}</text>
                <text
                    x={ex + (cos >= 0 ? 1 : -1) * 50}
                    y={ey + (sin >= 0 ? 1 : -1) * 10}
                    dy={18}
                    textAnchor={textAnchor}
                    fill="#999"
                >
                    {`${(percent * 100).toFixed(2)}%`}
                </text>
            </g>
        );
    };

    const onPieEnter = useCallback(
        (_: any, index: SetStateAction<number | undefined>) => {
            setActiveIndex(index);
        },
        [setActiveIndex]
    );

    const renderLegend = (e: any[]) => {
        let chartHaveData = e.filter(item => item.value > 0)
        let COLOR_CHART_FORMAT = COLOR_CHART.filter(item => (
            chartHaveData.find(item2 => (item.value === item2.name))
        )
        )
        return (
            <div className="bodyLengndChart" style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'normal' }}>
                {
                    chartHaveData.map((entry, index: number) => (
                        <div key={`item-${index}`} style={{ display: 'flex', width: '33%', marginBottom: 10 }}>
                            <span style={{ width: '20px', display: 'inline-flex', height: '20px', backgroundColor: COLOR_CHART_FORMAT[index % COLOR_CHART_FORMAT.length].color }}></span>
                            <div style={{ width: 180, marginLeft: 10, textAlign: 'left', color: COLOR_CHART_FORMAT[index % COLOR_CHART_FORMAT.length].color }}>
                                <span style={{ float: "left" }}>{entry.name}</span>
                            </div>
                        </div>
                    ))
                }
            </div>
        );
    }

    return (
        <Row style={{ paddingBottom: 8}} className='dashboard-container'>
            <Row style={{ width: '100%' }} className='content-title'>
                {t("dashboardPage.pieChart.title")}
            </Row>
            <div style={{ width: '100%', backgroundColor: '#F8FAFC' }}>
                {(isTotalPieChart === 0)
                    ? <Col className="customEmpty">
                        <CommonEmpty style={{ height: '50vh', flexDirection: 'column', display: 'flex', justifyContent: 'center', fontSize: '15px' }} />
                    </Col>
                    :
                    <Col>
                        <h2 style={{ textAlign: 'center', margin: 0 }}>{t("dashboardPage.pieChart.total")}: {isTotalPieChart}</h2>
                        <ResponsiveContainer width="100%" height={475} className='pie-chart'>
                            <PieChart>
                                <Pie
                                    activeIndex={activeIndex}
                                    activeShape={renderActiveShape}
                                    data={dataChartPie}
                                    cx="50%"
                                    cy="50%"
                                    innerRadius={110}
                                    outerRadius={180}
                                    fill="#8884d8"
                                    dataKey="value"
                                    onMouseEnter={onPieEnter}
                                    paddingAngle={0.7}
                                >
                                    {COLOR_CHART.map((entry, index) => {
                                        return entry.value !== 0 ? (
                                            <Cell key={`cell-${index}`} fill={COLOR_CHART[index].color} />
                                        ) : (
                                            ""
                                        );
                                    })}
                                </Pie>
                            </PieChart>
                        </ResponsiveContainer>
                    </Col>
                }
            </div>
            {(isTotalPieChart === 0)
                ? <></>
                : <Legend content={renderLegend(dataChartPie)} />
            }
        </Row>
    )
}

export default PieChartDashboard