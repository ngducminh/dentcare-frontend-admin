import { useCallback, useEffect, useState } from "react";
import moment from 'moment';
import { Bar, BarChart, CartesianGrid, Label, Legend, ResponsiveContainer, Tooltip as TooltipReCharts, XAxis, YAxis } from "recharts";
import { Col, DatePickerProps, Row } from "antd";
import { useTranslation } from "react-i18next";

import CommonDatePicker from "../../../components/Common/DatePicker";
import { DASHBOARD_PAGE_LIST_MONTHS } from "../../../utils/constants";

import homePageService from "../../../services/homePages.service"

const CustomizedAxisTick = (props: any) => {
    const { x, y, payload } = props;
    return (
        <g transform={`translate(${x},${y})`}>
            <text
                x={60}
                y={0}
                dy={16}
                textAnchor="end"
                fill="#666"
                transform="rotate(35)"
            >
                {payload.value}
            </text>
        </g>
    );
}

const CustomTooltip = ({ active, payload, label }: any) => {
    if (active && payload && payload.length) {
        return (
            <div className="custom-tooltip">
                <p className="label">{`${label}`}</p>
                <p className="desc" style={{ color: `${payload[0].color}` }}>{`${payload[0].name}`}: {formatterFloatNumber(payload[0].value.toString())}</p>
            </div>
        );
    }
    return null;
};


const formatterFloatNumber = function (cellValue: string | number) {
    if (cellValue) {
        // @ts-ignore
        return cellValue.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
    }
    if (cellValue === 0) {
        return "0";
    }
    return "";
};

function BarChartDashboard() {
    const { t } = useTranslation();
    const max_val: any = [];
    const [lableYaz, setLableYaz] = useState('');
    const [dataBarChart, setDataBarChart] = useState<any[]>([]);
    const [fromDateLong, setFromDateLong]= useState<number>(Math.floor((new Date(moment().startOf("year").format("YYYY-MM-DD 00:00:00"))).getTime() / 1000));
    const [toDateLong, setToDateLong]= useState<number>(Math.floor((new Date(moment().endOf("year").format("YYYY-MM-DD 00:00:00"))).getTime() / 1000));
    
    const tickFormatter = (value: string, index: number) => {
        max_val.push(value)
        let c = Array.from(new Set(max_val))
    
        // let max_val = e.reduce(function (accumulator, element) {
        //     return (accumulator > element) ? accumulator : element
        // });
        if (Number(c[0]) >= 1000000 && Number(c[0]) < 1000000000) {
            let moneyNumber = Number(value) / 1000000
            // @ts-ignore
            setLableYaz(t("dashboardPage.barChart.millions"))
    
            return (formatterFloatNumber(moneyNumber.toString()))
        } else if (Number(c[0]) >= 1000000000) {
            let moneyNumber = Number(value) / 1000000000
            // @ts-ignore
            setLableYaz(t("dashboardPage.barChart.billions"))
            return (formatterFloatNumber(moneyNumber.toString()))
        }
    
        // 
    
        else if (Number(c[0]) < 1000) {
            let moneyNumber = Number(value) / 1
            // @ts-ignore
            setLableYaz(t("dashboardPage.barChart.money"))
            return (formatterFloatNumber(moneyNumber.toString()))
        } else if (Number(c[0]) > 1000 && Number(c[0]) < 100000) {
            let moneyNumber = Number(value) / 1000
            // @ts-ignore
            setLableYaz(t("dashboardPage.barChart.thousands"))
            return (formatterFloatNumber(moneyNumber.toString()))
        }
        //
        // else if (Number(c[0]) < 100000) {
        //     let moneyNumber = Number(value) / 1000
        //     // @ts-ignore
        //     setLableYaz('Nghìn đồng')
        //     return (formatterFloatNumber(moneyNumber.toString()))
        // }
    
        else {
            return (formatterFloatNumber(value.toString()))
        }
    }

    const getData = useCallback(async ()=>{
        const resp = await homePageService.tinhDoanhThu({
            from: fromDateLong, 
            to: toDateLong
        });
        const data = resp?.data;
        if(resp?.status===200){
            const newData = Array.from({length: 12}, (_, i) => i + 1).map((item:any)=>{
                const checkMoney = data?.data?.find((itemFind:any)=>itemFind?.thoi_gian?.split("-")?.[1] === item.toString());
                return {
                    [t("dashboardPage.barChart.totalMoney")]: checkMoney?.doanh_thu || 0,
                    [t("dashboardPage.barChart.month")]: t(DASHBOARD_PAGE_LIST_MONTHS[item-1]?.label),
                }
            })
            setDataBarChart(newData)
        }else{
            setDataBarChart([])
        }
    },[fromDateLong, t, toDateLong])

    useEffect(()=>{
        getData();
    },[getData])
    
    const onChangeFormatYearMoney: DatePickerProps["onChange"] = (date:any) => {
        const startOfYear = moment(date).startOf("year").format("YYYY-MM-DD 00:00:00");
        const endOfYear = moment(date).endOf("year").format("YYYY-MM-DD 00:00:00");
        const dateStart = new Date(startOfYear);
        const dateEnd = new Date(endOfYear);
        setFromDateLong(Math.floor(dateStart.getTime() / 1000));
        setToDateLong(Math.floor(dateEnd.getTime() / 1000));
    }

    return (
        <Row className='dashboard-container'>
            <Row style={{ width: '100%' }} className='content-title' justify="space-between">
                {t("dashboardPage.barChart.title")}
                <Col>
                    <CommonDatePicker
                        picker="year"
                        format='yyyy'
                        defaultValue={moment()}
                        onChange={onChangeFormatYearMoney}
                        allowClear={true}
                        placeholder={t("dashboardPage.barChart.placeholder")}
    
                    />
                </Col>
            </Row>
            <Row style={{ width: '100%',}}>
                <ResponsiveContainer width="100%" height={425} >
                    <BarChart width={730} height={300} data={dataBarChart} margin={{ top: 60, right: 20, left: 60, bottom: 20 }}>
                        <CartesianGrid vertical={false} strokeDasharray="4" />
                        <XAxis dataKey={t("dashboardPage.barChart.month") as string} height={70} tick={<CustomizedAxisTick />} interval={0} />
                        <YAxis tickFormatter={tickFormatter} >
                            <Label position='top' offset={20}>
                                {lableYaz}
                            </Label>
                        </YAxis>
                        <TooltipReCharts content={<CustomTooltip />} />
                        {true ? '' : <text x="45%" y="30" dominantBaseline="hanging" fontSize="26" color="#14147e">Không có dữ liệu</text>}
                        <Legend align="right"  iconType='square' />
                        <Bar dataKey={t("dashboardPage.barChart.totalMoney") as string} fill="#3B82F6" />
                    </BarChart>
                </ResponsiveContainer>
            </Row>
        </Row>
    )
}

export default BarChartDashboard