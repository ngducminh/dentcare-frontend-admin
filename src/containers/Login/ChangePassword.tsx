import React, {useState, useEffect} from 'react'
import { Col, Form, Input, List, Modal, Row, notification } from 'antd';
import { useTranslation } from 'react-i18next';

import { ReactComponent as Checkbox } from "../../resources/images/checkbox-circle.svg";
import { ReactComponent as Error } from "../../resources/images/error.svg";

import CommonButton from '../../components/Common/Button';
import CommonForm from '../../components/Common/Form';
import LocalStorage from "../../utils/localStorage";
import { AT_LEAST_1_NUMBER, AT_LEAST_1_UPPERCASE, AT_LEAST_8_CHARS, REGEX_PASSWORD } from '../../utils/constants';

import userServices from "../../services/users.service";

export interface ChangePasswordProps{
    openModal: boolean;
    handleOk: () => void;
}

function ChangePassword(props: ChangePasswordProps) {
    const { openModal, handleOk } = props;
    const { t } = useTranslation();
    const [activeIndexes, setActiveIndexes] = useState<number[]>([]);
    const [repeatError, setRepeatError] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [formChanged, setFormChanged] = useState(false);

    const [form] = Form.useForm();
    const passwordNew = Form.useWatch("newPass", form);

    const onFinish = async (values:any) => {
        setIsLoading(true);
        const resp = await userServices.changePassword(values);
        if (resp?.status === 200) {
            notification.success({
                message: t("changePassword.message.changePasswordSuccess"),
            });
            LocalStorage.getInstance().save('accessToken', null);
            window.location.href = '/'
            handleOk();
        } 
        else {
            notification.error({
                message: resp?.data?.message || t('commonError.oopsSystem'),
            });
        }
        setIsLoading(false);
    };

    const listCondition = [
        t("changePassword.validate.atLeast8Chars"),
        t("changePassword.validate.atLeast1Uppercase"),
        t("changePassword.validate.atLeast1Number"),
    ];

    useEffect(() => {
        if (passwordNew) {
            let newActiveIndexes: number[] = [];
            if (passwordNew.match(AT_LEAST_8_CHARS)) {
                newActiveIndexes.push(0);
            }

            if (passwordNew.match(AT_LEAST_1_UPPERCASE)) {
                newActiveIndexes.push(1);
            }

            if (passwordNew.match(AT_LEAST_1_NUMBER)) {
                newActiveIndexes.push(2);
            }
            setActiveIndexes(newActiveIndexes);
        } else {
            setActiveIndexes([]);
        }
    }, [passwordNew]);

    const onValuesChange = (changedValues: any) => {
        setFormChanged(true);
    }

    const onCancel = () => {
        if (formChanged) {
            Modal.confirm({
                title: t('common.confirmAction'),
                okText: t('common.button.accept'),
                onOk: async () => {
                    form.resetFields();
                    setFormChanged(false);
                    handleOk();
                },
                onCancel: () => {
                    console.log("Cancel");
                },
                cancelText: t('common.button.cancel')
            })
        }else{
            handleOk();
        }
    }

    return (
        <Modal
            className="modal-change-password"
            title={t("changePassword.title")}
            open={openModal}
            maskClosable={false}
            onCancel={onCancel}
            footer={[
                <CommonButton
                    key="cancel"
                    onClick={onCancel}
                    size="small"
                >
                    {t("common.button.cancel")}
                </CommonButton>,
                <CommonButton
                    key="submit"
                    htmlType="submit"
                    btnType="primary"
                    size="small"
                    className="btn-icon-left"
                    loading={isLoading}
                    onClick={()=>form.submit()}
                >
                    {t("common.button.save")}
                </CommonButton>
            ]}
        >
            <CommonForm
                    form={form}
                    layout="vertical"
                    name="change-password"
                    requiredMark={false}
                    onFinish={onFinish}
                    onValuesChange={onValuesChange}
                    scrollToFirstError={{ behavior: 'smooth'}}
                >
                    <Row gutter={[16, 24]}>
                        <Col span={24}>
                            <Form.Item
                                validateTrigger={["onChange", "onBlur"]}
                                name="oldPass"
                                label={t("changePassword.label.passwordCurrent")}
                                rules={[
                                    { required: true, message: `${t("validate.input")} ${t("changePassword.label.passwordCurrent")}`}
                                ]}
                            >
                                <Input.Password placeholder={t("changePassword.placeholder.passwordCurrent") as string} allowClear />
                            </Form.Item>

                            <Form.Item
                                validateTrigger={["onChange", "onBlur"]}
                                name="newPass"
                                label={t("changePassword.label.passwordNew")}
                                rules={[
                                    { required: true, message: `${t("validate.input")} ${t("changePassword.label.passwordNew")}`},
                                    { pattern: REGEX_PASSWORD, message: "" }
                                ]}
                            >
                                <Input.Password placeholder={t("changePassword.placeholder.passwordNew") as string} allowClear />
                            </Form.Item>
                            <Col span={24} style={{ display: 'flex', alignItems: 'flex-end' }}>
                                <div className="change-password-condition">
                                    <List
                                        split={false}
                                        dataSource={listCondition}
                                        renderItem={(item, index) => (
                                            <List.Item
                                                className={activeIndexes.includes(index) ? "password-condition-active" : passwordNew ? "password-condition-error" : ""}
                                                style={{ display: 'flex', alignItems: 'center', justifyContent: 'flex-start' }}>
                                                {passwordNew && !activeIndexes.includes(index) ?
                                                    <><Error />{item}</> :
                                                    <><Checkbox />{item}</>}
                                            </List.Item>
                                        )} 
                                    />
                                </div>
                            </Col>
                            
                            <Form.Item
                                style={{ marginBottom: 0 }}
                                validateTrigger={["onChange", "onBlur"]}
                                name="confirmPass"
                                label={t("changePassword.label.passwordAccuracy")}
                                rules={[
                                    { required: true, message: `${t("validate.input")} ${t("changePassword.label.passwordAccuracy")}`},

                                    ({ getFieldValue }) => ({
                                        validator(_, value) {
                                            if (!value) {
                                                setRepeatError(false);
                                                return Promise.reject();
                                            }

                                            if (getFieldValue('newPass') !== value) {
                                                setRepeatError(true)
                                                return Promise.reject();
                                            }
                                            setRepeatError(false);
                                            return Promise.resolve();
                                        },
                                    }),
                                ]}
                            >
                                <Input.Password placeholder={t("changePassword.placeholder.passwordAccuracy") as string} allowClear />
                            </Form.Item>
                            {repeatError ? <p className="password-error">{t('changePassword.validate.passwordMismatched')}</p> : <></>}
                        </Col>
                    </Row>
            </CommonForm>
        </Modal>
    );
}

export default ChangePassword;

