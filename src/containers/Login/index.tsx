import React, {useEffect, useRef, useState} from 'react'
import { Button, Form, Checkbox, Input, Select, notification, Space } from 'antd';
import LocalStorage from "../../utils/localStorage";
import { useTranslation } from "react-i18next";
import {useDispatch} from "react-redux";

import background from "../../resources/images/login/background-login.jpg";
import { ReactComponent as Logo } from "../../resources/images/logo_small.svg";

import {LANGUAGE_LIST} from "../../utils/constants";
import {saveProfile} from "../../redux/actions/profile.actions";    // redux action

import userServices from "../../services/users.service";             // call api            
import ForgetPassword from './ForgetPassword';

const localLanguage = LocalStorage.getInstance().read('language');
function Login() {
    const dispatch = useDispatch();                                     // redux hook dispatch action để lưu profile
    const { t } = useTranslation();
    const formRef = useRef<any>()                                   // form ref để reset form sau khi login               
    const [currentLanguage, setCurrentLanguage] = useState(localLanguage || LANGUAGE_LIST[0]?.value)    // ngôn ngữ hiện tại
    const [isLoading, setIsLoading] = useState<boolean>(false)      // loading khi login
    const [showModal, setShowModal] = useState<boolean>(false)

    useEffect(() => {                           // reset form khi vào trang login
        dispatch(saveProfile(null))                        // reset profile khi vào trang login (để đảm bảo khi login lại thì sẽ lấy lại profile mới)
    }, [dispatch])          // eslint-disable-line react-hooks/exhaustive-deps

    // call api log in and save token
    const onFinish = async (values:any) => {
        setIsLoading(true);
        const resp = await userServices.login(values);
        const data = resp?.data;
        if (resp?.status === 200) {                         // nếu login thành công thì lưu token và lấy profile
            LocalStorage.getInstance().save('accessToken', data?.data?.accessTokenInfo?.accessToken);   // lưu token
            getProfile()                            // lấy profile                          
        } else {
            if (formRef?.current) {
                formRef?.current?.setFields([         // set lại error cho form và đăng nhập lại           
                    {
                        name: 'username',
                        errors: [''],
                    },
                    {
                        name: 'password',
                        errors: [t("login.wrongPassword")],
                    },
                ]);
            }
        }
        setIsLoading(false);
    };

    const getProfile = async () => {
        const resp = await userServices.getProfile();
        const data = resp?.data;
        if (resp?.status === 200) {
            let respInfo;
            if(data?.data?.permission===3){                             // nếu là khách hàng thì ko cho login
                LocalStorage.getInstance().save('accessToken', null);
                notification.error({
                    message: t("login.customerPermission")
                })
                return;
            }else if(data?.data?.permission===1){
                respInfo = await userServices.getListAccounts({
                    page: 0,
                    size: 1000,
                    search: JSON.stringify({
                        searchType: 0,                                    // ds bác sĩ
                    }),
                })
            }else{
                respInfo = await userServices.getListAccounts({
                    page: 0,
                    size: 1000,
                    search: JSON.stringify({
                        searchType: 1,                                  // ds nhân viên
                    }),
                })
            }
            const dataInfo = respInfo?.data;
            if(respInfo?.status === 200){
                const userInfo = dataInfo?.data?.content?.find((item:any)=>item?.admUser?.id === data?.data?.id)
                dispatch(saveProfile(userInfo))
                const redirectUrl = LocalStorage.getInstance().read('redirectUrl');
                if (redirectUrl) {
                    LocalStorage.getInstance().save('redirectUrl', null); //lưu lại url để redirect sau khi login
                    window.location.href = redirectUrl
                } else {
                    window.location.reload()
                }
            }
        }
    }

    const onFinishFailed = (errorInfo:any) => {
        console.log('Failed:', errorInfo);
    };

    const handleChangeLanguage = (value:string) => {
        setCurrentLanguage(value)
        LocalStorage.getInstance().save('language', value);
        window.location.reload()
    };

    return (
        <>
            <div className="login-container" style={{backgroundImage: `url(${background})` }}>
                <div className="login-box">
                    <div className="text-center">
                        <Logo className="style-size-logo"/>
                    </div>
    
                    <h3 className="txt-welcome">{t("login.welcome")}</h3>
                    <h2 className="txt-title">{t("login.title")}</h2>
                    <Form
                        ref={formRef}
                        name="basic"
                        onFinish={onFinish}
                        onFinishFailed={onFinishFailed}
                        autoComplete="off"
                        size={'large'}
                    >
                        <Form.Item
                            name="username"
                            // name="uname"
                            validateTrigger={"onBlur"}
                            rules={[{ required: true, message: t("validate.usernameRequired") as string }]}
                        >
                            <Input
                                placeholder={t("login.usernamePlaceholder") as string}
                                allowClear
                                maxLength={50}
                            />
                        </Form.Item>
                        <Form.Item
                            name="password"
                            // name="pwd"
                            validateTrigger={"onBlur"}
                            rules={[{ required: true, message: t("validate.passwordRequired") as string }]}
                        >
                            <Input.Password
                                placeholder={t("login.passwordPlaceholder") as string}
                                maxLength={20}
                            />
                        </Form.Item>
    
                        <Space style={{justifyContent:"space-between", display: "flex"}}>
                            <Form.Item name="remember" valuePropName="checked">
                                <Checkbox>{t("login.rememberLabel")}</Checkbox>
                            </Form.Item>
                            <Form.Item name="forgetPassword">
                                <div className="forget-password" onClick={()=>setShowModal(true)}>{t("login.forgetPassword")}</div>
                            </Form.Item>
                        </Space>
    
                        <div className="pdbt-20">
                            <Button type="primary" className="btn-login" htmlType="submit" loading={isLoading}>
                                {t("login.submit")}
                            </Button>
                        </div>
    
                        <div className="text-center">
                            <Select
                                value={currentLanguage}
                                style={{ width: 230 }}
                                onChange={handleChangeLanguage}
                                options={LANGUAGE_LIST}
                            />
                        </div>
                    </Form>
                </div>
            </div>
            { showModal ? 
                <ForgetPassword
                    openModal={showModal}
                    handleOk={()=>setShowModal(false)}
                />
                :<></>
            }
        </>
    );
}

export default Login;

