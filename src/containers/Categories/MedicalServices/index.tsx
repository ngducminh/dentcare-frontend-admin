import React, { useCallback, useState, useMemo, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { ExportOutlined } from '@ant-design/icons';
import moment from "moment";
import { saveAs } from 'file-saver';
import { Modal, Space, notification } from "antd";

import { ReactComponent as IconAdd } from '../../../resources/images/plus.svg';
import { ReactComponent as IconAction } from '../../../resources/images/action_icon.svg';

import MedicalServicesSearchBox from "./components/MedicalServicesSearchBox";
import MedicalServicesCreateModal from "./components/MedicalServicesCreateModal";
import { useQuery } from "../../../utils/customHooks";
import { DATE_TIME_FORMAT_SECOND, DEFAULT_PAGE_SIZE, SERVICE_PAGE_STATUS, SERVICE_PAGE_TYPE } from "../../../utils/constants";
import CommonButton from "../../../components/Common/Button";
import CommonDropdown from "../../../components/Common/Dropdown";
import CommonTag from "../../../components/Common/Tag";
import { buildQueryString } from "../../../utils/utilFunctions";
import CommonTable from "../../../components/Common/Table";
import CommonImage from "../../../components/Common/Image";

import medicalServices from "../../../services/medicalServices.service";

function MedicalServices(){
    const componentPath = '/services'
    const navigate = useNavigate();
    const { t } = useTranslation();
    const queryObj:any = useQuery();
    const {params = {}, search} = queryObj
    const {
        page: pageQuery,
        pageSize: pageSizeQuery,
        sortBy: sortByQuery,
        sortType: sortTypeQuery,
        search: searchQuery,
    } = params
    const page = pageQuery ? parseFloat(pageQuery) : 1;
    const pageSize = pageSizeQuery ? parseFloat(pageSizeQuery) : DEFAULT_PAGE_SIZE;
    const [data, setData] = useState<any>();
    const [visibleModalCreate, setVisibleModalCreate] = useState<boolean>(false);
    const [isLoading, setIsLoading] = useState(false);
    const [dataSelected, setDataSelected] = useState<number[]>([]);
    const [isLoadingExport, setIsLoadingExport] = useState(false);
    const [currentService, setCurrentService] = useState<any>(undefined)
    const [expandedRowKeys, setExpandedRowKeys] = useState<number[]>([]);

    const getData = useCallback(async (reload?: boolean) => {
        setIsLoading(true);
        const newSearch = searchQuery ? JSON.parse(searchQuery) : {};
        console.log("newSearch",newSearch, (JSON.stringify(newSearch)))
        
        const paramsSearch = {
            page: reload? 0 : page-1,
            size: pageSize,
            sortBy: sortByQuery,
            sortType: sortTypeQuery,
            search: JSON.stringify({...newSearch})
        }
        const resp = await medicalServices.getListServices(paramsSearch);
        const data = resp?.data;
        if (resp?.status === 200) {
            setData(data?.data)
        } else {
            notification.error({
                message: data?.message || t('commonError.oopsSystem'),
            });
        }
        setIsLoading(false);
    },[page, pageSize, searchQuery, sortByQuery, sortTypeQuery, t])

    useEffect(() => {
        getData()
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [search])

    const onPageChange = (pagination:any, filters:any, sorter:any) => {
        let queryString = buildQueryString({
            ...params,
            page: pageSize === pagination?.pageSize ? pagination?.current : 1,
            pageSize: pagination?.pageSize,
            sortBy: sorter?.order ? sorter?.field : '',
            sortType: sorter?.order ? sorter?.order === 'ascend' ? 'asc' : 'desc' : ''
        })
        navigate(`${componentPath}${queryString || ''}`)
    }

    // rowSelection objects indicates the need for row selection
    const rowSelection = {
        onChange: (selectedRowKeys:any, selectedRows:any) => {
            setDataSelected(selectedRowKeys);
            console.log('onChange rowSelection', selectedRowKeys, selectedRows);
        },
        onSelect: (record:any, selected:any, selectedRows:any) => {
            console.log('onChange onSelect', record, selected, selectedRows);
        },
        onSelectAll: (selected:any, selectedRows:any, changeRows:any) => {
            console.log('onChange onSelectAll', selected, selectedRows, changeRows);
        },
    };

    const handleOk = ()=>{
        getData(true);
        setVisibleModalCreate(false);
        setCurrentService(undefined)
    }

    // Thao tác xóa, đổi trạng thái
    const updateDepartments = useCallback((type:any, value?:any) => {
        if (dataSelected?.length === 0) {
            return
        }
        Modal.confirm({
            title: t('common.confirmAction'),
            okText: t('common.button.accept'),
            onOk: async () => {
                let resp;
                if(type === 'delete'){
                    resp = await medicalServices.deleteServices(dataSelected)
                }else{
                    if(value===0){
                        resp = await medicalServices.unlockServices(dataSelected)
                    }else{
                        resp = await medicalServices.lockServices(dataSelected)
                    }
                }
                const data = resp?.data;
                if (resp?.status === 200) {
                    if(type === 'delete'){
                        notification.success({
                            message: t('servicePage.message.deleteSuccess'),
                        });
                    }else{
                        notification.success({
                            message: t('servicePage.message.changeStatusSuccess'),
                        });
                    }
                    getData();
                } else {
                    notification.error({
                        message: data?.message || t('commonError.oopsSystem'),
                    });
                }
            },
            onCancel: () => {
                console.log('Cancel');
            },
            cancelText: t('common.button.cancel')
        })
    },[dataSelected, getData, t])

    // xuất excel
    const exportData = async () => {
        setIsLoadingExport(true);
        const newSearch = searchQuery ? JSON.parse(searchQuery) : {};
        const paramsSearch = {
            page: 0,
            size: 10000,
            sortBy: sortByQuery,
            sortType: sortTypeQuery,
            search: JSON.stringify({
                ...newSearch,
            }),
        }
        const resp = await medicalServices.exportService(paramsSearch);
        const data = resp?.data;
        if (resp?.status === 200) {
            const fileName = `Services_Data_Export_${moment().format('YYYYMMDD')}_${moment().unix()}.xlsx`
            saveAs(data, fileName);
        } else {
            notification.error({
                message: data?.message || t('commonError.oopsSystem'),
            });
        }
        
        setIsLoadingExport(false);
    }

    const items = useMemo(() => {
        const children = SERVICE_PAGE_STATUS.map((item:any, index:number) => {
            return {
                key: `1-${index}`,
                label: <div onClick={()=>{updateDepartments("changeStatus",item.value)}}>{`${t(item.label)}`}</div>
            }
        })
        return [
            {
                key: '1',
                label: `${t('servicePage.changeStatus')}`,
                children: children
            },
            {
                key: '2',
                label:  <div onClick={()=>{updateDepartments("delete")}}>{`${t('common.button.remove')}`}</div>,
            },
        ];
    }, [t, updateDepartments])

    const columns = [
        {
            title: t("servicePage.list.columns.STT"),
            key: 'index',
            width: '3%',
            align: 'center',
            fixed: "left",
            render: (cell:any, record:any, index:number) => (page - 1) * pageSize + index + 1,
        },
        {
            title: t("servicePage.list.columns.image"),
            dataIndex: 'image',
            key: 'image',
            align: 'center',
            fixed: "left",
            render: (value:any, row:any) => {
                return <CommonImage 
                        src={value}
                    />
                }
        },
        {
            title: t("servicePage.list.columns.name"),
            dataIndex: 'name',
            key: 'name',
            render: (value:any, row:any) => {
                return <div className="link" onClick={()=>{setCurrentService(row); setVisibleModalCreate(true)}}>{value || '--'}</div>;
            }
        },
        {
            title: t("servicePage.list.columns.price"),
            dataIndex: 'price',
            key: 'price',
            render: (cell: any, record: any, index: number) => {return cell.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}
        },
        {
            title: t("servicePage.list.columns.type"),
            dataIndex: 'type',
            key: 'type',
            render: (value: any, record:any, index:number) => t(SERVICE_PAGE_TYPE[value]?.label)
        },
        {
            title: t("servicePage.list.columns.doctor"),
            dataIndex: 'doctors',
            key: 'doctors',
            render:  (cell: any, record: any, index: number)=>  {
                return cell?.map((item:any) => `${item?.fullName}`)?.join(", ")
            },
        },
        {
            title: t("servicePage.list.columns.equipment"),
            dataIndex: 'admServiceEquipments',
            key: 'admServiceEquipments',
            render:  (cell: any, record: any, index: number)=>  {
                return cell?.map((item:any) => `${item?.admEquipment?.name} (${item?.quantity})`)?.join(", ")
            },
        },
        {
            title: t("servicePage.list.columns.description"),
            dataIndex: 'description',
            key: 'description',
        },
        {
            title: t("servicePage.list.columns.modifiedDate"),
            dataIndex: 'modifiedDate',
            key: 'modifiedDate',
            render:  (cell: any, record: any, index: number)=>  cell ? moment(cell).format(DATE_TIME_FORMAT_SECOND) : "",
        },
        {
            title: t("servicePage.list.columns.modifiedBy"),
            dataIndex: 'modifiedBy',
            key: 'modifiedBy',
        },
        {
            title: t("servicePage.list.columns.status"),
            dataIndex: 'status',
            key: 'status',
            render: (value:any, cell:any) => {
                const curStatus:any = SERVICE_PAGE_STATUS.find((x:any) => x.value === value) || {}
                return <CommonTag tagType={curStatus?.type}>{t(curStatus?.label)}</CommonTag>
            },
        },
    ];
    return <>
        <MedicalServicesSearchBox getData={()=>{}} componentPath={componentPath}/>
        
        <div className="avic-table-top">
            <div className="avic-table-top-title">
                {t("servicePage.list.title")}
            </div>
            <Space className="avic-table-top-right">
                <CommonButton icon={<ExportOutlined />} btnType="default" size={'small'} onClick={()=>{exportData()}} loading={isLoadingExport}>
                    {t("common.button.exportExcel")}
                </CommonButton>
                <CommonDropdown menu={{ items }}>
                    <CommonButton btnType="primary" size={'small'} className="btn-icon-right">
                        {t("common.button.action")}<IconAction />
                    </CommonButton>
                </CommonDropdown>
                <CommonButton btnType="primary" size={'small'} className="btn-icon-left" onClick={()=>{setVisibleModalCreate(true)}}>
                    <IconAdd />{t("common.button.addNew")}
                </CommonButton>
            </Space>
        </div>

        <CommonTable
                rowKey={'id'}
                loading={isLoading}
                dataSource={data?.content || []}
                columns={columns}
                data={data}
                onChange={onPageChange}
                rowSelection={{...rowSelection, checkStrictly: false,}}
                defaultSorter={{
                    order: sortTypeQuery,
                    field: sortByQuery,
                }}
                scroll={{x: 1800}}
                expandable={{
                    expandedRowKeys: expandedRowKeys,
                    onExpandedRowsChange: (expandedRows: any) => {
                        setExpandedRowKeys(expandedRows)
                    }
                }}
            /> 

        {visibleModalCreate ?
            <MedicalServicesCreateModal
                dataDetail={currentService}
                modalStatus={visibleModalCreate}
                handleOk={handleOk}
                handleCancel={()=>{setVisibleModalCreate(false); setCurrentService(undefined);}}
            />
            :<></>
        }
    </>
}

export default MedicalServices;