import React, { useEffect, useState } from 'react'
import {useNavigate} from "react-router-dom";
import {Row, Col, Form, Space} from "antd";
import { useTranslation } from 'react-i18next';
import {CaretDownOutlined, CaretUpOutlined} from "@ant-design/icons";
import moment from 'moment';

import { ReactComponent as Search } from '../../../../resources/images/search-1.svg';

import {buildQueryString} from '../../../../utils/utilFunctions'
import {useQuery} from '../../../../utils/customHooks'
import CommonForm from "../../../../components/Common/Form";
import CommonFormItem from '../../../../components/Common/FormItem';
import CommonButton from '../../../../components/Common/Button';
import { SERVICE_PAGE_STATUS, DEFAULT_PAGE_SIZE, SERVICE_PAGE_TYPE } from '../../../../utils/constants';
import CommonInput from '../../../../components/Common/Input';
import CommonRangePicker from '../../../../components/Common/RangePicker';

import userServices from "../../../../services/users.service";
import equipmentServices from "../../../../services/equipments.service";

function MedicalServicesSearchBox(props:any) {
    const componentPath = props?.componentPath
    const navigate = useNavigate();
    const queryObj:any = useQuery();
    const {params = {}, search} = queryObj;
    const {
        advance: advanceQuery,
        pageSize: pageSizeQuery,
        sortBy: sortByQuery,
        sortType: sortTypeQuery,
        search: searchQuery,
    } = params;
    const { t } = useTranslation();
    const [form] = Form.useForm();
    const [advance, setAdvance] = useState(advanceQuery as boolean || false);
    const searchQueryData = searchQuery ? JSON.parse(searchQuery) : {}
    const pageSize = pageSizeQuery ? parseFloat(pageSizeQuery) : DEFAULT_PAGE_SIZE;
    const [dataSelectDoctor, setDataSelectDoctor] = useState<{value:any, label: string}[]>([]);
    const [dataSelectEquipment, setDataSelectEquipment] = useState<{value:any, label: string}[]>([]);

    const getDataEquipment = async () => {
        const resp = await equipmentServices.findAllEquipment();
        const data = resp?.data;
        if (resp?.status === 200) {
            setDataSelectEquipment(data?.data?.filter((item:any)=> item?.isDelete === 0)?.map((item:any)=> ({value: item?.id, label: item?.name})))
        } else {
            setDataSelectEquipment([])
        }
    }

    const getDataDoctor = async ()=>{
        const paramsSearch = {
            page: 0,
            size: 1000,
            search: JSON.stringify({
                searchType: 0,
            }),
        }
        const resp = await userServices.getListAccounts(paramsSearch);
        const data = resp?.data;
        if (resp?.status === 200) {
            setDataSelectDoctor(data?.data?.content?.map((item:any)=> ({value: item?.id, label: item?.fullName})))
        } else {
            setDataSelectDoctor([])
        }
    }

    useEffect(() => {
        getDataEquipment();
        getDataDoctor();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useEffect(() => {
        setAdvance(advanceQuery === 'true')
    }, [advanceQuery])

    const onFinish = (values: any) => {
        onSearch(values)
    }

    const onClear = (clearParams = {}) => {
        onSearch({...clearParams},true)
        setTimeout(() => {
            form.resetFields();
        }, 100)
    }

    const onSearch = (data:any = {},isClear?:boolean) => {
        const dataSearch = {...data};
        if(data?.modifiedDate?.length){
            dataSearch["modifiedDateFromLong"] = moment(data?.modifiedDate?.[0]).startOf("day").valueOf();
            dataSearch["modifiedDateToLong"] = moment(data?.modifiedDate?.[1]).endOf("day").valueOf();
        }
        delete dataSearch.advance;
        delete dataSearch?.modifiedDate;
        let queryString ={ 
            advance, 
            page: 1,
            pageSize: pageSize,
            sortBy: sortByQuery,
            sortType: sortTypeQuery,
            search: JSON.stringify(dataSearch||{}),
        }
        if(isClear){
            queryString.advance = data.advance;
        }
        if (queryString !== search) {
            navigate(`${componentPath}${buildQueryString(queryString) || ''}`)
        } else {
            if (props?.getData) props?.getData()
        }
    }

    return <div className="avic-search-box">
            {
                !advance ?
                <div className="normal-search-box">
                    <CommonForm
                        form={form}
                        onFinish={onFinish}
                        layout="horizontal"
                        initialValues={{
                            keyword: searchQueryData?.keyword,
                        }}
                    >
                        <CommonFormItem name="keyword">
                            <CommonInput
                                placeholder={t("servicePage.search.placeholder.advancedSearch") as string}
                                prefix={
                                    <div
                                        onClick={() => onClear({advance: true})}
                                        className="open-advance-search-btn"
                                    >
                                        {t("servicePage.search.label.advancedSearch")} <CaretDownOutlined />
                                    </div>
                                }
                                addonAfter={
                                    <div onClick={() => form.submit()} className="btn-normal-search">
                                        <Search />
                                    </div>
                                }
                            />
                        </CommonFormItem>
                    </CommonForm>
                </div>
                :
                <div className="advance-search-box">
                    <div className="close-advance-search-btn" >
                        <div onClick={() => onClear({advance: false})} >
                            {t("servicePage.search.title")}
                        </div>
                        <Space>
                            <CaretUpOutlined className="cursor-pointer"  onClick={() =>  onClear({advance: false}) } />
                        </Space>
                    </div>
                    <CommonForm
                        form={form}
                        onFinish={onFinish}
                        layout="vertical"
                        initialValues={{
                            name: searchQueryData?.name,
                            price: searchQueryData?.price,
                            type: searchQueryData?.type,
                            doctorId: searchQueryData?.doctorId,
                            equipmentId: searchQueryData?.equipmentId,
                            modifiedBy: searchQueryData?.modifiedBy,
                            modifiedDate: (searchQueryData?.modifiedDateFromLong && searchQueryData?.modifiedDateToLong) ? [moment(searchQueryData?.modifiedDateFromLong),moment(searchQueryData?.modifiedDateToLong)] : [],
                            status: searchQueryData?.status,
                        }}
                    >
                        <Row gutter={30}>
                            <Col span={12}>
                                <CommonFormItem 
                                    label={t("servicePage.search.label.name")}
                                    placeholder={t("servicePage.search.placeholder.name") as string}
                                    name="name"
                                />
                            </Col>
                            <Col span={12}>
                                <CommonFormItem 
                                    label={t("servicePage.search.label.price")}
                                    placeholder={t("servicePage.search.placeholder.price") as string}
                                    name="price"
                                />
                            </Col>
                        </Row>

                        <Row gutter={30}>
                            <Col span={12}>
                                <CommonFormItem 
                                    label={t("servicePage.search.label.type")}
                                    type= "select"
                                    name="type"
                                    placeholder={t("servicePage.search.placeholder.type") as string}
                                    options={SERVICE_PAGE_TYPE?.map((item:any)=>({value: item.value, label: t(item.label)}))}
                                />
                            </Col>
                            <Col span={12}>
                                <CommonFormItem 
                                    label={t("servicePage.search.label.doctor")}
                                    type= "select"
                                    name="doctorId"
                                    placeholder={t("servicePage.search.placeholder.doctor") as string}
                                    options={dataSelectDoctor}
                                />
                            </Col>
                        </Row>
                        <Row gutter={30}> 
                            <Col span={12}>
                                <CommonFormItem 
                                    label={t("servicePage.search.label.equipment")}
                                    type= "select"
                                    name="equipmentId"
                                    placeholder={t("servicePage.search.placeholder.equipment") as string}
                                    options={dataSelectEquipment}
                                />
                            </Col>
                            <Col span={12}>
                                <CommonFormItem  
                                    name="modifiedDate"
                                    label={t("servicePage.search.label.modifiedDate")}
                                >
                                    <CommonRangePicker />
                                </CommonFormItem>
                            </Col>
                        </Row>
                        <Row gutter={30}>
                            <Col span={12}>
                                <CommonFormItem 
                                    label={t("servicePage.search.label.modifiedBy")}
                                    name="modifiedBy"
                                    placeholder={t("servicePage.search.placeholder.modifiedBy") as string}
                                />
                            </Col>
                            <Col span={12}>
                                <CommonFormItem 
                                    label={t("servicePage.search.label.status")}
                                    type= "select"
                                    name="status"
                                    placeholder={t("servicePage.search.placeholder.status") as string}
                                    options={SERVICE_PAGE_STATUS?.map((item:any)=>({value: item.value, label: t(item.label)}))}
                                />
                            </Col>
                        </Row>

                        <Space className="form-btn-container">
                            <CommonButton btnType="default" size={'small'} onClick={()=> onClear({advance: true})}>
                                {t("common.button.deleteCondition")}
                            </CommonButton>
                            <CommonButton btnType="primary" size={'small'} htmlType="submit">
                                {t("layout.search")}
                            </CommonButton>
                        </Space>
                    </CommonForm>
                </div>
            }
            </div>
}

export default MedicalServicesSearchBox;

