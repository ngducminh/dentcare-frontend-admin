import React, { useEffect, useState } from 'react'
import {Row, Col, Modal, Form, notification, Upload, UploadProps, UploadFile} from 'antd';
import { useTranslation } from 'react-i18next';
import ImgCrop from 'antd-img-crop';
import { RcFile } from 'antd/es/upload';
import { PlusOutlined } from '@ant-design/icons';

import { ReactComponent as UpdateIcon } from '../../../../resources/images/update_icon.svg';
import { ReactComponent as AddIcon } from '../../../../resources/images/plus.svg';
import { ReactComponent as DeleteIcon } from '../../../../resources/images/delete.svg';

import CommonForm from '../../../../components/Common/Form';
import CommonFormItem from '../../../../components/Common/FormItem';
import CommonButton from '../../../../components/Common/Button';
import { EQUIPMENT_PAGE_STATUS, SERVICE_PAGE_TYPE } from '../../../../utils/constants';

import userServices from "../../../../services/users.service";
import equipmentServices from "../../../../services/equipments.service";
import medicalServices from "../../../../services/medicalServices.service";

interface MedicalServicesCreateModalProps {
    dataDetail: any,
    modalStatus: boolean,
    handleOk: () => void,
    handleCancel: () => void
}

function MedicalServicesCreateModal(props:MedicalServicesCreateModalProps) {
    const [form] = Form.useForm();
    const {modalStatus, handleOk, handleCancel, dataDetail} = props
    const { t } = useTranslation();
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [dataSelectDoctor, setDataSelectDoctor] = useState<{value:any, label: string}[]>([]);
    const [dataSelectEquipment, setDataSelectEquipment] = useState<{value:any, label: string}[]>([]);
    const [fileList, setFileList] = useState<UploadFile[]>(dataDetail?.id ?[
        {
            uid: '-1',
            name: 'image.png',
            status: 'done',
            url: dataDetail?.image,
        },
    ]:[]);

    const getDataEquipment = async () => {
        const resp = await equipmentServices.findAllEquipment();
        const data = resp?.data;
        if (resp?.status === 200) {
            setDataSelectEquipment(data?.data?.filter((item:any)=> item?.isDelete === 0)?.map((item:any)=> ({value: item?.id, label: item?.name})))
        } else {
            setDataSelectEquipment([])
        }
    }
    const getDataDoctor = async ()=>{
        const paramsSearch = {
            page: 0,
            size: 1000,
            search: JSON.stringify({
                searchType: 0,
            }),
        }
        const resp = await userServices.getListAccounts(paramsSearch);
        const data = resp?.data;
        if (resp?.status === 200) {
            setDataSelectDoctor(data?.data?.content?.map((item:any)=> ({value: item?.id, label: item?.fullName})))
        } else {
            setDataSelectDoctor([])
        }
    }
    useEffect(() => {
        getDataEquipment();
        getDataDoctor();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    
    const onChange: UploadProps['onChange'] = ({ fileList: newFileList }) => {
        if(newFileList?.length===0){
            setFileList([])
        }else{
            form.setFields([{
                name: "image",
                errors: [""],
            }]);
        }
        console.log("newFileList",newFileList)
    };
    
    const onPreview = async (file: UploadFile) => {
        let src = file.url as string;
        if (!src) {
        src = await new Promise((resolve) => {
            const reader = new FileReader();
            reader.readAsDataURL(file.originFileObj as RcFile);
            reader.onload = () => resolve(reader.result as string);
        });
        }
        const image = new Image();
        image.src = src;
        const imgWindow = window.open(src);
        imgWindow?.document.write(image.outerHTML);
    };

    const onUploadFile = async (options:any) => {
        if (!options?.file) {
            return
        }
        console.log("options?.file",options)
        const reader = new FileReader()
        reader.readAsDataURL(options?.file)
        reader.onload = () => {
            console.log('called: ', reader)
            setFileList([{
                uid: options?.file?.uid,
                name: options?.file?.name,
                status: 'done',
                url: reader.result as string,
            }])
        }
    }

    const onFinish =  (values:any) => {
        if(fileList.length===0){
            form.setFields([{
                name: "image",
                errors: [`${t("validate.select")} ${t("servicePage.form.label.image")}!`],
            }]);
            return
        }
        onCreateService(values);
    }

    const onFinishFailed = (errorInfo:any) => {
        if(fileList.length===0){
            form.setFields([{
                name: "image",
                errors: [`${t("validate.select")} ${t("servicePage.form.label.image")}!`],
            }]);
            return
        }
        console.log('Failed:', errorInfo, fileList);
    };


    const onCreateService = async (values: any) => {
        setIsLoading(true);
        if(dataDetail?.id){
            const dataSubmit={
                service: {
                    ...dataDetail,
                    ...values, 
                    doctors: values?.doctors?.map((item:any)=> ({id: item})),
                    image: fileList[0]?.url,
                },
                slg: values?.equipment?.map((item:any)=>item.quantity),
                equipments: values?.equipment?.map((item:any)=>({id: item.equipmentId})),
            }
            delete dataSubmit?.service?.equipment;
            delete dataSubmit?.service?.admServiceEquipments;
            const resp = await medicalServices.updateService(dataSubmit);
            const data = resp?.data;
            if (resp?.status === 200) {
                notification.success({
                    message: t('servicePage.message.editSuccess'),
                });
                handleOk();
            } else {
                notification.error({
                    message: data?.message || t('commonError.oopsSystem'),
                });
            }
        }else{
            const dataSubmit={
                service: {
                    ...values, 
                    doctors: values?.doctors?.map((item:any)=> ({id: item})),
                    image: fileList[0]?.url,
                },
                slg: values?.equipment?.map((item:any)=>item.quantity),
                equipments: values?.equipment?.map((item:any)=>({id: item.equipmentId})),
            }
            delete dataSubmit?.service?.equipment;
            console.log('dataSubmit',dataSubmit)
            const resp = await medicalServices.createService(dataSubmit);
            const data = resp?.data;
            if (resp?.status === 200) {
                notification.success({
                    message: t('servicePage.message.createSuccess'),
                });
                handleOk();
            } else {
                notification.error({
                    message: data?.message || t('commonError.oopsSystem'),
                });
            }
        }
        setIsLoading(false);
    }

    const onDelete = async ()=>{
        setIsLoading(true);
        Modal.confirm({
            title: t('common.confirmAction'),
            content: t('common.editDataAction'),
            centered: true,
            okText: t('common.button.accept'),
            onOk: async () => { 
                const resp = await medicalServices.deleteService(dataDetail?.id);
                const data = resp?.data;
                if (resp?.status === 200) {
                    notification.success({
                        message: t('servicePage.message.deleteSuccess'),
                    });
                    handleOk();
                } else {
                    notification.error({
                        message: data?.message || t('commonError.oopsSystem'),
                    });
                }
            },
            okButtonProps:{loading: isLoading},
            cancelText: t('common.button.cancel')
        });
        setIsLoading(false);
    }
    console.log("dataDetail",dataDetail)


    return <Modal
        className="service-modal-create"
        title={dataDetail?.id ? t("servicePage.form.editTitle") : t("servicePage.form.createTitle")}
        open={modalStatus}
        maskClosable={false}
        onCancel={handleCancel}
        footer={[
            <CommonButton
                key="close"
                onClick={handleCancel}
                size="small"
            >
                {t("common.button.close")}
            </CommonButton>,
            dataDetail?.id ? <CommonButton
                key="remove"
                onClick={onDelete}
                btnType="danger"
                size="small"
                className="btn-icon-left"
            >
                <><DeleteIcon/> {t("common.button.remove")}</>
            </CommonButton> : null,
            <CommonButton
                form="myForm"
                key="submit"
                htmlType="submit"
                btnType="primary"
                size="small"
                className="btn-icon-left"
                loading={isLoading}
            >
                {dataDetail?.id ? <><UpdateIcon/> {t("common.button.update")}</> : <><AddIcon/> {t("common.button.addNew")}</>}
            </CommonButton>
        ]}
    >
        <CommonForm
            form={form}
            id="myForm"
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            layout="vertical"
            initialValues={{
                name: dataDetail?.name,
                price: dataDetail?.price,
                doctors: dataDetail?.doctors ? dataDetail?.doctors?.map((item:any) => item?.id): [],
                type: dataDetail?.type,
                status: dataDetail?.status,
                description: dataDetail?.description,
                equipment: dataDetail?.admServiceEquipments ? dataDetail?.admServiceEquipments?.map((item:any)=>({equipmentId: item?.admEquipment?.id, quantity: item?.quantity})) : [],
            }}
        >
            <Row gutter={20}>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("servicePage.form.label.name") as string} 
                        name="name"
                        placeholder={t("servicePage.form.placeholder.name") as string}
                        showRequiredIcon
                        rules={[
                            { required: true, message: `${t("validate.input")} ${t("servicePage.form.label.name")}!` }
                        ]}
                    />
                </Col>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("servicePage.form.label.price") as string} 
                        name="price"
                        placeholder={t("servicePage.form.placeholder.price") as string}
                        rules={[
                            { required: true, message: `${t("validate.input")} ${t("servicePage.form.label.price")}!` }
                        ]}
                        type="inputNumber"
                        min={0}
                        showRequiredIcon
                    />
                </Col>
            </Row>
            <Row gutter={20}>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("servicePage.form.label.type") as string}
                        name="type"
                        placeholder={t("servicePage.form.placeholder.type") as string}  
                        showRequiredIcon
                        type="select"
                        options={SERVICE_PAGE_TYPE?.map((item:any)=>({value: item.value, label: t(item.label)}))}
                        rules={[
                            { required: true, message: `${t("validate.select")} ${t("servicePage.form.label.type")}!`},
                        ]}
                    />
                </Col>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("servicePage.form.label.doctor") as string}
                        name="doctors"
                        placeholder={t("servicePage.form.placeholder.doctor") as string}  
                        showRequiredIcon
                        type="select"
                        mode="multiple"
                        options={dataSelectDoctor}
                        rules={[
                            { required: true, message: `${t("validate.select")} ${t("servicePage.form.label.doctor")}!`},
                        ]}
                    />
                </Col>
            </Row>
            <Row gutter={20}>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("servicePage.form.label.status") as string}
                        name="status" 
                        placeholder={t("servicePage.form.placeholder.status") as string}
                        rules={[
                            { required: true, message: `${t("validate.select")} ${t("servicePage.form.label.status")}!`},
                        ]}
                        showRequiredIcon
                        type="select"                                    
                        options={EQUIPMENT_PAGE_STATUS?.map((item:any)=>({value: item.value, label: t(item.label)}))}
                    />
                </Col>
            </Row>
            <div className="title-service">{t("servicePage.form.label.equipmentUse")}</div>
            <Row gutter={20}>
                <Col span={24}>
                    <Form.List name="equipment">
                    {(fields, { add, remove }) => {
                        return (
                        <div>
                            {fields.map((field, index) => (
                            <Row gutter={20} key={field.key}>
                                <Col span={12}>
                                    <CommonFormItem
                                        label={`${t("servicePage.form.label.equipment")} ${index+1}`}
                                        name={[index, "equipmentId"]}
                                        rules={[
                                            { required: true, message: `${t("validate.select")} ${t("servicePage.form.label.equipment")} ${index+1}!`},
                                        ]}
                                        showRequiredIcon
                                        placeholder={`${t("servicePage.form.placeholder.equipment")} ${index+1}` as string}
                                        type='select'
                                        options={dataSelectEquipment}
                                    />
                                </Col>
                                <Col span={9}>
                                    <CommonFormItem
                                        name={[index, "quantity"]}
                                        label={t("servicePage.form.label.quantity")}
                                        type='inputNumber'
                                        min={1}
                                        rules={[
                                            { required: true, message: `${t("validate.input")} ${t("servicePage.form.label.quantity")}!`},
                                        ]}
                                        showRequiredIcon
                                        placeholder={t("servicePage.form.placeholder.quantity") as string}
                                    />
                                </Col>
                                <Col span={3}>
                                    <CommonFormItem
                                        name="button-remove-equipment"
                                        label=" "
                                    >
                                        <CommonButton
                                            key="remove"
                                            onClick={() => remove(field.name)}
                                            btnType="danger"
                                            size="small"
                                            className="btn-icon-left dynamic-delete-button"
                                        >
                                            <DeleteIcon/>
                                        </CommonButton>
                                    </CommonFormItem>
                                </Col>
                            </Row>
                            ))}
                            <Row style={{justifyContent: 'center'}}>
                                <CommonButton
                                    type="dashed"
                                    onClick={() => add()}
                                    style={{ width: "60%" }}
                                    className="btn-icon-left btn-add-equipment"
                                >
                                    <PlusOutlined /> {t('servicePage.form.label.equipmentAdd')}
                                </CommonButton>
                            </Row>
                        </div>
                        );
                    }}
                    </Form.List>
                </Col>
            </Row>
            <Row gutter={20}>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("servicePage.form.label.image") as string} 
                        name="image"
                        placeholder={t("servicePage.form.placeholder.image") as string}
                        showRequiredIcon
                    >
                        <ImgCrop showGrid rotationSlider aspectSlider showReset>
                            <Upload
                                listType= "picture-card"
                                accept="image/png, image/jpeg"
                                fileList={fileList}
                                onChange={onChange}
                                onPreview={onPreview}
                                customRequest={onUploadFile}
                                maxCount={1}
                            >
                                {fileList.length < 1 && '+ Upload'}
                            </Upload>
                        </ImgCrop>
                    </CommonFormItem>
                </Col>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("servicePage.form.label.description") as string}
                        name="description" 
                        placeholder={t("servicePage.form.placeholder.description") as string}
                        type='textArea'
                        rows={5}
                    />
                </Col>
            </Row>

        </CommonForm>
    </Modal>
}

export default MedicalServicesCreateModal;

