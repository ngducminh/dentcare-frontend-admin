import React, { useState } from 'react'
import {Row, Col, Modal, Form, notification} from 'antd';
import { useTranslation } from 'react-i18next';

import { ReactComponent as UpdateIcon } from '../../../../resources/images/update_icon.svg';
import { ReactComponent as AddIcon } from '../../../../resources/images/plus.svg';
import { ReactComponent as DeleteIcon } from '../../../../resources/images/delete.svg';

import CommonForm from '../../../../components/Common/Form';
import CommonFormItem from '../../../../components/Common/FormItem';
import CommonButton from '../../../../components/Common/Button';
import { EQUIPMENT_PAGE_STATUS, EQUIPMENT_PAGE_TYPE } from '../../../../utils/constants';

import equipmentServices from "../../../../services/equipments.service";

interface EquipmentsCreateModalProps {
    dataDetail: any,
    modalStatus: boolean,
    handleOk: () => void,
    handleCancel: () => void
}

function EquipmentsCreateModal(props:EquipmentsCreateModalProps) {
    const [form] = Form.useForm();
    const {modalStatus, handleOk, handleCancel, dataDetail} = props
    const { t } = useTranslation();
    const [isLoading, setIsLoading] = useState<boolean>(false);

    const onFinish =  (values:any) => {
        onCreateEquipment(values);
    }

    const onFinishFailed = (errorInfo:any) => {
        console.log('Failed:', errorInfo);
    };


    const onCreateEquipment = async (values: any) => {
        setIsLoading(true);
        if(dataDetail?.id){
            const resp = await equipmentServices.updateEquipment({...dataDetail,...values});
            const data = resp?.data;
            if (resp?.status === 200) {
                notification.success({
                    message: t('equipmentPage.message.editSuccess'),
                });
                handleOk();
            } else {
                notification.error({
                    message: data?.message || t('commonError.oopsSystem'),
                });
            }
        }else{
            const resp = await equipmentServices.createEquipment(values);
            const data = resp?.data;
            if (resp?.status === 200) {
                notification.success({
                    message: t('equipmentPage.message.createSuccess'),
                });
                handleOk();
            } else {
                notification.error({
                    message: data?.message || t('commonError.oopsSystem'),
                });
            }
        }
        setIsLoading(false);
    }

    const onDelete = async ()=>{
        setIsLoading(true);
        Modal.confirm({
            title: t('common.confirmAction'),
            content: t('common.editDataAction'),
            centered: true,
            okText: t('common.button.accept'),
            onOk: async () => { 
                const resp = await equipmentServices.deleteEquipment(dataDetail?.id);
                const data = resp?.data;
                if (resp?.status === 200) {
                    notification.success({
                        message: t('equipmentPage.message.deleteSuccess'),
                    });
                    handleOk();
                } else {
                    notification.error({
                        message: data?.message || t('commonError.oopsSystem'),
                    });
                }
            },
            okButtonProps:{loading: isLoading},
            cancelText: t('common.button.cancel')
        });
        setIsLoading(false);
    }

    return <Modal
        title={dataDetail?.id ? t("equipmentPage.form.editTitle") : t("equipmentPage.form.createTitle")}
        open={modalStatus}
        maskClosable={false}
        onCancel={handleCancel}
        footer={[
            <CommonButton
                key="close"
                onClick={handleCancel}
                size="small"
            >
                {t("common.button.close")}
            </CommonButton>,
            dataDetail?.id ? <CommonButton
                key="remove"
                onClick={onDelete}
                btnType="danger"
                size="small"
                className="btn-icon-left"
            >
                <><DeleteIcon/> {t("common.button.remove")}</>
            </CommonButton> : null,
            <CommonButton
                form="myForm"
                key="submit"
                htmlType="submit"
                btnType="primary"
                size="small"
                className="btn-icon-left"
                loading={isLoading}
            >
                {dataDetail?.id ? <><UpdateIcon/> {t("common.button.update")}</> : <><AddIcon/> {t("common.button.addNew")}</>}
            </CommonButton>
        ]}
    >
        <CommonForm
            form={form}
            id="myForm"
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            layout="vertical"
            initialValues={{
                name: dataDetail?.name,
                price: dataDetail?.price,
                type: dataDetail?.type ,
                status: dataDetail?.status,
                description: dataDetail?.description,
            }}
        >
            <Row gutter={20}>
                <Col span={24}>
                    <CommonFormItem 
                        label={t("equipmentPage.form.label.name") as string} 
                        name="name"
                        placeholder={t("equipmentPage.form.placeholder.name") as string}
                        showRequiredIcon
                        rules={[
                            { required: true, message: `${t("validate.input")} ${t("equipmentPage.form.label.name")}!` }
                        ]}
                    />
                </Col>
                <Col span={24}>
                    <CommonFormItem 
                        label={t("equipmentPage.form.label.price") as string} 
                        name="price"
                        placeholder={t("equipmentPage.form.placeholder.price") as string}
                        rules={[
                            { required: true, message: `${t("validate.input")} ${t("equipmentPage.form.label.price")}!` }
                        ]}
                        type="inputNumber"
                        min={0}
                        showRequiredIcon
                    />
                </Col>
                <Col span={24}>
                    <CommonFormItem 
                        label={t("equipmentPage.form.label.type") as string}
                        name="type"
                        placeholder={t("equipmentPage.form.placeholder.type") as string}  
                        showRequiredIcon
                        type="select"
                        options={EQUIPMENT_PAGE_TYPE?.map((item:any)=>({value: item.value, label: t(item.label)}))}
                        rules={[
                            { required: true, message: `${t("validate.select")} ${t("equipmentPage.form.label.type")}!`},
                        ]}
                    />
                </Col>
                <Col span={24}>
                    <CommonFormItem 
                        label={t("equipmentPage.form.label.status") as string}
                        name="status" 
                        placeholder={t("equipmentPage.form.placeholder.status") as string}
                        rules={[
                            { required: true, message: `${t("validate.select")} ${t("equipmentPage.form.label.status")}!`},
                        ]}
                        showRequiredIcon
                        type="select"                                    
                        options={EQUIPMENT_PAGE_STATUS?.map((item:any)=>({value: item.value, label: t(item.label)}))}
                    />
                </Col>
                <Col span={24}>
                    <CommonFormItem 
                        label={t("equipmentPage.form.label.description") as string}
                        name="description" 
                        placeholder={t("equipmentPage.form.placeholder.description") as string}
                        type='textArea'
                        rows={4}
                    />
                </Col>
            </Row>

        </CommonForm>
    </Modal>
}

export default EquipmentsCreateModal;

