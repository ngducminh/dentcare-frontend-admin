import React, {useEffect, useState} from 'react'
import {useQuery} from '../../utils/customHooks'
import {useTranslation} from "react-i18next";
import LocalStore from "../../utils/localStorage";
import ProfileSidebar from "./ProfileSidebar";
import ProfileTabs from "./ProfileTabs";
import {Row, Col} from "antd";

function Profile () {
    const { t } = useTranslation();

    const queryObj:any = useQuery();
    const {params = {}} = queryObj
    const {
        tab: tabQuery,
    } = params

    useEffect(() => {

    }, [])

    return <Row className="profile-page">
        <Col span={6}>
            <ProfileSidebar/>
        </Col>
        <Col span={18}>
            <ProfileTabs />
        </Col>
    </Row>
}

export default Profile;

