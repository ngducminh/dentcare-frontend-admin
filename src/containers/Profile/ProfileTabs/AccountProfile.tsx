import React, {forwardRef, useImperativeHandle, useState} from 'react'
import {useTranslation} from "react-i18next";
import {EditOutlined} from '@ant-design/icons';
import moment from "moment";
import {Form, notification, Space} from "antd";
import {useDispatch, useSelector} from "react-redux";

import CommonButton from "../../../components/Common/Button";
import CommonForm from "../../../components/Common/Form";
import CommonFormItem from "../../../components/Common/FormItem";
import {
    DATE_FORMAT,
    DOCTOR_PAGE_PERMISSION,
    REGEX_ONLY_NUMBER,
} from "../../../utils/constants";
import {saveProfile} from "../../../redux/actions/profile.actions";

import userServices from "../../../services/users.service";

function AccountProfile (props:any, ref:any) {
    const dispatch = useDispatch();
    const { t } = useTranslation();
    const [form] = Form.useForm();
    const [isEdit, setIsEdit] = useState(false)

    const { profile} = useSelector((state:any) => state?.profileReducer);

    const [isLoading, setIsLoading] = useState(false)

    useImperativeHandle(ref, () => ({
        isEdit,
        onClear
    }));

    const onFinish = (values: any) => {
        onSubmit(values)
    }

    const onSubmit = async (values:any) => {
        const body = {
            ...profile,
            fullName: values.fullName,
            email: values.email,
            phoneNumber: values.phoneNumber,
            dob: values.dob,
        }
        let resp;

        setIsLoading(true)
        if(profile?.admUser?.permission===1){
            resp = await userServices.updateDoctor(body);
        }else{
            resp = await userServices.updateStaff(body);
        }
        const data = resp?.data;
        if (resp?.status === 200) {
            notification.success({
                message: t('profilePage.submitSuccess'),
            });
            dispatch(saveProfile({
                ...profile,
                ...data?.data
            }));
        } else {
            notification.error({
                message: data?.message || t('commonError.oopsSystem'),
            });
            setIsLoading(false);
        }
        setIsLoading(false);
        setIsEdit(false)
    }

    const onClear = () => {
        form.resetFields();
        setIsEdit(false)
    }

    return <div className="profile-tab-content">
        <div className="profile-tab-content-header">
            <div className="profile-tab-content-header-left">
                {t('profilePage.accountProfileTab')}
            </div>
            <div className="profile-tab-content-header-right">
                {
                    !isEdit &&
                    <EditOutlined className='cursor-pointer' onClick={() => setIsEdit(true)}/>
                }
            </div>
        </div>

        <CommonForm
            form={form}
            onFinish={onFinish}
            layout="horizontal"
            initialValues={{
                fullName: profile?.fullName,
                username: profile?.admUser?.username,
                employeeCode: profile?.admUser?.permission===1 ? profile?.doctorCode : profile?.staffCode,
                role: profile?.admUser?.permission,
                email: profile?.email,
                phoneNumber: profile?.phoneNumber,
                dob: profile?.dob ? moment(profile?.dob) : '',
            }}
            isLoading={isLoading}
        >
            <CommonFormItem
                isView={!isEdit}
                name="fullName"
                label={t('profilePage.label.fullName')}
                placeholder={t('profilePage.placeholder.fullName') as string}
                rules={[
                    { whitespace: true, required: true, message: t('validate.fullNameRequired') as string }
                ]}
            />
            <CommonFormItem
                isView={!isEdit}
                name="username"
                label={t('profilePage.label.userName')}
                placeholder={t('profilePage.placeholder.userName') as string}
                disabled={true}
            />
            <CommonFormItem
                isView={!isEdit}
                name="employeeCode"
                label={ profile?.admUser?.permission===1 ? t('profilePage.label.doctorCode') : t('profilePage.label.employeeCode')}
                placeholder={ profile?.admUser?.permission===1 ? t('profilePage.placeholder.doctorCode') as string :  t('profilePage.placeholder.employeeCode') as string}
                disabled={true}
            />
            <CommonFormItem
                isView={!isEdit}
                name="role"
                label={t('profilePage.label.roles')}
                placeholder={t('profilePage.placeholder.roles') as string}
                disabled={true}
                type='select'
                options={DOCTOR_PAGE_PERMISSION?.map((item:any)=>({value: item.value, label: t(item.label)}))}
            />
            <CommonFormItem
                isView={!isEdit}
                name="email"
                label={t('profilePage.label.email')}
                placeholder={t('profilePage.placeholder.email') as string}
            />
            <CommonFormItem
                isView={!isEdit}
                name="phoneNumber"
                label={t('profilePage.label.phoneNumber')}
                placeholder={t('profilePage.placeholder.phoneNumber') as string}
                rules={[
                    { pattern: new RegExp(REGEX_ONLY_NUMBER), message: t('validate.phoneNumberFormat') as string },
                ]}
                maxLength={10}
            />
            <CommonFormItem
                isView={!isEdit}
                name="dob"
                label={t('profilePage.label.dateOfBirth')}
                placeholder={t('profilePage.placeholder.dateOfBirth') as string}
                type={'datePicker'}
                format={DATE_FORMAT}
            />
            {
                isEdit && <Space className="form-btn-container">
                    <CommonButton size={'small'} onClick={onClear}>
                        {t('common.button.cancel')}
                    </CommonButton>
                    <CommonButton btnType="primary" size={'small'} htmlType="submit">
                        {t('common.button.save')}
                    </CommonButton>
                </Space>
            }
        </CommonForm>

    </div>
}

export default forwardRef(AccountProfile);

