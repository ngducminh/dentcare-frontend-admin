import React, {forwardRef, useEffect, useImperativeHandle, useState} from 'react'
import {useTranslation} from "react-i18next";
import {EditOutlined} from '@ant-design/icons'
import {Form, notification, Space} from "antd";

import CommonButton from "../../../components/Common/Button";
import CommonForm from "../../../components/Common/Form";
import CommonFormItem from "../../../components/Common/FormItem";
import {changeThemeCss} from "../../../utils/utilFunctions";
import {useDispatch, useSelector} from "react-redux";
import {LANGUAGE_LIST} from "../../../utils/constants";
// import usersServices from "../../../services/users.service";
// import {saveProfile} from "../../../redux/actions/profile.actions";

const listBg = [
    {
        id: 1,
        background: '#D9D9D9'
    },
    {
        id: 2,
        background: '#61b6fb'
    },
    {
        id: 3,
        background: '#ffd36a'
    },
    {
        id: 4,
        background: '#68d0ba'
    },
    {
        id: 5,
        background: '#5C4E8E'
    },
]

function AccountConfig (props:any, ref:any) {
    const dispatch = useDispatch();
    const { t } = useTranslation();
    const [form] = Form.useForm();
    const [isEdit, setIsEdit] = useState(false)

    const {
        profile
    } = useSelector((state:any) => state?.profileReducer);

    const [isLoading, setIsLoading] = useState(false)

    useImperativeHandle(ref, () => ({
        isEdit,
        onClear
    }));

    useEffect(() => {

    }, [])

    const onFinish = (values: any) => {
        onSubmit(values)
    }

    const onSubmit = async (values:any) => {
        // const body = {
        //     "address": profile?.address,
        //     "avatar": profile?.avatar,
        //     "configUserInfo": {
        //         ...profile?.configUserInfo,
        //         "background": values?.background,
        //         "hourNose": values?.hourNose,
        //         "language": values?.language,
        //     },
        //     "dateOfBirth": profile?.dateOfBirth,
        //     "employeeCode": profile?.employeeCode,
        //     "faxNumber": profile?.faxNumber,
        //     "fullName": profile?.fullName,
        //     "phoneNumber": profile?.phoneNumber,
        //     "sex": profile?.sex
        // }

        // console.log('body', body)
        setIsLoading(true)
        // const resp = await usersServices.updateProfile(body);
        // const data = resp?.data;
        // // const headers = resp?.headers;
        // if (resp?.status === 200) {
            setIsEdit(false)
            // dispatch(saveProfile(data?.data))
            notification.success({
                message: t('profilePage.submitSuccess'),
            });
        // } else {
        //     notification.error({
        //         message: data?.message || t('commonError.oopsSystem'),
        //     });
        // }
        setIsLoading(false)
    }

    const onClear = () => {
        form.resetFields();
        setIsEdit(false)
    }

    const handleChangeTheme = (data:any) => {
        changeThemeCss(data)
        if (form) {
            form.setFieldValue('background', data.id)
        }
    }

    return <div className="profile-tab-content">
        <div className="profile-tab-content-header">
            <div className="profile-tab-content-header-left">
                {t('profilePage.accountConfigTab')}
            </div>
            <div className="profile-tab-content-header-right">
                {
                    !isEdit &&
                    <EditOutlined className='cursor-pointer' onClick={() => setIsEdit(true)}/>
                }
            </div>
        </div>

        <CommonForm
            form={form}
            onFinish={onFinish}
            layout="horizontal"
            initialValues={{
                language: profile?.configUserInfo?.language,
                hourNose: profile?.configUserInfo?.hourNose,
                background: profile?.configUserInfo?.language,
            }}
            isLoading={isLoading}
        >
            <CommonFormItem
                isView={!isEdit}
                name="language"
                label={'Ngôn ngữ'}
                placeholder="Chọn ngôn ngữ"
                options={LANGUAGE_LIST}
                type={'select'}
            />
            <CommonFormItem
                isView={!isEdit}
                name="hourNose"
                label={t("profilePage.label.hourNose")}
                placeholder={t("profilePage.placeholder.hourNose") as string}
            />
            <CommonFormItem
                isView={!isEdit}
                name="weatherZone"
                label={t("profilePage.label.weatherZone")}
                placeholder={t("profilePage.placeholder.weatherZone") as string}
            />
            <CommonFormItem
                isView={!isEdit}
                name="background"
                label={t("profilePage.label.systemColor")}
            >
                <Space className="account-config-bg-list">
                    {
                        listBg.map((item, index) => {
                            return <div
                                key={index}
                                style={{background: item.background}}
                                className={`account-config-bg-item cursor-pointer ${form.getFieldValue('background') === item.id ? 'active' : ''}`}
                                onClick={() => handleChangeTheme(item)}
                            >
                            </div>
                        })
                    }
                </Space>
            </CommonFormItem>

            {
                isEdit && <Space className="form-btn-container">
                    <CommonButton size={'small'} onClick={onClear}>
                        {t('common.button.cancel')}
                    </CommonButton>
                    <CommonButton btnType="primary" size={'small'} htmlType="submit">
                        {t('common.button.save')}
                    </CommonButton>
                </Space>
            }
        </CommonForm>

    </div>
}

export default forwardRef(AccountConfig);

