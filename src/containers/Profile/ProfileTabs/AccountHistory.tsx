import React, {useEffect, useState} from 'react'
import {useTranslation} from "react-i18next";
import {notification, Steps} from "antd";
// import actionsServices from "../../../services/actions.service";
import CommonRangePicker from "../../../components/Common/RangePicker";
import CommonEmpty from "../../../components/Common/Empty";
import {modifyResultActions} from "../../../utils/utilFunctions";
import CommonSpin from "../../../components/Common/Spin";

function AccountHistory () {
    const { t } = useTranslation();
    // const [isEdit, setIsEdit] = useState(false)
    const [data, setData] = useState([])
    const [isLoading, setIsLoading] = useState(false)

    // const [dateRange, setDateRange] = useState<any>([]);

    // useEffect(() => {
    //     getData()
    // }, [])

    // const getData = async (dateRange?:any) => {
    //     // let query:string = buildQueryString()
    //     setIsLoading(true)
    //     let paramsSearch = {}
    //     if (dateRange) {
    //         paramsSearch = {
    //             fromCreateDate: dateRange[0] ? dateRange[0].toISOString() : '',
    //             toCreateDate: dateRange[1] ? dateRange[1].toISOString() : '',
    //         }
    //     }
    //     const resp = await actionsServices.getAccountActionLogs(paramsSearch);
    //     const data = resp?.data;
    //     // const headers = resp?.headers;
    //     if (resp?.status === 200) {
    //         const newData:any = modifyResultActions(data)
    //         setData(newData)
    //         setIsLoading(false)
    //     }  else {
    //         notification.error({
    //             message: data?.message || t('commonError.oopsSystem'),
    //         });
    //         setIsLoading(false)
    //     }
    // }

    // console.log('dateRange', dateRange)

    return <div className="profile-tab-content">

        <div className="profile-tab-content-header">
            <div className="profile-tab-content-header-left">
                {t('profilePage.accountActionLogsTab')}
            </div>
            {/*<div className="profile-tab-content-header-right">*/}
            {/*    <FileSearchOutlined*/}
            {/*        className='cursor-pointer'*/}
            {/*        onClick={() => setIsEdit(!isEdit)}*/}
            {/*    />*/}
            {/*</div>*/}
        </div>
        <div className="dpl-flex align-items-center width-350 pdbt-15">
            <CommonRangePicker
                placeholder={[t('common.fromDate'), t('common.toDate')]}
                onChange={(val:any) => {
                    // setDateRange(val)
                    // getData(val)
                }}
            />
        </div>

        <CommonSpin isLoading={isLoading}>
            {
                data.length === 0
                    ?
                    <CommonEmpty />
                    :
                    <Steps
                        progressDot
                        current={data.length}
                        direction="vertical"
                        items={data}
                        className="avic-vertical-step"
                    />
            }
        </CommonSpin>
    </div>
}

export default AccountHistory;

