import React from 'react'
import {useTranslation} from "react-i18next";
import ImgCrop from 'antd-img-crop';
import {notification, Upload} from 'antd';
import {ReactComponent as CameraIcon} from "../../resources/images/camera-icon.svg";
import { useDispatch, useSelector } from 'react-redux';
import { saveProfile } from '../../redux/actions/profile.actions';

import userServices from "../../services/users.service";

function CameraAntd(props:any) {
    const { t } = useTranslation();
    const dispatch = useDispatch();
    const { profile} = useSelector((state:any) => state?.profileReducer);

    const onSubmit = async (values:any) => {
        props?.setIsLoading(true);
        const body = {
            ...profile?.admUser,
            avatar: values,
        }
        const resp = await userServices.updateUser(body);
        const data = resp?.data;
        if (resp?.status === 200) {
            notification.success({
                message: t('profilePage.changeAvatar'),
            });
            dispatch(saveProfile({
                ...profile,
                admUser: {
                    ...data?.data
                }
            }))
        } else {
            notification.error({
                message: data?.message || t('commonError.oopsSystem'),
            });
        }
        props?.setIsLoading(false);
    }

    const onUploadFile = async (options:any) => {
        if (!options?.file) {
            return
        }
        console.log("options?.file",options)
        const reader = new FileReader()
        reader.readAsDataURL(options?.file)
        reader.onload = () => {
            console.log('called: ', reader)
            onSubmit(reader.result)
        }
    }

    return (
        <div className="avatar-change-icon">
            <ImgCrop showGrid rotationSlider aspectSlider showReset>
                <Upload
                    fileList={[]}
                    customRequest={onUploadFile}
                >
                    <CameraIcon />
                </Upload>
            </ImgCrop>
        </div>
    );
}

export default CameraAntd;

