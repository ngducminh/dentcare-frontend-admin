import React, { useState } from 'react'
import {Row, Col, Upload, UploadProps, UploadFile, Radio} from 'antd';
import { useTranslation } from 'react-i18next';
import ImgCrop from 'antd-img-crop';
import { RcFile } from 'antd/es/upload';
import moment from 'moment';

import CommonForm from '../../../components/Common/Form';
import CommonFormItem from '../../../components/Common/FormItem';
import { DOCTOR_PAGE_STATUS, DATE_FORMAT, REGEX_PHONE_NUMBER } from '../../../utils/constants';

export interface StaffInfoTabProps{
    dataDetail: any,
    formStaffInfo: any,
    onCreateStaff: any
}

function StaffInfoTab(props: StaffInfoTabProps) {
    const { t } = useTranslation();
    const { dataDetail, formStaffInfo, onCreateStaff } = props;
    const [fileList, setFileList] = useState<UploadFile[]>(dataDetail?.id ?[
        {
            uid: '-1',
            name: 'image.png',
            status: 'done',
            url: dataDetail?.image,
        },
    ]:[]);
    
    const onChange: UploadProps['onChange'] = ({ fileList: newFileList }) => {
        if(newFileList?.length===0){
            setFileList([])
        }else{
            formStaffInfo.setFields([{
                name: "image",
                errors: [""],
            }]);
        }
        console.log("newFileList",newFileList)
    };
    
    const onPreview = async (file: UploadFile) => {
        let src = file.url as string;
        if (!src) {
        src = await new Promise((resolve) => {
            const reader = new FileReader();
            reader.readAsDataURL(file.originFileObj as RcFile);
            reader.onload = () => resolve(reader.result as string);
        });
        }
        const image = new Image();
        image.src = src;
        const imgWindow = window.open(src);
        imgWindow?.document.write(image.outerHTML);
    };

    const onUploadFile = async (options:any) => {
        if (!options?.file) {
            return
        }
        console.log("options?.file",options)
        const reader = new FileReader()
        reader.readAsDataURL(options?.file)
        reader.onload = () => {
            console.log('called: ', reader)
            setFileList([{
                uid: options?.file?.uid,
                name: options?.file?.name,
                status: 'done',
                url: reader.result as string,
            }])
        }
    }

    const onFinishStaffInfo =  (values:any) => {
        if(fileList.length===0){
            formStaffInfo.setFields([{
                name: "image",
                errors: [`${t("validate.select")} ${t("staffPage.form.label.image")}!`],
            }]);
            onCreateStaff(false);
            return
        }else{
            formStaffInfo.setFieldValue("image",fileList[0]?.url)
            onCreateStaff(true);
        }
    }

    const onFinishStaffInfoFailed = (errorInfo:any) => {
        if(fileList.length===0){
            formStaffInfo.setFields([{
                name: "image",
                errors: [`${t("validate.select")} ${t("staffPage.form.label.image")}!`],
            }]);
            return
        }
        onCreateStaff(false)
        console.log('Failed:', errorInfo, fileList);
    };

    return (
        <CommonForm
            form={formStaffInfo}
            onFinish={onFinishStaffInfo}
            onFinishFailed={onFinishStaffInfoFailed}
            layout="vertical"
            initialValues={{
                staffCode: dataDetail?.staffCode ? dataDetail?.staffCode : `KH_${moment().unix()}`,
                fullName: dataDetail?.fullName,
                dob: dataDetail?.dob ? moment(dataDetail?.dob) : undefined,
                gender: dataDetail ? dataDetail?.gender : 0,
                email: dataDetail?.email,
                phoneNumber: dataDetail?.phoneNumber,
                address: dataDetail?.address,
                status: dataDetail?.status,
                note: dataDetail?.note,
            }}
        >
            <Row gutter={20}>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("staffPage.form.label.staffCode") as string} 
                        name="staffCode"
                        placeholder={t("staffPage.form.placeholder.staffCode") as string}
                        disabled
                    />
                </Col>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("staffPage.form.label.fullName") as string} 
                        name="fullName"
                        placeholder={t("staffPage.form.placeholder.fullName") as string}
                        showRequiredIcon
                        rules={[
                            { required: true, message: `${t("validate.input")} ${t("staffPage.form.label.fullName")}!` }
                        ]}
                    />
                </Col>
            </Row>
            <Row gutter={20}>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("staffPage.form.label.dob") as string} 
                        name="dob"
                        placeholder={t("staffPage.form.placeholder.dob") as string}
                        type='datePicker'
                        showRequiredIcon
                        rules={[
                            { required: true, message: `${t("validate.select")} ${t("staffPage.form.label.dob")}!` }
                        ]}
                        disabledDate={(current:any)=> current >= moment().endOf("day").toDate()}
                        format={DATE_FORMAT}
                    />
                </Col>
                <Col span={12}>
                    <CommonFormItem
                        name="gender"
                        label={t('staffPage.form.label.gender')}
                        showRequiredIcon
                    >
                        <Radio.Group >
                            <Radio value={0}>{t("staffPage.options.gender.male")}</Radio>
                            <Radio value={1}>{t("staffPage.options.gender.female")}</Radio>
                        </Radio.Group>
                    </CommonFormItem>
                </Col>
            </Row>
            <Row gutter={20}>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("staffPage.form.label.email") as string}
                        name="email"
                        placeholder={t("staffPage.form.placeholder.email") as string}  
                        showRequiredIcon
                        rules={[
                            { required: true, message: `${t("validate.input")} ${t("staffPage.form.label.email")}!`},
                        ]}
                    />
                </Col>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("staffPage.form.label.phoneNumber") as string}
                        name="phoneNumber"
                        placeholder={t("staffPage.form.placeholder.phoneNumber") as string}  
                        showRequiredIcon
                        rules={[
                            { required: true, message: `${t("validate.input")} ${t("staffPage.form.label.phoneNumber")}!`},
                            { pattern: new RegExp(REGEX_PHONE_NUMBER), message: t('validate.phoneNumberFormat') as string },
                        ]}
                        maxLength={10}
                    />
                </Col>
            </Row>
            <Row gutter={20}>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("staffPage.form.label.address") as string}
                        name="address"
                        placeholder={t("staffPage.form.placeholder.address") as string}  
                        showRequiredIcon
                        rules={[
                            { required: true, message: `${t("validate.input")} ${t("staffPage.form.label.address")}!`},
                        ]}
                    />
                </Col>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("staffPage.form.label.status") as string}
                        name="status" 
                        placeholder={t("staffPage.form.placeholder.status") as string}
                        rules={[
                            { required: true, message: `${t("validate.select")} ${t("staffPage.form.label.status")}!`},
                        ]}
                        showRequiredIcon
                        type="select"                                    
                        options={DOCTOR_PAGE_STATUS?.map((item:any)=>({value: item.value, label: t(item.label)}))}
                    />
                </Col>
            </Row>
            <Row gutter={20}>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("staffPage.form.label.image") as string} 
                        name="image"
                        placeholder={t("staffPage.form.placeholder.image") as string}
                        showRequiredIcon
                    >
                        <ImgCrop showGrid rotationSlider aspectSlider showReset>
                            <Upload
                                listType= "picture-card"
                                accept="image/png, image/jpeg"
                                fileList={fileList}
                                onChange={onChange}
                                onPreview={onPreview}
                                customRequest={onUploadFile}
                                maxCount={1}
                            >
                                {fileList.length < 1 && '+ Upload'}
                            </Upload>
                        </ImgCrop>
                    </CommonFormItem>
                </Col>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("staffPage.form.label.note") as string}
                        name="note" 
                        placeholder={t("staffPage.form.placeholder.note") as string}
                        type='textArea'
                        rows={5}
                    />
                </Col>
            </Row>
        </CommonForm>
    )
}

export default StaffInfoTab