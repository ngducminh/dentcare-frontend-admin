import React, { useEffect, useState } from 'react'
import {useNavigate} from "react-router-dom";
import {Row, Col, Form, Space} from "antd";
import { useTranslation } from 'react-i18next';
import {CaretDownOutlined, CaretUpOutlined} from "@ant-design/icons";
import moment from "moment";

import { ReactComponent as Search } from '../../../resources/images/search-1.svg';

import {useQuery} from '../../../utils/customHooks'
import CommonForm from "../../../components/Common/Form";
import CommonFormItem from '../../../components/Common/FormItem';
import CommonButton from '../../../components/Common/Button';
import { STAFF_PAGE_STATUS, DEFAULT_PAGE_SIZE } from '../../../utils/constants';
import CommonInput from '../../../components/Common/Input';
import CommonRangePicker from '../../../components/Common/RangePicker';
import { buildQueryString } from '../../../utils/utilFunctions';

function StaffsSearchBox(props:any) {                       // tìm kiếm nhân viên
    const componentPath = props?.componentPath                              // đường dẫn
    const navigate = useNavigate();                                 // dùng để chuyển trang
    const queryObj:any = useQuery();                                    // lấy ra các params trên url
    const {params = {}, search} = queryObj;                  // params: các params trên url, search: chuỗi params trên url
    const {
        advance: advanceQuery,                                          // advanceQuery: params advance trên url
        pageSize: pageSizeQuery,                                            // pageSizeQuery: params pageSize trên url
        sortBy: sortByQuery,                                            // sortByQuery: params sortBy trên url
        sortType: sortTypeQuery,                                        // sortTypeQuery: params sortType trên url
        search: searchQuery,                                                // searchQuery: params search trên url
    } = params;
    const { t } = useTranslation();                                     
    const [form] = Form.useForm();                                      // tạo ra 1 form
    const [advance, setAdvance] = useState(advanceQuery as boolean || false);           // advance: trạng thái tìm kiếm nâng cao
    const searchQueryData = searchQuery ? JSON.parse(searchQuery) : {}                  // searchQueryData: dữ liệu tìm kiếm trên url
    const pageSize = pageSizeQuery ? parseFloat(pageSizeQuery) : DEFAULT_PAGE_SIZE;         // pageSize: số lượng item trên 1 trang

    useEffect(() => {
        setAdvance(advanceQuery === 'true')                 // set trạng thái tìm kiếm nâng cao
    }, [advanceQuery])                                      // nếu advanceQuery thay đổi thì chạy lại useEffect

    const onFinish = (values: any) => {
        onSearch(values)
    }

    const onClear = (clearParams = {}) => {                     // xóa các params
        onSearch({...clearParams},true)                         // gọi hàm tìm kiếm với clearParams là các params cần xóa
        setTimeout(() => {
            form.resetFields();                                 // reset form
        }, 100)
    }

    const onSearch = (data:any = {},isClear?:boolean) => {              // tìm kiếm với data là dữ liệu tìm kiếm, isClear: xóa các params
        const dataSearch = {...data};                                       // tạo ra 1 biến dataSearch bằng data
        if(data?.modifiedDate?.length){                                     // nếu có modifiedDate thì set modifiedDateFromLong và modifiedDateToLong
            dataSearch["modifiedDateFromLong"] = moment(data?.modifiedDate?.[0]).startOf("day").valueOf();  // lấy ra ngày bắt đầu
            dataSearch["modifiedDateToLong"] = moment(data?.modifiedDate?.[1]).endOf("day").valueOf();      // lấy ra ngày kết thúc
        }
        delete dataSearch.advance;                                  // xóa advance trong dataSearch
        delete dataSearch?.modifiedDate;                            // xóa modifiedDate trong dataSearch
        let queryString ={                                          // tạo ra 1 biến queryString
            advance, 
            page: 1,
            pageSize: pageSize,
            sortBy: sortByQuery,
            sortType: sortTypeQuery,
            search: JSON.stringify(dataSearch||{}),
        }
        if(isClear){
            queryString.advance = data.advance;                                     // nếu là xóa thì set lại advance
        }
        if (queryString !== search) {                                           // nếu queryString khác search thì chuyển trang
            navigate(`${componentPath}${buildQueryString(queryString) || ''}`)              // chuyển trang
        } else {
            if (props?.getData) props?.getData()                                    // nếu ko thì gọi hàm getData
        }
    }

    return <div className="avic-search-box">
            {
                !advance ?
                <div className="normal-search-box">
                    <CommonForm
                        form={form}
                        onFinish={onFinish}
                        layout="horizontal"
                        initialValues={{
                            keyword: searchQueryData?.keyword,
                        }}
                    >
                        <CommonFormItem name="keyword">
                            <CommonInput
                                placeholder={t("staffPage.search.placeholder.advancedSearch") as string}
                                prefix={
                                    <div
                                        onClick={() => onClear({advance: true})}    // xóa các params
                                        className="open-advance-search-btn"
                                    >
                                        {t("staffPage.search.label.advancedSearch")} <CaretDownOutlined />
                                    </div>
                                }
                                addonAfter={
                                    <div onClick={() => form.submit()} className="btn-normal-search">
                                        <Search />
                                    </div>
                                }
                            />
                        </CommonFormItem>
                    </CommonForm>
                </div>
                :
                <div className="advance-search-box">
                    <div className="close-advance-search-btn" >
                        <div onClick={() => onClear({advance: false})} >
                            {t("staffPage.search.title")}
                        </div>
                        <Space>
                            <CaretUpOutlined className="cursor-pointer"  onClick={() =>  onClear({advance: false}) } />
                        </Space>
                    </div>
                    <CommonForm
                        form={form}
                        onFinish={onFinish}
                        layout="vertical"
                        initialValues={{                                // set giá trị mặc định cho form
                            staffCode: searchQueryData?.staffCode,      // set giá trị mặc định cho form
                            fullName: searchQueryData?.fullName,
                            email: searchQueryData?.email,
                            phoneNumber: searchQueryData?.phoneNumber,
                            username: searchQueryData?.username,
                            modifiedBy: searchQueryData?.modifiedBy,
                            modifiedDate: (searchQueryData?.modifiedDateFromLong && searchQueryData?.modifiedDateToLong) ? [moment(searchQueryData?.modifiedDateFromLong),moment(searchQueryData?.modifiedDateToLong)] : [],
                            status: searchQueryData?.status,
                        }}
                    >
                        <Row gutter={30}>
                            <Col span={12}>
                                <CommonFormItem 
                                    label={t("staffPage.search.label.staffCode")}
                                    placeholder={t("staffPage.search.placeholder.staffCode") as string}
                                    name="staffCode"
                                />
                            </Col>
                            <Col span={12}>
                                <CommonFormItem 
                                    label={t("staffPage.search.label.fullName")}
                                    placeholder={t("staffPage.search.placeholder.fullName") as string}
                                    name="fullName"
                                />
                            </Col>
                        </Row>
                        
                        <Row gutter={30}>
                            <Col span={12}>
                                <CommonFormItem 
                                    label={t("staffPage.search.label.email")}
                                    placeholder={t("staffPage.search.placeholder.email") as string}
                                    name="email"
                                />
                            </Col>
                            <Col span={12}>
                                <CommonFormItem 
                                    label={t("staffPage.search.label.phoneNumber")}
                                    placeholder={t("staffPage.search.placeholder.phoneNumber") as string}
                                    name="phoneNumber"
                                />
                            </Col>
                        </Row>

                        <Row gutter={30}>
                            <Col span={12}>
                                <CommonFormItem 
                                    label={t("staffPage.search.label.userName")}
                                    placeholder={t("staffPage.search.placeholder.userName") as string}
                                    name="username"
                                />
                            </Col>
                            <Col span={12}>
                                <CommonFormItem  
                                    name="modifiedDate"
                                    label={t("staffPage.search.label.modifiedDate")}
                                >
                                    <CommonRangePicker />
                                </CommonFormItem>
                            </Col>
                        </Row>
                        <Row gutter={30}> 
                            <Col span={12}>
                                <CommonFormItem 
                                    label={t("staffPage.search.label.modifiedBy")}
                                    name="modifiedBy"
                                    placeholder={t("staffPage.search.placeholder.modifiedBy") as string}
                                />
                            </Col>
                            <Col span={12}>
                                <CommonFormItem 
                                    label={t("staffPage.search.label.status")}
                                    type= "select"
                                    name="status"
                                    placeholder={t("staffPage.search.placeholder.status") as string}
                                    options={STAFF_PAGE_STATUS?.map((item:any)=>({value: item.value, label: t(item.label)}))}
                                />
                            </Col>
                        </Row>

                        <Space className="form-btn-container">
                            <CommonButton btnType="default" size={'small'} onClick={()=> onClear({advance: true})}>
                                {t("common.button.deleteCondition")}
                            </CommonButton>
                            <CommonButton btnType="primary" size={'small'} htmlType="submit">
                                {t("layout.search")}
                            </CommonButton>
                        </Space>
                    </CommonForm>
                </div>
            }
            </div>
}

export default StaffsSearchBox;

