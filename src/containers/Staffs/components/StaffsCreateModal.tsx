import React, { useCallback, useEffect, useState } from 'react'
import { Modal, Form, notification, Tabs, TabsProps} from 'antd';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import { ReactComponent as UpdateIcon } from '../../../resources/images/update_icon.svg';
import { ReactComponent as AddIcon } from '../../../resources/images/plus.svg';
import { ReactComponent as DeleteIcon } from '../../../resources/images/delete.svg';

import CommonButton from '../../../components/Common/Button';
import StaffInfoTab from './StaffInfoTab';
import AccountInfoTab from './AccountInfoTab';

import userServices from "../../../services/users.service";

interface StaffsCreateModalProps {
    dataDetail: any,
    modalStatus: boolean,
    handleOk: () => void,
    handleCancel: () => void
}

function StaffsCreateModal(props:StaffsCreateModalProps) {
    const [activeKey, setActiveKey] = useState<string>("1");
    const [items, setItems] = useState<TabsProps["items"]>([]);
    const [formAccountInfo] = Form.useForm();
    const [formStaffInfo] = Form.useForm();
    const {modalStatus, handleOk, handleCancel, dataDetail} = props;
    const { t } = useTranslation();
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [isSubmitAccountSuccess, setIsSubmitAccountSuccess] = useState<boolean>(false);
    const [isSubmitStaffSuccess, setIsSubmitStaffSuccess] = useState<boolean>(false);
    const { profile } = useSelector((state:any) => state?.profileReducer);


    const onSubmit = ()=>{ 
        setIsLoading(true);
        formAccountInfo.submit();
        formStaffInfo.submit();
        setIsLoading(false)
    }

    const onCreateStaff = useCallback(async () => {
        setIsLoading(true);
        if(dataDetail?.id){ //Chỉnh sửa nhân viên
            const resp = await userServices.updateUser({...dataDetail?.admUser, ...formAccountInfo.getFieldsValue()});
            const data = resp?.data;
            if (resp?.status === 200) {
                const respStaff = await userServices.updateStaff({...dataDetail,...formStaffInfo.getFieldsValue()});
                const dataStaff = respStaff?.data;
                if (respStaff?.status === 200) {
                    notification.success({
                        message: t('staffPage.message.editSuccess'),
                    });
                    if(profile?.admUser?.id === dataDetail?.admUser?.id){
                        window.location.reload()
                    }else{
                        handleOk();
                    }
                } else {
                    notification.error({
                        message: dataStaff?.message || t('commonError.oopsSystem'),
                    });
                    setIsSubmitAccountSuccess(false);
                    setIsSubmitStaffSuccess(false);
                    setIsLoading(false);
                }
            } else {
                notification.error({
                    message: data?.message || t('commonError.oopsSystem'),
                });
                setIsSubmitAccountSuccess(false);
                setIsSubmitStaffSuccess(false);
                setIsLoading(false);
            }
        }else{ // Thêm mới nhân viên
            const resp = await userServices.createUser({...formAccountInfo.getFieldsValue()});
            const data = resp?.data;
            if (resp?.status === 200) {
                const respStaff = await userServices.createStaff({...formStaffInfo.getFieldsValue(), admUser: {id: data?.data?.id}});
                const dataStaff = respStaff?.data;
                if (respStaff?.status === 200) {
                    notification.success({
                        message: t('staffPage.message.createSuccess'),
                    });
                    handleOk();
                } else {
                    notification.error({
                        message: dataStaff?.message || t('commonError.oopsSystem'),
                    });
                    setIsSubmitAccountSuccess(false);
                    setIsSubmitStaffSuccess(false);
                    setIsLoading(false);
                }
            } else {
                notification.error({
                    message: data?.message || t('commonError.oopsSystem'),
                });
                setIsSubmitAccountSuccess(false);
                setIsSubmitStaffSuccess(false);
                setIsLoading(false);
            }
        }
        setIsSubmitAccountSuccess(false);
        setIsSubmitStaffSuccess(false);
        setIsLoading(false);
    },[dataDetail, formAccountInfo, formStaffInfo, handleOk, profile?.admUser?.id, t])


    useEffect(()=>{
        console.log("isSubmitAccountSuccess", isSubmitAccountSuccess);
        console.log("isSubmitStaffSuccess", isSubmitStaffSuccess);
        if(isSubmitAccountSuccess && isSubmitStaffSuccess){
            onCreateStaff();
        }
        if(!isSubmitStaffSuccess){
            setActiveKey("1")
        }else if(!isSubmitAccountSuccess){
            setActiveKey("2")
        }
    },[isSubmitAccountSuccess, isSubmitStaffSuccess, onCreateStaff])

    // Xóa nhân viên
    const onDelete = async ()=>{
        setIsLoading(true);
        Modal.confirm({
            title: t('common.confirmAction'),
            content: t('common.editDataAction'),
            centered: true,
            okText: t('common.button.accept'),
            onOk: async () => { 
                const respStaff = await userServices.deleteStaffs([dataDetail?.id]);
                const dataStaff = respStaff?.data;
                if (respStaff?.status === 200) {
                    const resp = await userServices.deleteUsers([dataDetail?.admUser?.id]);
                    const data = resp?.data;
                    if (resp?.status === 200) {
                        notification.success({
                            message: t('staffPage.message.createSuccess'),
                        });
                        handleOk();
                    } else {
                        notification.error({
                            message: data?.message || t('commonError.oopsSystem'),
                        });
                    }
                } else {
                    notification.error({
                        message: dataStaff?.message || t('commonError.oopsSystem'),
                    });
                }
            },
            okButtonProps:{loading: isLoading},
            cancelText: t('common.button.cancel')
        });
        setIsLoading(false);
    }

    console.log("dataDetail",dataDetail)

    const onChangeTab = (key:string) => {
        setActiveKey(key);
    }

    // render tabs item
    useEffect(()=>{
        const listItem = [ {
            key: "1",
            label: t("staffPage.form.tabs.staffInfo") as string,
            closable: false,
            children: <StaffInfoTab
                            dataDetail={dataDetail}
                            formStaffInfo={formStaffInfo}
                            onCreateStaff={setIsSubmitStaffSuccess}
                        />
        },
        {
            key: "2",
            label: t("staffPage.form.tabs.accountInfo") as string,
            closable: false,
            children: <AccountInfoTab
                            dataDetail={dataDetail?.admUser}
                            formAccountInfo={formAccountInfo}
                            onCreateAccount={setIsSubmitAccountSuccess}
                        />
        }];
        setItems(listItem);
    },[dataDetail, formAccountInfo, formStaffInfo, t]);

    return <Modal
        className="staff-modal-create"
        title={dataDetail?.id ? t("staffPage.form.editTitle") : t("staffPage.form.createTitle")}
        open={modalStatus}
        maskClosable={false}
        onCancel={handleCancel}
        footer={[
            <CommonButton
                key="close"
                onClick={handleCancel}
                size="small"
            >
                {t("common.button.close")}
            </CommonButton>,
            dataDetail?.id ? <CommonButton
                key="remove"
                onClick={onDelete}
                btnType="danger"
                size="small"
                className="btn-icon-left"
            >
                <><DeleteIcon/> {t("common.button.remove")}</>
            </CommonButton> : null,
            <CommonButton
                key="submit"
                htmlType="submit"
                btnType="primary"
                size="small"
                onClick={onSubmit}
                className="btn-icon-left"
                loading={isLoading}
            >
                {dataDetail?.id ? <><UpdateIcon/> {t("common.button.update")}</> : <><AddIcon/> {t("common.button.addNew")}</>}
            </CommonButton>
        ]}
    >
        <Tabs 
            items={items} 
            activeKey={activeKey}
            onChange={onChangeTab}
        />
    </Modal>
}

export default StaffsCreateModal;

