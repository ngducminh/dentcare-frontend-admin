import React, { useCallback, useState, useMemo, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { ExportOutlined } from '@ant-design/icons';
import moment from "moment";
import { saveAs } from 'file-saver';
import { Modal, Space, notification } from "antd";

import { ReactComponent as IconAdd } from '../../resources/images/plus.svg';
import { ReactComponent as IconAction } from '../../resources/images/action_icon.svg';

import StaffsSearchBox from "./components/StaffsSearchBox";
import StaffsCreateModal from "./components/StaffsCreateModal";
import { useQuery } from "../../utils/customHooks";
import { DATE_TIME_FORMAT_SECOND, DEFAULT_PAGE_SIZE, EQUIPMENT_PAGE_STATUS } from "../../utils/constants";
import CommonButton from "../../components/Common/Button";
import CommonDropdown from "../../components/Common/Dropdown";
import CommonTag from "../../components/Common/Tag";
import { buildQueryString } from "../../utils/utilFunctions";
import CommonTable from "../../components/Common/Table";
import CommonImage from "../../components/Common/Image";

import userServices from "../../services/users.service";

function Staffs(){
    const componentPath = '/staffs'
    const navigate = useNavigate();
    const { t } = useTranslation();
    const queryObj:any = useQuery();
    const {params = {}, search} = queryObj
    const {
        page: pageQuery,
        pageSize: pageSizeQuery,
        sortBy: sortByQuery,
        sortType: sortTypeQuery,
        search: searchQuery,
    } = params
    const page = pageQuery ? parseFloat(pageQuery) : 1;
    const pageSize = pageSizeQuery ? parseFloat(pageSizeQuery) : DEFAULT_PAGE_SIZE;
    const [data, setData] = useState<any>();
    const [visibleModalCreate, setVisibleModalCreate] = useState<boolean>(false);
    const [isLoading, setIsLoading] = useState(false);
    const [dataSelected, setDataSelected] = useState<number[]>([]);
    const [isLoadingExport, setIsLoadingExport] = useState(false);
    const [currentStaff, setCurrentStaff] = useState<any>(undefined)
    const [expandedRowKeys, setExpandedRowKeys] = useState<number[]>([]);

    const getData = useCallback(async (reload?: boolean) => {
        setIsLoading(true);
        const newSearch = searchQuery ? JSON.parse(searchQuery) : {};
        const paramsSearch = {
            page: reload? 0 : page-1,
            size: pageSize,
            sortBy: sortByQuery,
            sortType: sortTypeQuery,
            search: JSON.stringify({
                ...newSearch,
                searchType: 1,
            }),
        }
        const resp = await userServices.getListAccounts(paramsSearch);
        const data = resp?.data;
        if (resp?.status === 200) {
            setData(data?.data)
        } else {
            notification.error({
                message: data?.message || t('commonError.oopsSystem'),
            });
        }
        setIsLoading(false);
    },[page, pageSize, searchQuery, sortByQuery, sortTypeQuery, t])

    useEffect(() => {
        getData()
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [search])

    const onPageChange = (pagination:any, filters:any, sorter:any) => {
        let queryString = buildQueryString({
            ...params,
            page: pageSize === pagination?.pageSize ? pagination?.current : 1,
            pageSize: pagination?.pageSize,
            sortBy: sorter?.order ? sorter?.field : '',
            sortType: sorter?.order ? sorter?.order === 'ascend' ? 'asc' : 'desc' : ''
        })
        navigate(`${componentPath}${queryString || ''}`)
    }

    // rowSelection objects indicates the need for row selection
    const rowSelection = {
        onChange: (selectedRowKeys:any, selectedRows:any) => {
            setDataSelected(selectedRowKeys);
            console.log('onChange rowSelection', selectedRowKeys, selectedRows);
        },
        onSelect: (record:any, selected:any, selectedRows:any) => {
            console.log('onChange onSelect', record, selected, selectedRows);
        },
        onSelectAll: (selected:any, selectedRows:any, changeRows:any) => {
            console.log('onChange onSelectAll', selected, selectedRows, changeRows);
        },
    };

    const handleOk = ()=>{
        getData(true);
        setVisibleModalCreate(false);
        setCurrentStaff(undefined);
    }

    // Thao tác xóa, đổi trạng thái
    const updateStaffs = useCallback((type:any, value?:any) => {
        if (dataSelected?.length === 0) {
            return
        }
        Modal.confirm({
            title: t('common.confirmAction'),
            okText: t('common.button.accept'),
            onOk: async () => {
                let resp;
                if(type === 'delete'){
                    resp = await userServices.deleteStaffs(dataSelected)
                }else{
                    if(value===0){
                        resp = await userServices.unlockStaffs(dataSelected)
                    }else{
                        resp = await userServices.lockStaffs(dataSelected)
                    }
                }
                const data = resp?.data;
                if (resp?.status === 200) {
                    if(type === 'delete'){
                        notification.success({
                            message: t('staffPage.message.deleteSuccess'),
                        });
                    }else{
                        notification.success({
                            message: t('staffPage.message.changeStatusSuccess'),
                        });
                    }
                    getData();
                } else {
                    notification.error({
                        message: data?.message || t('commonError.oopsSystem'),
                    });
                }
            },
            onCancel: () => {
                console.log('Cancel');
            },
            cancelText: t('common.button.cancel')
        })
    },[dataSelected, getData, t])

    // xuất excel
    const exportData = async () => {            // xuất excel
        setIsLoadingExport(true);
        const newSearch = searchQuery ? JSON.parse(searchQuery) : {};           // lấy search
        const paramsSearch = {                                                  // gọi api xuất excel
            page: 0,
            size: 10000,
            sortBy: sortByQuery,
            sortType: sortTypeQuery,
            search: JSON.stringify({
                ...newSearch,
                searchType: 1,
            }),
        }
        const resp = await userServices.exportUser(paramsSearch);               // gọi api xuất excel
        const data = resp?.data;
        if (resp?.status === 200) {
            const fileName = `Staffs_Data_Export_${moment().format('YYYYMMDD')}_${moment().unix()}.xlsx`            // tên file
            saveAs(data, fileName);                                         // lưu file
        } else {
            notification.error({
                message: data?.message || t('commonError.oopsSystem'),
            });
        }
        
        setIsLoadingExport(false);
    }

    const items = useMemo(() => {                   // danh sách các item trong dropdown
        const children = EQUIPMENT_PAGE_STATUS.map((item:any, index:number) => {        // danh sách các trạng thái
            return {
                key: `1-${index}`,              // key
                label: <div onClick={()=>{updateStaffs("changeStatus",item.value)}}>{`${t(item.label)}`}</div>      // label
            }
        })
        return [                                                    // trả về 1 mảng các item
            {
                key: '1',                       // key
                label: `${t('staffPage.changeStatus')}`,
                children: children
            },
            {
                key: '2',
                label:  <div onClick={()=>{updateStaffs("delete")}}>{`${t('common.button.remove')}`}</div>,
            },
        ];
    }, [t, updateStaffs])                       // t: dịch từ khóa, updateStaffs: hàm xóa, đổi trạng thái

    const columns = [
        {
            title: t("staffPage.list.columns.STT"),
            key: 'index',
            fixed: "left",
            width: "3%",
            align: "center",
            render: (cell:any, record:any, index:number) => (page - 1) * pageSize + index + 1,          // STT
        },
        {
            title: t("staffPage.list.columns.image"),
            dataIndex: 'image',                                 // ảnh đại diện
            key: 'image',
            align: "center",
            fixed: "left",
            render: (value:any, row:any) => {
                return <CommonImage 
                        src={value}
                    />
                }
        },
        {
            title: t("staffPage.list.columns.staffCode"),
            dataIndex: 'staffCode',                             // mã nhân viên
            key: 'staffCode',
            sorter: true,
            render: (value:any, row:any) => {
                return <div className="link" onClick={()=>{setCurrentStaff(row); setVisibleModalCreate(true)}}>{value || '--'}</div>;
            }
        },
        {
            title: t("staffPage.list.columns.fullName"),
            dataIndex: 'fullName',
            key: 'fullName',
            sorter: true,
        },
        {
            title: t("staffPage.list.columns.userName"),
            dataIndex: 'admUser',
            key: 'admUser',
            sorter: true,
            render:  (cell: any, record: any, index: number)=>  cell?.username,
        },
        {
            title: t("staffPage.list.columns.email"),
            dataIndex: 'email',
            key: 'email',
            sorter: true,
        },
        {
            title: t("staffPage.list.columns.phoneNumber"),
            dataIndex: 'phoneNumber',
            key: 'phoneNumber',
            sorter: true,
        },
        {
            title: t("staffPage.list.columns.note"),
            dataIndex: 'note',
            key: 'note',
            sorter: true,
        },
        {
            title: t("staffPage.list.columns.modifiedDate"),
            dataIndex: 'modifiedDate',
            key: 'modifiedDate',
            sorter: true,
            render:  (cell: any, record: any, index: number)=>  cell ? moment(cell).format(DATE_TIME_FORMAT_SECOND) : "",
        },
        {
            title: t("staffPage.list.columns.modifiedBy"),
            dataIndex: 'modifiedBy',
            key: 'modifiedBy',
            sorter: true,
        },
        {
            title: t("staffPage.list.columns.status"),
            dataIndex: 'status',
            key: 'status',
            sorter: true,
            render: (value:any, cell:any) => {
                const curStatus:any = EQUIPMENT_PAGE_STATUS.find((x:any) => x.value === value) || {}
                return <CommonTag tagType={curStatus?.type}>{t(curStatus?.label)}</CommonTag>
            },
        },
    ];
    return <>
        <StaffsSearchBox getData={()=>{}} componentPath={componentPath}/>       {/* tìm kiếm */}
        
        <div className="avic-table-top">
            <div className="avic-table-top-title">
                {t("staffPage.list.title")}
            </div>
            <Space className="avic-table-top-right">
                <CommonButton icon={<ExportOutlined />} btnType="default" size={'small'} onClick={()=>{exportData()}} loading={isLoadingExport}>
                    {t("common.button.exportExcel")}
                </CommonButton>
                <CommonDropdown menu={{ items }}>
                    <CommonButton btnType="primary" size={'small'} className="btn-icon-right">
                        {t("common.button.action")}<IconAction />
                    </CommonButton>
                </CommonDropdown>
                <CommonButton btnType="primary" size={'small'} className="btn-icon-left" onClick={()=>{setVisibleModalCreate(true)}}>
                    <IconAdd />{t("common.button.addNew")}
                </CommonButton>
            </Space>
        </div>

        <CommonTable
                rowKey={'id'}
                loading={isLoading}
                dataSource={data?.content || []}
                columns={columns}
                data={data}
                onChange={onPageChange}
                rowSelection={{...rowSelection, checkStrictly: false,}}     // checkStrictly: false: check tất cả các checkbox, true: chỉ check 1 checkbox
                defaultSorter={{                          // sắp xếp mặc định
                    order: sortTypeQuery,               // kiểu sắp xếp
                    field: sortByQuery,                     // trường sắp xếp
                }}
                scroll={{x: 1800}}                              // scroll
                expandable={{                                   // expandable
                    expandedRowKeys: expandedRowKeys,               // các row được mở rộng
                    onExpandedRowsChange: (expandedRows: any) => {              // khi mở rộng row
                        setExpandedRowKeys(expandedRows)                    // set lại các row được mở rộng
                    }
                }}
            /> 

        {visibleModalCreate ?
            <StaffsCreateModal
                dataDetail={currentStaff}
                modalStatus={visibleModalCreate}
                handleOk={handleOk}
                handleCancel={()=>{setVisibleModalCreate(false); setCurrentStaff(undefined);}}
            />
            :<></>
        }
    </>
}

export default Staffs;