import { createStore, applyMiddleware } from 'redux';       // applyMiddleware is used to add middleware to the store
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import rootReducer from './reducers';
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web
import { persistStore, persistReducer } from 'redux-persist';

const persistConfig = {         // config object cho redux-persist để lưu state vào localStorage
    key: 'reducer',        // key: key của object state     
    storage: storage,                   // storage: nơi lưu trữ state
};

const persistedReducer = persistReducer(persistConfig, rootReducer);        // tạo ra reducer mới với config của redux-persist

const loggerMiddleware = createLogger();                    // loggerMiddleware để log ra action và state của store

export const store = createStore(
    persistedReducer,                               // thay rootReducer bằng persistedReducer
    applyMiddleware(                            // applyMiddleware is used to add middleware to the store
        thunkMiddleware,                            // thunkMiddleware để dispatch action có kiểu function
        loggerMiddleware                        // loggerMiddleware để log ra action và state của store
    )
);
export const persistor = persistStore(store);
