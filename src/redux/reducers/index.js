import { combineReducers } from 'redux';

import globalReducer from './global.reducer';
import profileReducer from './profile.reducer';

const rootReducer = combineReducers({
  globalReducer,
  profileReducer
});

export default rootReducer;