import * as Constants from '../constants/profile.constants';

const initialState = {                      //khởi tạo state
  profile: null as any,                    //profile là thông tin của user
};

export default function (state = initialState, action:any) {      //action là thông tin của user truyền vào
  switch (action.type) {                                        //hằng số             
    case Constants.SAVE_PROFILE:                             //nếu là SAVE_PROFILE
      return {                                                //trả về thông tin của user
        ...state,
        profile: action?.payload,
      };
    default:
      return state
  }
}