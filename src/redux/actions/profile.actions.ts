import * as Constants from '../constants/profile.constants';

export const saveProfile = (payload:any) => {           //payload là thông tin của user truyền vào
    return {
        type: Constants.SAVE_PROFILE,                   //hằng số
        payload: payload
    }
}

