import React from 'react';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import { useSelector } from 'react-redux';

import LocalStore from "./utils/localStorage";

import Layout from "./components/Layout";
import Profile from './containers/Profile';
import Page404 from './containers/Page404';
import Login from './containers/Login';
import Dashboard from './containers/Dashboard';
import Appointments from './containers/Appointments';
import Medicals from './containers/Medicals';
import MedicalsDetail from './containers/Medicals/components/MedicalsDetail';
import Equipments from './containers/Categories/Equipments';
import Specializes from './containers/Categories/Specializes';
import MedicalServices from './containers/Categories/MedicalServices';
import Doctors from './containers/Doctors';
import Staffs from './containers/Staffs';
import Customers from './containers/Customers';
import HomePageInfo from './containers/SystemManager/HomePageInfo';
import Accounts from './containers/SystemManager/Accounts';
import RevenueReport from './containers/Reports/RevenueReport';
import AppointmentReport from './containers/Reports/AppointmentReport';

const MyRoutes = () => {        // This is the main route of the project and it will be called in index.tsx
    const accessToken = LocalStore.getInstance().read('accessToken')    //  Get accessToken from localStorage to check if user is logged in or not
    const {profile} = useSelector((state:any) => state?.profileReducer);    // Get profile from redux to check if user is logged in or not
    return (
        <Router>
            {
                accessToken         //check accessToken to know if user is logged in or not
                ?
                    <Layout>
                        <Routes>
                            <Route path="/" element={<Dashboard />}/>
                            <Route path="/profile" element={<Profile />}/>

                            {/* Quản lý lịch khám */}
                            <Route path="/appointments" element={<Appointments />}/>

                            {/* Quản lý phiếu khám */}
                            <Route path="/medicals" element={<Medicals />}/>
                            <Route path="/medicals/detail/:medicalId" element={<MedicalsDetail />}/>
                            
                            {/* Quản lý bác sĩ */}
                            {profile?.admUser?.permission !== 1 && <Route path="/doctors" element={<Doctors />}/>}
                            
                            {/* Quản lý Nhân viên */}
                            {profile?.admUser?.permission !== 1 && <Route path="/staffs" element={<Staffs />}/>}
                            
                            {/* Quản lý Khách hàng */}
                            {profile?.admUser?.permission !== 1 && <Route path="/customers" element={<Customers />}/>}
                            
                            {/* Báo cáo doanh thu */}
                            {profile?.admUser?.permission === 0 && <Route path="/revenue-report" element={<RevenueReport />}/>}

                            {/* thống kê lịch hẹn */}
                            {profile?.admUser?.permission === 0 && <Route path="/appointment-statistical" element={<AppointmentReport />}/>}

                            {/* Danh mục chức vụ */}
                            { profile?.admUser?.permission === 0 && <Route path="/equipments" element={<Equipments />}/>}

                            {/* Danh mục chuyên môn */}
                            {profile?.admUser?.permission === 0 && <Route path="/specializes" element={<Specializes />}/>}
                            
                            {/* Danh mục dịch vụ */}
                            {profile?.admUser?.permission === 0 && <Route path="/services" element={<MedicalServices />}/>}
                            
                            {/* Thông tin trang chủ */}
                            {profile?.admUser?.permission === 0 && <Route path="/home-information" element={<HomePageInfo />}/>}

                            {/* danh sách tài khoản người dùng */}
                            {profile?.admUser?.permission === 0 && <Route path="/accounts" element={<Accounts />}/>}

                            <Route path='*' element={<Page404/>} />
                        </Routes>
                    </Layout>
                :
                <Routes>
                    <Route path="/" element={<Login />}/>
                    <Route path='*' element={<Page404/>} />
                </Routes>
            }
        </Router>
    );
}
export default MyRoutes
