import { Select, SelectProps } from "antd";
import {getDOMProps} from "../../utils/utilFunctions";
import {useTranslation} from "react-i18next";

interface CommonSelectProps extends SelectProps { // tạo ra 1 interface CommonSelectProps kế thừa SelectProps
    isView?: boolean, // isView: xem
    options?: any,  // options: danh sách các option
}

const CommonSelect = (props:CommonSelectProps) => {                     // tạo ra 1 component CommonSelect kế thừa CommonSelectProps
    const {options = [], isView} = props;                           // lấy ra options và isView từ props
    if (isView) {
        const curItem:any = Array.isArray(options) ? options.find(x => x.value === props?.value) : {}     // tìm ra item có value bằng value của props
        return <div className="avic-select-view">{curItem?.label || '--'}</div> // hiển thị label của item đó
    }
    
    const childProps = { ...props }; // tạo ra 1 biến childProps bằng props
    delete childProps?.isView; // xóa isView trong childProps
    return <Select                  // trả về 1 Select
        filterOption={(input, option) =>                                // tìm kiếm
            (option?.label as string ?? '').toLowerCase().includes(input.toLowerCase())                 // tìm kiếm theo label
        }
        optionFilterProp="label"                            // tìm kiếm theo label
        {...childProps}                                                 // truyền các props còn lại vào Select
        className={`avic-select ${childProps?.className || ''}`}        // thêm class avic-select
    /> 
}

export default CommonSelect;