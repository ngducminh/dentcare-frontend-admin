import { Empty, EmptyProps } from "antd";

interface CommonEmptyProps extends EmptyProps {
}

const CommonEmpty = (props:CommonEmptyProps) => {

    return <Empty
        {...props}
        className={`avic-empty ${props?.className || ''}`}
    >
        {props?.children}
    </Empty>
}

export default CommonEmpty;