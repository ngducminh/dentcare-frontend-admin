import { Button, ButtonProps } from "antd";

interface CommonButtonProps extends ButtonProps {                           // tạo 1 interface CommonButtonProps kế thừa ButtonProps
    trueWidth?: boolean,                                                            // trueWidth: chiều rộng của button
    btnType?: 'primary' | 'info' | 'success' | 'danger' | 'warning' | 'default'     // btnType: kiểu button
}

const CommonButton = (props:CommonButtonProps) => {    // tạo 1 component CommonButton
    const {btnType = "default"} = props                 // gán giá trị mặc định cho btnType là default không ở trong DOM

    const childProps = { ...props };                    // gán giá trị của props cho childProps
    delete childProps?.btnType;                             // xóa thuộc tính btnType của childProps
 
    return <Button                                          // trả về 1 button
        type="primary" 
        {...childProps}
        className={`avic-btn 
        ${props?.className || ''} ${props?.trueWidth ? 'true-width' : ''} ${btnType}`}  // className của button 
    >
        {props?.children}                            
    </Button>
}

export default CommonButton;