import {useTranslation} from "react-i18next";
import {ReactComponent as Logo} from "../resources/images/logo_small.svg";
import React from "react";

const SystemInfo = () => {
    const { t } = useTranslation();

    return(
        <div className="overlay-box system-info-box">
            <Logo className="logo-system"/>
            <div className="system-info-title">
                {t("login.title")}
            </div>
        </div>
    )
}

export default SystemInfo;