import { Col, Image, Row } from "antd";
import { useState, useEffect } from 'react';
import moment from 'moment-timezone';

const TimeZone = () => {
    const [dateState, setDateState] = useState(moment().tz('Asia/Ho_Chi_Minh'));
    const [dateState1, setDateState1] = useState(moment().tz('Asia/Singapore'));
    
    useEffect(() => {
        setInterval(() => setDateState(moment().tz('Asia/Ho_Chi_Minh')), 1000);
    }, []);

    useEffect(() => {
        setInterval(() => setDateState1(moment().tz('Asia/Singapore')), 1000);
    }, []);
    // console.log("123",moment().tz('Asia/Ho_Chi_Minh').toLocaleString(), "456" ,dateState)

    return(
        <div className="overlay-box timezone-box">
            <Row gutter={20}>
                <Col span={12}>
                    <a href="https://time.is/vi/Hanoi" target="_blank" rel="noreferrer">
                        <div className="time-detail">
                            <Image
                                src="https://flagcdn.com/vn.svg"
                                preview={false}
                            />
                            <div className="time-detail-city">Hà Nội</div>
                            <div className="time-detail-clock">
                                {moment(dateState).hours()<10 ? '0'+moment(dateState).hours() : moment(dateState).hours()} : {moment(dateState).minutes()<10 ? '0'+moment(dateState).minutes() : moment(dateState).minutes()}
                                <span>: {moment(dateState).seconds()<10 ? '0'+moment(dateState).seconds() : moment(dateState).seconds()}</span>
                            </div>
                            <div className="time-detail-date">
                                {moment(dateState).format('ddd, DD MMM')}
                                {/*{moment(dateState).day() === 0 ? `Chủ nhật`:`Th ${moment(dateState).day()+1}`},*/}
                                {/*{moment(dateState).date() < 10 ? ` 0${moment(dateState).date()}`:` ${moment(dateState).date()}`} tháng*/}
                                {/*{moment(dateState).month() < 9 ? ` 0${moment(dateState).month()}`:` ${moment(dateState).month()+1}`}*/}
                            </div>
                        </div>
                    </a>
                </Col>
                <Col span={12}>
                    <a href="https://time.is/vi/Singapore" target="_blank" rel="noreferrer">
                        <div className="time-detail">
                            <Image
                                src="https://flagcdn.com/sg.svg"
                                preview={false}
                            />
                            <div className="time-detail-city">Singapore</div>
                            <div className="time-detail-clock">
                                {moment(dateState1).hours()<10 ? '0'+moment(dateState1).hours() : moment(dateState1).hours()} : {moment(dateState1).minutes()<10 ? '0'+moment(dateState1).minutes() : moment(dateState1).minutes()}
                                <span>: {moment(dateState1).seconds()<10 ? '0'+moment(dateState1).seconds() : moment(dateState1).seconds()}</span>
                            </div>
                            <div className="time-detail-date">
                                {moment(dateState).format('ddd, DD MMM')}
                                {/*{moment(dateState1).day() === 0 ? `Chủ nhật`:`Th ${moment(dateState1).day()+1}`},*/}
                                {/*{moment(dateState1).date() < 10 ? ` 0${moment(dateState1).date()}`:` ${moment(dateState1).date()}`} tháng*/}
                                {/*{moment(dateState1).month() < 9 ? ` 0${moment(dateState1).month()}`:` ${moment(dateState1).month()+1}`}*/}
                            </div>
                        </div>
                    </a>
                </Col>
            </Row>
        </div>
    )
}

export default TimeZone;