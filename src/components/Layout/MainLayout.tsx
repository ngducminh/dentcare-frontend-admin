import {Breadcrumb, Layout, Space} from 'antd';
import {useLocation, useNavigate} from "react-router-dom";
import React, {useMemo, ReactNode} from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import {useSelector} from "react-redux";

import {ReactComponent as CategoryIcon} from "../../resources/images/category.svg";

import {ReactComponent as HomeIcon} from "../../resources/images/home.svg";
import {ReactComponent as CalendarIcon} from "../../resources/images/calendar-icon.svg";
import {ReactComponent as MedicalIcon} from "../../resources/images/medical.svg";
import {ReactComponent as SettingIcon} from "../../resources/images/setting.svg";
import {ReactComponent as DoctorIcon} from "../../resources/images/doctor-icon.svg";
import {ReactComponent as StaffIcon} from "../../resources/images/staff-icon.svg";
import {ReactComponent as CustomerIcon} from "../../resources/images/customer-icon.svg";
import {ReactComponent as ReportIcon} from "../../resources/images/report-icon.svg";

import Sidebar from "./Sidebar";
import Language from "../Language";
import {isHavePermissionScene} from "../../utils/utilFunctions";
import HeaderProfile from "./HeaderProfile";
import Notifications from "./Notifications";

const { Header, Content } = Layout;

interface LayoutProps {
    children: ReactNode
}

function MainLayout(props:LayoutProps) {
    const navigate = useNavigate();
    const location = useLocation();
    const { t } = useTranslation();

    const {
        profile
    } = useSelector((state:any) => state?.profileReducer);

    const items:any = useMemo(() => [
        {
            label: t("layout.sidebar.home"),
            key: 'home',
            icon: <div className="sidebar-icon">
                <HomeIcon />
            </div>,
            onClick: () => {
                navigate('/')
            },
        },
        {
            label: t("layout.sidebar.manageAppointment"),
            key: 'appointments',
            icon: <div className="sidebar-icon">
                <CalendarIcon />
            </div>,
            onClick: () => {
                navigate('/appointments')
            },
        },
        {
            label: t("layout.sidebar.manageMedical"),
            key: 'medicals',
            icon: <div className="sidebar-icon">
                <MedicalIcon />
            </div>,
            onClick: () => {
                navigate('/medicals')
            },
            actions: [
                {key: 'detail', label: t("medicalPage.detail")}
            ]
        },
        {
            label: t("layout.sidebar.manageDoctor"),
            key: 'doctors',
            icon: <div className="sidebar-icon">
                <DoctorIcon />
            </div>,
            onClick: () => {
                navigate('/doctors')
            },
            hidden: (profile?.admUser?.permission !== 0 && profile?.admUser?.permission !== 2) ? true :false
        },
        {
            label: t("layout.sidebar.manageStaff"),
            key: 'staffs',
            icon: <div className="sidebar-icon">
                <StaffIcon />
            </div>,
            onClick: () => {
                navigate('/staffs')
            },
            hidden: (profile?.admUser?.permission !== 0 && profile?.admUser?.permission !== 2) ? true :false
        },
        {
            label: t("layout.sidebar.manageCustomer"),
            key: 'customers',
            icon: <div className="sidebar-icon">
                <CustomerIcon />
            </div>,
            onClick: () => {
                navigate('/customers')
            },
            hidden: (profile?.admUser?.permission !== 0 && profile?.admUser?.permission !== 2) ? true :false
        },
        {
            label: t("layout.sidebar.report"),
            key: 'report',
            icon: <div className="sidebar-icon">
                <ReportIcon />
            </div>,
            children: [
                {
                    label: t("layout.sidebar.revenueReport"),
                    key: 'revenue-report',
                    onClick: () => {
                        navigate('/revenue-report')
                    }
                },
                {
                    label: t("layout.sidebar.appointmentStatistical"),
                    key: 'appointment-statistical',
                    onClick: () => {
                        navigate('/appointment-statistical')
                    }
                },
            ],
            hidden: profile?.admUser?.permission !== 0 ? true :false
        },
        {
            label: t("layout.sidebar.manageCategory"),
            key: 'manage-category',
            icon: <div className="sidebar-icon">
                <CategoryIcon />
            </div>,
            children: [
                {
                    label: t("layout.sidebar.categoryEquipment"),
                    key: 'equipments',
                    onClick: () => {
                        navigate('/equipments')
                    }
                },
                {
                    label: t("layout.sidebar.categorySpecialize"),
                    key: 'specializes',
                    onClick: () => {
                        navigate('/specializes')
                    }
                },
                {
                    label: t("layout.sidebar.categoryService"),
                    key: 'services',
                    onClick: () => {
                        navigate('/services')
                    }
                },
            ],
            hidden: profile?.admUser?.permission !== 0 ? true :false
        },
        {
            label: t("layout.sidebar.systemManager"),
            key: 'system-manager',
            icon: <div className="sidebar-icon">
                <SettingIcon />
            </div>,
            children: [
                {
                    label: t("layout.sidebar.account"),
                    key: 'accounts',
                    onClick: () => {
                        navigate('/accounts')
                    }
                },
                {
                    label: t("layout.sidebar.homeInformation"),
                    key: 'home-information',
                    onClick: () => {
                        navigate('/home-information')
                    }
                },
            ],
            hidden: profile?.admUser?.permission !== 0 ? true :false
        },


        // item ẩn, khai báo để lấy title và breadcrumb
        {
            label: t("layout.sidebar.accountInformation"),
            key: 'profile',
            hidden: true
        },

    ], [navigate, profile?.admUser?.permission, t]);

    const config = useMemo(() => {
        const pathname = location?.pathname;
        const splitPathname = pathname.split('/')[1]
        const actionName = pathname.split('/')[2]
        const selectedKeys = splitPathname
        let openKeys = ""
        let breadcrumb = pathname==='/' ? [t("layout.sidebar.home")] : [ <Link to="/">{t("layout.sidebar.home") as string}</Link> ];
        let arrMenus = []

        for (let i = 0; i < items.length; i++) {
            if (Array.isArray(items[i]?.children)) {
                let newChild:any = []
                items[i]?.children.map((child:any) => {
                    if (isHavePermissionScene(child.key, profile) && !child.hidden) {
                        newChild.push(child)
                    }
                    return child;
                })
                if (newChild.length > 0) {
                    arrMenus.push({
                        ...items[i],
                        children: newChild
                    })
                }
            } else {
                if (isHavePermissionScene(items[i].key, profile) && !items[i].hidden) {
                    arrMenus.push({
                        ...items[i],
                    })
                }
            }
            if (items[i].key === selectedKeys) {
                breadcrumb.push(items[i].label)

                // add action like: create, edit, detail to breadcrumb
                const action = actionName && Array.isArray(items[i].actions) ? items[i].actions.find((x:any) => x.key === actionName) : null
                if (action) {
                    breadcrumb.push(action?.label)
                }
            } else {
                if (Array.isArray(items[i]?.children)) {
                    const child = items[i].children.find((x:any) => x.key === selectedKeys)
                    if (child) {
                        openKeys = items[i].key
                        breadcrumb.push(items[i].label)
                        breadcrumb.push(child.label)

                        // add action like: create, edit, detail to breadcrumb
                        const action = actionName && Array.isArray(child.actions) ? child.actions.find((x:any) => x.key === actionName) : null
                        if (action) {
                            breadcrumb.push(action?.label)
                        }
                    }
                }
            }
        }

        return {
            selectedKeys,
            openKeys,
            arrMenus,
            breadcrumb
        }
    }, [location?.pathname, t, items, profile]);

    return (
        <Layout className={'fixed-sidebar full-height-layout'} style={{ minHeight: '100vh' }}>
            <Sidebar config={config}/>
            <Layout className="main-layout fixed-header">
                <Header>
                    <div className="main-header">
                        <div className="page-title">
                            {Array.isArray(config?.breadcrumb) && config?.breadcrumb[config?.breadcrumb?.length - 1]}
                        </div>
                        <Space
                            size={20}
                            style={{
                                display: "flex",
                                alignItems: "center",
                                justifyContent: "flex-end",
                                height: "100%"
                            }}
                        >
                            {/* <div className="noti-container dpl-flex align-items-center" style={{paddingTop: 5}}>
                                <Badge size="small" count={3} overflowCount={99}>
                                    <NotificationIcon />
                                </Badge>
                            </div> */}
                            
                            <Notifications />

                            <Language />

                            <HeaderProfile />
                        </Space>
                    </div>
                </Header>
                <Breadcrumb className="avic-breadcrumb">
                    {   config?.breadcrumb?.length > 1 &&
                        Array.isArray(config?.breadcrumb) && config?.breadcrumb.map((x,y) => {
                            return <Breadcrumb.Item key={y}>{x}</Breadcrumb.Item>
                        })
                    }
                </Breadcrumb>
                <Content>
                    <div className="main-layout-background">
                        {props.children}
                    </div>
                </Content>
                {/*<Footer style={{ textAlign: 'center' }}>Created by Jason</Footer>*/}
            </Layout>
        </Layout>
    );
}

export default MainLayout;

