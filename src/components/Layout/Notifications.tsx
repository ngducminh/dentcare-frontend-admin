import {Badge, notification} from 'antd';

import React, {useMemo, useState} from 'react';
import {useTranslation} from "react-i18next";
import {ReactComponent as Notification} from "../../resources/images/notification.svg";
import CommonDropdown from "../Common/Dropdown";
// import notificationsServices from "../../services/notifications.service";
import CommonSpin from "../Common/Spin";
import CommonEmpty from "../Common/Empty";
import {timeAgo} from "../../utils/utilFunctions";

const fakeData = [
    {
        "content": "Lịch hẹn <span style='color: #50b0ff'>DentCare_APP_18647601</span> đã bị từ chối.",
        "createdDate": "2023-05-17T15:18:41.522Z",
        "isRead": 0,
        "message": "string",
        "messageType": "string",
        "notificationDetailId": 0,
    },
    {
        "content": "Lịch hẹn <span style='color: #50b0ff'>DentCare_APP_18647602</span> đã được duyệt bởi <span style='color: #50b0ff'>Admin</span>.",
        "createdDate": "2023-05-17T14:18:41.522Z",
        "isRead": 1,
        "message": "string",
        "messageType": "string",
        "notificationDetailId": 0,
    },
    {
        "content": "Lịch hẹn <span style='color: #50b0ff'>DentCare_APP_18647603</span> đã được tạo phiếu khám.",
        "createdDate": "2023-05-17T13:18:41.522Z",
        "isRead": 0,
        "message": "string",
        "messageType": "string",
        "notificationDetailId": 0,
    },
    {
        "content": "Lịch hẹn <span style='color: #50b0ff'>DentCare_APP_18647604</span> đã hoàn thành ca khám.",
        "createdDate": "2023-05-17T6:18:41.522Z",
        "isRead": 1,
        "message": "string",
        "messageType": "string",
        "notificationDetailId": 0,
    },
    {
        "content": "Lịch hẹn <span style='color: #50b0ff'>DentCare_APP_18647605</span> đã được duyệt bởi <span style='color: #50b0ff'>Admin</span>.",
        "createdDate": "2023-05-17T11:18:41.522Z",
        "isRead": 0,
        "message": "string",
        "messageType": "string",
        "notificationDetailId": 0,
    },
    {
        "content": "Lịch hẹn <span style='color: #50b0ff'>DentCare_APP_18647602</span> đã được duyệt bởi <span style='color: #50b0ff'>Admin</span>.",
        "createdDate": "2023-05-16T15:18:41.522Z",
        "isRead": 1,
        "message": "string",
        "messageType": "string",
        "notificationDetailId": 0,
    },
    {
        "content": "Lịch hẹn <span style='color: #50b0ff'>DentCare_APP_18647601</span> đã bị từ chối.",
        "createdDate": "2023-05-15T15:18:41.522Z",
        "isRead": 0,
        "message": "string",
        "messageType": "string",
        "notificationDetailId": 0,
    },
    {
        "content": "Lịch hẹn <span style='color: #50b0ff'>DentCare_APP_18647602</span> đã được duyệt bởi <span style='color: #50b0ff'>Admin</span>.",
        "createdDate": "2023-05-15T15:18:41.522Z",
        "isRead": 0,
        "message": "string",
        "messageType": "string",
        "notificationDetailId": 0,
    },
    {
        "content": "Lịch hẹn <span style='color: #50b0ff'>DentCare_APP_18647601</span> đã bị từ chối.",
        "createdDate": "2023-05-15T15:18:41.522Z",
        "isRead": 0,
        "message": "string",
        "messageType": "string",
        "notificationDetailId": 0,
    },
    {
        "content": "Lịch hẹn <span style='color: #50b0ff'>DentCare_APP_18647602</span> đã được duyệt bởi <span style='color: #50b0ff'>Admin</span>.",
        "createdDate": "2023-05-15T15:18:41.522Z",
        "isRead": 0,
        "message": "string",
        "messageType": "string",
        "notificationDetailId": 0,
    },
    {
        "content": "Lịch hẹn <span style='color: #50b0ff'>DentCare_APP_18647601</span> đã bị từ chối.",
        "createdDate": "2023-05-15T15:18:41.522Z",
        "isRead": 0,
        "message": "string",
        "messageType": "string",
        "notificationDetailId": 0,
    },
    {
        "content": "Lịch hẹn <span style='color: #50b0ff'>DentCare_APP_18647602</span> đã được duyệt bởi <span style='color: #50b0ff'>Admin</span>.",
        "createdDate": "2023-05-15T15:18:41.522Z",
        "isRead": 0,
        "message": "string",
        "messageType": "string",
        "notificationDetailId": 0,
    },
    {
        "content": "Lịch hẹn <span style='color: #50b0ff'>DentCare_APP_18647601</span> đã bị từ chối.",
        "createdDate": "2023-05-15T15:18:41.522Z",
        "isRead": 0,
        "message": "string",
        "messageType": "string",
        "notificationDetailId": 0,
    },
    {
        "content": "Lịch hẹn <span style='color: #50b0ff'>DentCare_APP_18647602</span> đã được duyệt bởi <span style='color: #50b0ff'>Admin</span>.",
        "createdDate": "2023-05-15T15:18:41.522Z",
        "isRead": 0,
        "message": "string",
        "messageType": "string",
        "notificationDetailId": 0,
    },
    {
        "content": "Lịch hẹn <span style='color: #50b0ff'>DentCare_APP_18647601</span> đã bị từ chối.",
        "createdDate": "2023-05-15T15:18:41.522Z",
        "isRead": 0,
        "message": "string",
        "messageType": "string",
        "notificationDetailId": 0,
    },
    {
        "content": "Lịch hẹn <span style='color: #50b0ff'>DentCare_APP_18647602</span> đã được duyệt bởi <span style='color: #50b0ff'>Admin</span>.",
        "createdDate": "2023-05-15T15:18:41.522Z",
        "isRead": 0,
        "message": "string",
        "messageType": "string",
        "notificationDetailId": 0,
    },
    {
        "content": "Lịch hẹn <span style='color: #50b0ff'>DentCare_APP_18647601</span> đã bị từ chối.",
        "createdDate": "2023-05-15T15:18:41.522Z",
        "isRead": 0,
        "message": "string",
        "messageType": "string",
        "notificationDetailId": 0,
    },
    {
        "content": "Lịch hẹn <span style='color: #50b0ff'>DentCare_APP_18647602</span> đã được duyệt bởi <span style='color: #50b0ff'>Admin</span>.",
        "createdDate": "2023-05-15T15:18:41.522Z",
        "isRead": 0,
        "message": "string",
        "messageType": "string",
        "notificationDetailId": 0,
    },
    {
        "content": "Lịch hẹn <span style='color: #50b0ff'>DentCare_APP_18647601</span> đã bị từ chối.",
        "createdDate": "2023-05-15T15:18:41.522Z",
        "isRead": 0,
        "message": "string",
        "messageType": "string",
        "notificationDetailId": 0,
    },
    {
        "content": "Lịch hẹn <span style='color: #50b0ff'>DentCare_APP_18647602</span> đã được duyệt bởi <span style='color: #50b0ff'>Admin</span>.",
        "createdDate": "2023-05-15T15:18:41.522Z",
        "isRead": 0,
        "message": "string",
        "messageType": "string",
        "notificationDetailId": 0,
    },
    {
        "content": "Lịch hẹn <span style='color: #50b0ff'>DentCare_APP_18647601</span> đã bị từ chối.",
        "createdDate": "2023-05-15T15:18:41.522Z",
        "isRead": 0,
        "message": "string",
        "messageType": "string",
        "notificationDetailId": 0,
    },
    {
        "content": "Lịch hẹn <span style='color: #50b0ff'>DentCare_APP_18647602</span> đã được duyệt bởi <span style='color: #50b0ff'>Admin</span>.",
        "createdDate": "2023-05-15T15:18:41.522Z",
        "isRead": 0,
        "message": "string",
        "messageType": "string",
        "notificationDetailId": 0,
    },
    {
        "content": "Lịch hẹn <span style='color: #50b0ff'>DentCare_APP_18647601</span> đã bị từ chối.",
        "createdDate": "2023-05-15T15:18:41.522Z",
        "isRead": 0,
        "message": "string",
        "messageType": "string",
        "notificationDetailId": 0,
    },
    {
        "content": "Lịch hẹn <span style='color: #50b0ff'>DentCare_APP_18647602</span> đã được duyệt bởi <span style='color: #50b0ff'>Admin</span>.",
        "createdDate": "2023-05-15T15:18:41.522Z",
        "isRead": 0,
        "message": "string",
        "messageType": "string",
        "notificationDetailId": 0,
    },
    {
        "content": "Lịch hẹn <span style='color: #50b0ff'>DentCare_APP_18647601</span> đã bị từ chối.",
        "createdDate": "2023-05-15T15:18:41.522Z",
        "isRead": 0,
        "message": "string",
        "messageType": "string",
        "notificationDetailId": 0,
    },
    {
        "content": "Lịch hẹn <span style='color: #50b0ff'>DentCare_APP_18647602</span> đã được duyệt bởi <span style='color: #50b0ff'>Admin</span>.",
        "createdDate": "2023-05-15T15:18:41.522Z",
        "isRead": 0,
        "message": "string",
        "messageType": "string",
        "notificationDetailId": 0,
    },{
        "content": "Lịch hẹn <span style='color: #50b0ff'>DentCare_APP_18647601</span> đã bị từ chối.",
        "createdDate": "2023-05-15T15:18:41.522Z",
        "isRead": 0,
        "message": "string",
        "messageType": "string",
        "notificationDetailId": 0,
    },
    {
        "content": "Lịch hẹn <span style='color: #50b0ff'>DentCare_APP_18647602</span> đã được duyệt bởi <span style='color: #50b0ff'>Admin</span>.",
        "createdDate": "2023-05-15T15:18:41.522Z",
        "isRead": 0,
        "message": "string",
        "messageType": "string",
        "notificationDetailId": 0,
    },
    {
        "content": "Lịch hẹn <span style='color: #50b0ff'>DentCare_APP_18647601</span> đã bị từ chối.",
        "createdDate": "2023-05-15T15:18:41.522Z",
        "isRead": 0,
        "message": "string",
        "messageType": "string",
        "notificationDetailId": 0,
    },
    {
        "content": "Lịch hẹn <span style='color: #50b0ff'>DentCare_APP_18647602</span> đã được duyệt bởi <span style='color: #50b0ff'>Admin</span>.",
        "createdDate": "2023-05-15T15:18:41.522Z",
        "isRead": 0,
        "message": "string",
        "messageType": "string",
        "notificationDetailId": 0,
    },
]

function DashboardSearch() {
    const { t } = useTranslation();

    const [isLoading, setIsLoading] = useState(false)
    const [listNotifications, setListNotifications] = useState(fakeData)

    const getListNotifications = async (page:number) => {
        // setIsLoading(true)
        // const paramsSearch = {
        //     page: page,
        //     size: 10
        // }
        // const resp = await notificationsServices.getList(paramsSearch);
        // const data = resp?.data;
        // if (resp?.status === 200) {
        //     // setListNotifications(data?.content || [])
        //     setIsLoading(false)
        // } else {
        //     setIsLoading(false)
        //     notification.error({
        //         message: data?.message || t('commonError.oopsSystem'),
        //     });
        // }
    }

    const handleDropdownOpenChange = (open:boolean) => {
        if (open) {
            getListNotifications(0)
        }
    }

    const renderListItem = (item:any, index:number) => {
        return <div key={index} className="notification-item">
            <div className={`notification-item-status ${item.isRead === 1 ? 'is-read' : ''}`}/>
            <div className="notification-item-content-box">
                <div dangerouslySetInnerHTML={{__html: item?.content}} className="notification-item-content ellipsis-2" />
                <div className="notification-item-date">
                    {timeAgo(item.createdDate)}
                </div>
            </div>

        </div>
    }

    const renderListNotification = () => {
        return <>
            {Array.isArray(listNotifications) && listNotifications.map(renderListItem)}
        </>
    }

    const dropDownRender = () => {
        return <div className="notification-box">
            <div className="notification-content-title-box">
                <div className="notification-content-title">
                    Thông báo
                </div>

                <div className="notification-content-read-all">Đánh dấu xem tất cả</div>
            </div>
            <CommonSpin isLoading={isLoading}>
                <div className="notification-content">
                    {
                        listNotifications?.length === 0
                        ?
                        <CommonEmpty className="pdt-70"/>
                        :
                        renderListNotification()
                    }
                </div>
            </CommonSpin>
        </div>
    }

    return <CommonDropdown
        trigger={['click']}
        onOpenChange={handleDropdownOpenChange}
        dropdownRender={dropDownRender}
        // placement="bottom"
    >
        <div className="noti-container dpl-flex align-items-center cursor-pointer" style={{paddingTop: 5}}>
            <Badge size="small" count={16} overflowCount={99}>
                <Notification />
            </Badge>
        </div>
    </CommonDropdown>;
}

export default DashboardSearch;

