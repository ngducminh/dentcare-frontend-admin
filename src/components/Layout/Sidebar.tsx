import {
    RightCircleOutlined
} from '@ant-design/icons';
import { Layout, Menu } from 'antd';
import React, {useState} from 'react';

import { ReactComponent as LogoSmall } from '../../resources/images/logo.svg';
// import { ReactComponent as Logo } from '../../resources/images/logo-sidebar.svg';

import { ReactComponent as CollapseIcon } from '../../resources/images/sidebar-collapse-icon.svg';

import TimeZone from "../TimeZone";
import SystemInfo from "../SystemInfo";

const { Sider } = Layout;

function Sidebar (props:any) {
    const [collapsed, setCollapsed] = useState(false);

    return <Sider
        className="sidebar-container"
        collapsible
        collapsed={collapsed}
        // onCollapse={value => setCollapsed(value)}
        trigger={null}
    >
        <div className="sidebar-box">
            <div className="sidebar-content">
                <div>
                    {
                        collapsed &&
                        <div className="un-collapse-icon" onClick={() => setCollapsed(false)}>
                            <RightCircleOutlined />
                        </div>
                    }
                    {
                        !collapsed &&
                        <div className="collapse-icon" onClick={() => setCollapsed(true)}>
                            <CollapseIcon />
                        </div>
                    }
    
                    <div className="logo" >
                        {
                            // !collapsed ? <Logo /> :
                            <LogoSmall />
                        }
                    </div>
                </div>
                <div className="menu-wrapper">
                    <Menu
                        theme="dark"
                        selectedKeys={[props?.config?.selectedKeys || 'home']}
                        defaultOpenKeys={[props?.config?.openKeys]}
                        mode="inline"
                        items={props?.config?.arrMenus}
                    />
                </div>
            </div>

            <div className="sidebar-footer">
                <TimeZone/>
                <SystemInfo />
            </div>
        </div>
    </Sider>;
}

export default Sidebar;

