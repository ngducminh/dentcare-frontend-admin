import ViIcon from "../resources/images/vietnamese.svg";
import UkIcon from "../resources/images/english.svg";

// ROLE_ADMIN
// ROLE_SALE_LEADER
// ROLE_SALE_MAN
// ROLE_SALE_MANAGER
// ROLE_SALE_ADMIN
// ROLE_ACCOUNTANT

export const ROUTER_ROLE = [
    {
        scene: 'dashboard',
        roles: null //['ROLE_ADMIN', 'ROLE_SALE_LEADER', 'ROLE_SALE_MAN', 'ROLE_SALE_MANAGER', 'ROLE_SALE_ADMIN', 'ROLE_ACCOUNTANT']
    },
    {
        scene: 'quote-status',
        roles: null
    },
    {
        scene: 'purchases-status',
        roles: null
    },
    {
        scene: 'manage-quote-request',
        roles: null
    },
    {
        scene: 'roles',
        roles: null
    },
]
export const ROLES_LIST = [
    {
        id: 'ROLE_ADMIN',
        name: 'Admin'
    },
    {
        id: 'ROLE_SALE_MANAGER',
        name: 'Sale Manager'
    },
    {
        id: 'ROLE_SALE_LEADER',
        name: 'Sale Leader'
    },
    {
        id: 'ROLE_SALE_MAN',
        name: 'Sale man'
    },
    {
        id: 'ROLE_SALE_ADMIN',
        name: 'Sale Admin'
    },
    {
        id: 'ROLE_ACCOUNTANT',
        name: 'Accountant'
    },
]
export const LANGUAGE_LIST = [
    {
        value: 'vi',
        label: 'Tiếng Việt',
        icon: ViIcon
    },
    {
        value: 'en',
        label: 'English',
        icon: UkIcon
    },
]
export const DATE_FORMAT = 'DD/MM/YYYY';
export const TIME_FORMAT = 'HH:mm';
export const DATE_TIME_FORMAT = 'DD/MM/YYYY, HH:mm';
export const DATE_TIME_FORMAT_SECOND = 'DD/MM/YYYY, HH:mm:ss';
export const DOB_FORMAT = 'DD MMMM, YYYY';
export const PAGE_SIZE_LIST = [10, 20, 50, 100]
export const DEFAULT_PAGE_SIZE = 10
// export const REGEX_PASSWORD = /(?=^.{8,}$)(?=.*[0-9])(?=.*[A-Za-z]).*/g
export const REGEX_USERNAME = /^([a-zA-Z0-9_]+)$/g
export const REGEX_CODE = /^([a-zA-Z0-9]+)$/g
export const REGEX_WITHOUT_SPECIAL_CHAR = /^([a-zA-Z0-9 ]+)$/g
export const REGEX_PHONE_NUMBER = /^[0-9]([0-9]+){8}$/g
export const REGEX_ONLY_NUMBER = /^[0-9]+$/g
export const REGEX_EMAIL= /^[\w-\\.]+@([\w-]+\.)+[\w-]{2,4}$/g
export const AT_LEAST_8_CHARS = /^.{8,}$/;    // Ít nhất 8 kí tự
export const AT_LEAST_1_UPPERCASE = /^(?=.*[A-Z]).*$/;    // Ít nhất 1 kí tự viết hoa
export const AT_LEAST_1_NUMBER = /^(?=.*\d).*$/;  // Ít nhất 1 số
export const REGEX_PASSWORD = /^(?=.*[A-Z])(?=.*\d).{8,}$/;


export const VI_MOMENT_CONFIG = {
    week: {
        dow: 1 /// Date offset
    },
    months: [
        'Tháng 1',
        'Tháng 2',
        'Tháng 3',
        'Tháng 4',
        'Tháng 5',
        'Tháng 6',
        'Tháng 7',
        'Tháng 8',
        'Tháng 9',
        'Tháng 10',
        'Tháng 11',
        'Tháng 12',
    ],
    monthsShort: [
        'Thg 1',
        'Thg 2',
        'Thg 3',
        'Thg 4',
        'Thg 5',
        'Thg 6',
        'Thg 7',
        'Thg 8',
        'Thg 9',
        'Thg 10',
        'Thg 11',
        'Thg 12',
    ],
    weekdays: ['Chủ nhật', 'Thứ 2', 'Thứ 3', 'Thứ 4', 'Thứ 5', 'Thứ 6', 'Thứ 7'],
    weekdaysShort: ['CN', 'Th 2', 'Th 3', 'Th 4', 'Th 5', 'Th 6', 'Th 7'],
    weekdaysMin: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
    relativeTime: {
        future: 'trong %s',
        past: '%s trước',
        s: 'vài giây trước',
        ss: '%d giây',
        m: '1 phút',
        mm: '%d phút',
        h: '1 giờ',
        hh: '%d giờ',
        d: '1 ngày',
        dd: '%d ngày',
        w: '1 tuần',
        ww: '%d tuần',
        M: '1 tháng',
        MM: '%d tháng',
        y: '1 năm',
        yy: '%d năm',
    },
}
// appointmentPage
export const APPOINTMENT_PAGE_STATUS = [
    {
        value: 0,
        label: 'appointmentPage.options.status.pending',
        type: 'default'
    },
    {
        value: 1,
        label: 'appointmentPage.options.status.approved',
        type: 'warning'
    },
    {
        value: 2,
        label: 'appointmentPage.options.status.examining',
        type: 'info'
    },
    {
        value: 3,
        label: 'appointmentPage.options.status.examined',
        type: 'success'
    },
    {
        value: 4,
        label: 'appointmentPage.options.status.cancelAppointment',
        type: 'danger'
    },
]

// equipmentPage
export const EQUIPMENT_PAGE_STATUS = [
    {
        value: 0,
        label: 'equipmentPage.options.status.active',
        type: 'success'
    },
    {
        value: 1,
        label: 'equipmentPage.options.status.inactive',
        type: 'default'
    },
]

export const EQUIPMENT_PAGE_TYPE=[
    {
        value: 0,
        label: "equipmentPage.options.type.basic",
    },
    {
        value: 1,
        label: "equipmentPage.options.type.medicalInstrument",
    },
    {
        value: 2,
        label: "equipmentPage.options.type.dentalChair",
    },
    {
        value: 3,
        label: "equipmentPage.options.type.sterilizationEquipment",
    },
    {
        value: 4,
        label: "equipmentPage.options.type.testingEquipment",
    },
    {
        value: 5,
        label: "equipmentPage.options.type.implantableEquipment",
    },
    {
        value: 6,
        label: "equipmentPage.options.type.beautyEquipment",
    },
]

// doctorPage
export const DOCTOR_PAGE_STATUS = [
    {
        value: 0,
        label: 'doctorPage.options.status.active',
        type: 'success'
    },
    {
        value: 1,
        label: 'doctorPage.options.status.inactive',
        type: 'default'
    },
]

export const DOCTOR_PAGE_PERMISSION = [
    {
        value: 0,
        label: 'doctorPage.options.permission.admin',
    },
    {
        value: 1,
        label: 'doctorPage.options.permission.doctor',
    },
    {
        value: 2,
        label: 'doctorPage.options.permission.staff',
    },
    {
        value: 3,
        label: 'doctorPage.options.permission.customer',
    },
]

// staffPage
export const STAFF_PAGE_STATUS = [
    {
        value: 0,
        label: 'staffPage.options.status.active',
        type: 'success'
    },
    {
        value: 1,
        label: 'staffPage.options.status.inactive',
        type: 'default'
    },
]

export const STAFF_PAGE_PERMISSION = [
    {
        value: 0,
        label: 'staffPage.options.permission.admin',
    },
    {
        value: 1,
        label: 'staffPage.options.permission.doctor',
    },
    {
        value: 2,
        label: 'staffPage.options.permission.staff',
    },
    {
        value: 3,
        label: 'staffPage.options.permission.customer',
    },
]

// customerPage
export const CUSTOMER_PAGE_STATUS = [
    {
        value: 0,
        label: 'customerPage.options.status.active',
        type: 'success'
    },
    {
        value: 1,
        label: 'customerPage.options.status.inactive',
        type: 'default'
    },
]

export const CUSTOMER_PAGE_PERMISSION = [
    {
        value: 0,
        label: 'customerPage.options.permission.admin',
    },
    {
        value: 1,
        label: 'customerPage.options.permission.doctor',
    },
    {
        value: 2,
        label: 'customerPage.options.permission.staff',
    },
    {
        value: 3,
        label: 'customerPage.options.permission.customer',
    },
]

// servicePage
export const SERVICE_PAGE_STATUS = [
    {
        value: 0,
        label: 'servicePage.options.status.active',
        type: 'success'
    },
    {
        value: 1,
        label: 'servicePage.options.status.inactive',
        type: 'default'
    },
]

export const SERVICE_PAGE_TYPE=[
    {
        value: 0,
        label: "servicePage.options.type.advise",
    },
    {
        value: 1,
        label: "servicePage.options.type.dentalAesthetics",
    },
    {
        value: 2,
        label: "servicePage.options.type.implant",
    },
    {
        value: 3,
        label: "servicePage.options.type.minorSurgery",
    },
]

//specializePage
export const SPECIALIZE_PAGE_STATUS = [
    {
        value: 0,
        label: 'specializePage.options.status.active',
        type: 'success'
    },
    {
        value: 1,
        label: 'specializePage.options.status.inactive',
        type: 'default'
    },
]

// accountPage
export const ACCOUNT_PAGE_STATUS = [
    {
        value: 0,
        label: 'accountPage.options.status.active',
        type: 'success'
    },
    {
        value: 1,
        label: 'accountPage.options.status.inactive',
        type: 'default'
    },
]
export const ACCOUNT_PAGE_PERMISSION = [
    {
        value: 0,
        label: 'accountPage.options.permission.admin',
    },
    {
        value: 1,
        label: 'accountPage.options.permission.doctor',
    },
    {
        value: 2,
        label: 'accountPage.options.permission.staff',
    },
    {
        value: 3,
        label: 'accountPage.options.permission.customer',
    },
]

// dashboard Page
export const DASHBOARD_PAGE_LIST_MONTHS = [
    {value: 1, label: "dashboardPage.options.months.january"},
    {value: 2, label: "dashboardPage.options.months.february"},
    {value: 3, label: "dashboardPage.options.months.march"},
    {value: 4, label: "dashboardPage.options.months.april"},
    {value: 5, label: "dashboardPage.options.months.may"},
    {value: 6, label: "dashboardPage.options.months.june"},
    {value: 7, label: "dashboardPage.options.months.july"},
    {value: 8, label: "dashboardPage.options.months.august"},
    {value: 9, label: "dashboardPage.options.months.september"},
    {value: 10, label: "dashboardPage.options.months.october"},
    {value: 11, label: "dashboardPage.options.months.november"},
    {value: 12, label: "dashboardPage.options.months.december"},
]

