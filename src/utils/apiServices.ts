import LocalStorage from "./localStorage";
import {AxiosInstance} from "axios";
import {LANGUAGE_LIST} from "./constants";
import axios from "axios";


class APIService { 
    // khởi tạo một đối tượng Axios (biến instance) trong constructor của lớp này để gửi các yêu cầu HTTP đến máy chủ từ xa.
    private instance: AxiosInstance;
    constructor() {
        this.instance = axios.create({
            baseURL: process.env.REACT_APP_API_URL,
        });
    }

    //thực hiện các yêu cầu HTTP như GET, POST, PUT, DELETE. Nó nhận vào các thông số như phương thức (method), đường dẫn (path), dữ liệu (data), và cấu hình (config) và trả về một promise
    async request(method:string, path:string, data:any = {}, config:any = {}) {
        this.handleRequestHeader();
        try {
            return await this.instance.request({ method, url: path, data, ...config});
        } catch (error:any) {
            return this.handleResponseError(error);
        }
    }

    // tải dữ liệu lên server
    async upload(method:string, path:string, data = {}, config = {}) {
        // check request ở đây
        this.handleRequestHeader(true)
        try {
            return await this.instance.request({ method, url: path, data, ...config});
        } catch (error:any) {
            return this.handleResponseError(error);
        }
    }

    //bao gồm tiêu đề "Accept-Language", "Accept", "Content-Type", và "Authorization" dựa trên thông tin từ local storage như accessToken
    handleRequestHeader(isUpload = false) {
        const accessToken = LocalStorage.getInstance().read('accessToken');
        const localLanguage = LocalStorage.getInstance().read('language') || LANGUAGE_LIST[0]?.value;

        this.instance.defaults.headers.common['Accept-Language'] = localLanguage;
        if (isUpload) {
            this.instance.defaults.headers.common['Accept'] = `application/json`; // nếu là ảnh thì là content type
            this.instance.defaults.headers.common['Content-Type'] = `multipart/form-data`;
        } else {
            this.instance.defaults.headers.common['Accept'] = `application/json`; // còn không sẽ dùng json như bthg
            this.instance.defaults.headers.common['Content-Type'] = `application/json;charset=UTF-8`;
        }

        if (accessToken) {
            this.instance.defaults.headers.common['Authorization'] = `Bearer ${accessToken}`; // lấy token để check
        }
    }

    handleResponseError({ response, request, message }:any) {
        // Server trả về lỗi không thành công
        if (response) {
            this.handleLoginState(response);
            return response;
        } else {
            // Reponse dont have, token is faild
            // comment when request_timeout data.
            // this.store.clearData();
            // window.location.reload();
        }

        // Không nhận được phản hồi từ phía server
        if (request) {
            return { code: '408', message: `request_timeout` };
        }

        return { code: '999', message };
    }

    // xóa accessToken và return về màn login
    handleLoginState(response:any) {
        if (response.status === 401) {                                           // những actor không có quyền sẽ lưu lại pathname, 
            const pathname = window.location.pathname
            if (pathname) {
                LocalStorage.getInstance().save('redirectUrl', pathname);
            }
            LocalStorage.getInstance().save('accessToken', null);               //xóa token và đăng nhập lại
            window.location.href = '/'
            // window.location.reload();
        }
    }
}

export default APIService;
