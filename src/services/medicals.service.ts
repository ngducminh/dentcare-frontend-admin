import APIService from '../utils/apiServices';

export interface MedicalData{
    admAppointment: {
        id: number
    },
    diagnostic?: string,
    medicalCode: string,
    medicalDate: string,
    note?: string,
    prescription?: string
}

export interface MedicalSearchParams {
    search?: string,
    page?: number,
    size?: number,
    sortBy?: string,
    sortType?: string,
}

class MedicalServices extends APIService {

    async getListMedicals(params: MedicalSearchParams) {
        let apiParams = { 
            page: params.page || 0,
            size: params.size || 10,
            sortType: params.sortType ||"DESC", 
            sortBy: params.sortBy || "modifiedDate",
            search: params.search || JSON.stringify({}) 
        }
        return await this.request('GET', `api/v1/medical/getPage`, {}, {params: apiParams});
    }


    // api medical
    async findAllMedical() {
        return await this.request('GET', `api/v1/medical/findAll`);
    }
    async getMedical(medicalId: number) {
        return await this.request('GET', `api/v1/medical/${medicalId}`);
    }
    async createMedical(data: MedicalData) {
        return await this.request('POST', `api/v1/medical/add`, data);
    }
    async updateMedical(data: any) {
        return await this.request('PUT', `api/v1/medical`, data);
    }
    async deleteMedical(medicalId:number) {
        return await this.request('DELETE', `api/v1/medical/${medicalId}`);
    }
    async deleteMedicals(medicalIds:number[]) {
        return await this.request('DELETE', `api/v1/medical/deleteIds`, medicalIds);
    }

    async exportMedical(params: MedicalSearchParams) {
        return await this.request('GET', `api/v1/medical/exportExcel`, {}, {
            responseType: 'blob',
            params: params,
        });
    }

}
const service = new MedicalServices();
export default service
