import APIService from '../utils/apiServices';

export interface SendMailData{ 
    attachment?: string,
    msgBody: string,
    recipient: string,
    subject: string
}


class SendMailServices extends APIService {
    // api send email
    async createSendMail(data: SendMailData) {
        return await this.request('POST', `public/v1/auth/sendMail`, data);
    }
    // send mail with attachment
    async createSendMailWithAttachment(data: SendMailData) {
        return await this.request('POST', `public/v1/auth/sendMailWithAttachment`, data);
    }
}
const service = new SendMailServices();
export default service
